import { TestBed } from '@angular/core/testing';

import { UserDocsService } from './user-docs.service';

describe('UserDocsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserDocsService = TestBed.get(UserDocsService);
    expect(service).toBeTruthy();
  });
});
