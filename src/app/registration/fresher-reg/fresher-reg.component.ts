import { Component,Inject, OnInit,ElementRef,ViewChild } from '@angular/core';
import { NgForm, FormArray, FormControl,AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { CustomService } from '../../Services/PublicService/customservices.service';
import { FreshersregService } from '../../Services/PublicService/freshersreg.service';
import { State } from '../../Services/PublicService/state';
import { District } from '../../Services/PublicService/district';
import * as $ from 'jquery';
import * as _moment from 'moment';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { HighestQualification } from '../../Services/PublicService/highest-qualification';
import { Courses } from '../../Services/PublicService/courses';
import { Course } from '../../Services/PublicService/course';
import { MAT_DIALOG_DATA,MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";

@Component({
selector: 'app-fresher-reg',
templateUrl: './fresher-reg.component.html',
styleUrls: ['./fresher-reg.component.css'],
providers: []

})
export class FresherRegComponent implements OnInit {
checked:any;
@ViewChild('someModal') someModal:ElementRef;
public states:State[];
public districts:District[];
public highestQualifications:HighestQualification[];
public courses:Courses[];
public highestQualifications1:HighestQualification[];
public highestQualifications2:HighestQualification[];
public highestQualifications3:HighestQualification[];
public highestQualifications4:HighestQualification[];
public courses1:Course[];
public personaldetails;

datePickerConfig:Partial<BsDatepickerConfig>;
firstFormGroup: FormGroup;
secondFormGroup: FormGroup;
thirdFormGroup: FormGroup;
isEditable = true;
items: FormArray;
mobpattern=/^([6-9]){1}([0-9]){9}?$/;
emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;
get personalform() { return this.firstFormGroup.controls}
get qualiform() { return this.items.controls}

keyPress(event: any) {
const pattern = /[0-9]/;
let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault(); 
}
}

// ((([0-9]|[1-9]\d)\.\d{4})|100\.0000)
percentPress(event: any){
    console.log("eve-----",event)
    // const pattern = /[0-9]{2}/;
    const pattern = /([0-9]){0,1}?(.[0-9]{0,2})/
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault(); 
    }  
}

keyPress21(event: any) {
console.log("incoming is",event);
// mobpattern=/^([6-9]){1}([0-9]){9}?$/;
const pattern = /(7\d|8\d|9\d)\d{9}$/;
let inputChar = String.fromCharCode(event.charCode);
console.log("inputChar is...",inputChar);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault(); 
}
}
keyPress2(event: any) {
const pattern = /[0-9\+\-\ ]/;
let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault(); 
}
}
keyPress1(event: any) {
const pattern = /[a-zA-Z ]/;

let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault();
}
}
model: any = {};
edu:any={};
user1:any={};
user: string ;
error_message :string;
public birthdate: any;
public age: number;
public age_error:string;
private str:string="";

ngOnInit() {
}

constructor(
private route: ActivatedRoute,
private router: Router,
// private customadapter:CustomadapterService,
private customsdservice: CustomService,
private freshservice:FreshersregService,
private _formBuilder: FormBuilder,
public dialog: MatDialog,
) 
{
this.getuserdetails();
this.states = this.customsdservice.getStates();

console.log(this.states);
this.highestQualifications = this.customsdservice.getHighestQualifications();
this.highestQualifications1 = this.customsdservice.getHighestQualifications();
this.highestQualifications2 = this.customsdservice.getHighestQualifications();
this.highestQualifications3 = this.customsdservice.getHighestQualifications();
this.highestQualifications4 = this.customsdservice.getHighestQualifications();
console.log(this.highestQualifications);
 this.datePickerConfig = Object.assign({},
 { containerClass:'theme-dark-blue'});
this.firstFormGroup = this._formBuilder.group({
firstname: ['',Validators.required],
lastname: ['',Validators.required],
gender: ['' ,Validators.required],
dob: ['',Validators.required],
mob:['',Validators.required],
email: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
city: ['',Validators.required],
state: ['',Validators.required],
district: ['',Validators.required],
address1:['',Validators.required],
address2:['',Validators.required],
pin:['',Validators.required],
permanent_city: ['',Validators.required],
permanent_state: ['',Validators.required],
permanent_district: ['',Validators.required],
permanent_address1:['',Validators.required],
permanent_address2:['',Validators.required],
permanent_pin:['',Validators.required],
checkPermanentAddress:[''],
});

this.secondFormGroup = this._formBuilder.group({

items: this._formBuilder.array([ this.createItem() ])
});

}
createItem(): FormGroup {
return this._formBuilder.group({
highest: ['', Validators.required],
coursess: ['', Validators.required],
othercourse: [''],
specialization: ['', Validators.required],
board12: ['', Validators.required],
yop12: ['', Validators.required],
medium12: ['', Validators.required],
percent12: ['', Validators.required],
school12: ['', Validators.required],
board10: ['', Validators.required],
yop10: ['', Validators.required],
medium10: ['', Validators.required],
percent: ['', Validators.required],
school10: ['', Validators.required],
degpassingyear:['', Validators.required],
degcoursetype:['', Validators.required],
univname:['', Validators.required],
percent10:['',Validators.required]
});
}
additem(): void {
	console.log("yuyuuyuyuyuyu")
this.items = this.secondFormGroup.get('items') as FormArray;
console.log("index value is",this.items.length);
var i=this.items.length;
if (i<5)
{
     this.items.push(this.createItem());
   }
else 
{
alert("Maximum limit reached");
}
}

removeitem(i): void {
console.log(i)
this.items = this.secondFormGroup.get('items') as FormArray;
if(i!= 0){
this.items.removeAt(i);
}
else{
}
}
//get district
public districtsn:any ={};
onSelect(statename) {
console.log("state is",statename);
this.districtsn = this.customsdservice.getDistrict().filter((item)=> item.mystate == statename);
console.log(this.districtsn);

}


public districtsm:any = [];
onSelect1(state) {
console.log("state777777777777777 is",state);
this.districtsm = this.customsdservice.getDistrict().filter((item)=> item.mystate == state);
console.log(this.districtsm);
}
public courses4 : any = [];
getcourses(qualification){
console.log("my qualification and id is",qualification);
this.courses4 = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification);
console.log('cordesddddddddd',this.courses4);
}
public courses3:any = [];
getcourses1(qualification1){
console.log("my qualification is",qualification1);
this.courses3 = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification1);
console.log('cordesddddddddd',this.courses3);
}

public courses2:any = [];
getcourses2(qualification2){
console.log("my qualification is",qualification2);
this.courses2 = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification2);
console.log("mycourses for graduation",this.courses2);
}
getToday(): string {
return new Date().toISOString().split('T')[0]
}

getValidDate(){
let a=new Date();
return new Date(a.getFullYear()-16,a.getMonth(),a.getDate())
}

calculateAge(data){
console.log("my event data is",data); 
if (data) {
let dob=(_moment((data)).format("DD/MM/YYYY"));
this.birthdate = dob;
}

}
submitregdetails1(edu){

this.personaldetails = {edu,'dob':this.birthdate}
console.log('my personal details',this.personaldetails);
}
submitedudetails(data){
let persondata = this.personaldetails;
const finaldata = {'personal':persondata,'education':data.value }
console.log('my finaldata is',finaldata);
this.freshservice.freshsubmit(finaldata).subscribe(data => {
console.log("inside if",data);
if(data == "Success"){
console.log(JSON.stringify(data));
this.dialog.open(regsuccesspopup, {
height: '243px',
width: '465px',
});

}
console.log(data);

},
error => console.log("errooorr",error)
);
}

public getuserdetails(){
this.freshservice.userdetails().subscribe(
userdata =>{
console.log("userdata in ts is..",userdata[0]);
var user_mobile = userdata[0].mobile_number;
var user_name = userdata[0].full_name;
var last_name=user_name.split(" ");
console.log(last_name);
var n =last_name.length;
console.log(last_name.length);
for(var i=0;i<n;i++){
if(i==0){
this.firstFormGroup.patchValue({firstname:last_name[i],mob:user_mobile})
} 
else{
this.str=this.str+last_name[i]+" ";
console.log("data",this.str);
}
}
console.log("final data",this.str);
this.firstFormGroup.patchValue({lastname:this.str,mob:user_mobile})
}
)
}

getDepth(outlet) {
return outlet.activatedRouteData['depth'];
}

}
//termsofusecomponentpopup
@Component({
selector: 'regsuccesspopup',
templateUrl: 'regsuccesspopup.component.html',
styleUrls: ['./regsuccesspopup.component.css']
})
export class regsuccesspopup{
constructor(
public successpopupRef: MatDialogRef<regsuccesspopup>,
@Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog,private router: Router) {}

closepopup(){
this.successpopupRef.close();
}
public conss
Successregisteration() {
this.router.navigateByUrl('/home'); // Absolute path
}
}
