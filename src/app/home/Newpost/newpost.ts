import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import {LoginService} from '../../Services/PublicService/login.service';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { InitialregistrationService } from "../../Services/PublicService/initialregistration.service";

@Component({
  selector: 'Newpost',
  templateUrl: './newpost.html',
})
export class Newpost implements OnInit {

  
  public imagePath;
  public videoPath;
  public imgURL: any;
  public imgURL1: any;
  public videoURL: any;
  public videoURL1: any;
  public message: string;


	public str:string;
	public postbtn:boolean=true;
	public images:File = null;
	public vidoes:File = null;
	public vidshow:boolean=true;
	public imgshow:boolean=true;
	public selectedFile: File = null;
    public selectedFile12: File = null;

    showSpinner = false;

	constructor(
		public loginService:LoginService,
    private initialRegServ: InitialregistrationService,
		public router:Router,
		public dialogRef: MatDialogRef<Newpost> ){}

    public requestSpinner:Boolean = false;

	ngOnInit() {
    this.initialRegServ.redirectToHome = "No Redirect"
	}

  public show22:boolean = false;

  public buttonName:any = 'Start a Post';
  public buttonName1:any = 'Post';

   toggle() {
    this.show22 = !this.show22;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.show22)  
      this.buttonName1 = "Post";
    else
      this.buttonName = "Start a Post";
  }

  // loadData(){
  //   this.showSpinner = true;
  //   setTimeout(()=>{
  //     this.showSpinner = false;

  //   },5000)
  // }

	txtcontent(data){
		if (data!=''){
		console.log("c--------",data, data.target.value)
		this.str=data.target.value.trim()
		this.postbtn=false; 
		}
		else{
			if (this.images!=null){
				this.postbtn=false;
			}else if(this.vidoes!=null){
				this.postbtn=false;
			}else{
				this.postbtn=true;
			}
		}
	} 
	onCloseclick(){
   this.imgURL1=false;
   this.videoURL1=false;
   this.vidshow = true;
   this.imgshow =true;
   this.vidoes=null;
   this.images=null;
   this.selectedFile=null;
   this.selectedFile12=null;
   if (this.str==''){
     this.postbtn=true;
   }
 }
 heading=''
 post1(data){
  this.requestSpinner = true;
  console.log("in post---",data)
  let heading=data.trim()
    const fd = new FormData();
    fd.append("image",this.selectedFile);
    fd.append("video",this.selectedFile12);
    fd.append("content",this.str)
    fd.append("heading",heading)
    console.log("hghfghfg",this.selectedFile,fd);
    // this.router.navigate(['home']);
    if(this.selectedFile || this.str != null){
    this.loginService.postdata(fd).subscribe(
      backResponse => {
        console.log("hgfffffghfgjty",backResponse);
        
        this.imgURL1=false;
        this.videoURL1=false;
        this.str='';
        this.heading=''
        this.toggle();
        this.selectedFile=null;
        this.selectedFile12=null;
        this.postbtn=true;
        
        this.closepop();
        this.initialRegServ.redirectToHome = "Redirect"
        this.router.navigate(['/message']);

        
        
        
      },
      error =>{ console.log("erroorrrr",error)
      alert("While posting an error occured,please try again!")
      }
    );
    
    // this.router.navigate(['home']);
  }
  else{
    alert("Sorry you haven't selected any file");
  }

}
   closepop(){
       this.dialogRef.close();

   }

profiledata(){
      this.loginService.profiledata1().subscribe(backResponse => { 
      console.log("profile data",backResponse)       
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrr",error)
    );
  }
  
  public profi:File = null;
  public full_name:any;
  
filechange(event){
  this.selectedFile = <File> event.target.files[0];
 
   
   if(this.selectedFile.size <= 5000000){
     this.postbtn=false;
     this.vidshow = false;
     this.images =this.selectedFile;
     this.show22=true;
     console.log("imagesizeeeeee......",this.images)
   }
   else{
     this.message = "The image size should be less than 5MB";
   }
  if (event.target.files[0].length === 0)
      return;
 
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.imgURL1 = reader.result; 
    }



}

  

preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
    var mimeType = files[0].type;
    if (mimeType.match(/video\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.videoPath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.videoURL = reader.result; 
    }
  }

  filechange12(event){
  this.selectedFile12 = <File> event.target.files[0];
 
 if(this.selectedFile12.size <= 50000000){
  this.postbtn=false;
   this.imgshow = false;
    this.vidoes = this.selectedFile12;
    this.show22=true;
    console.log("size",this.vidoes)
    
 }
 else{
   this.message = "The video size should be less than 50MB";
 }
  if (event.target.files[0].length === 0)
      return;
 
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/video\/*/) == null) {
      this.message = "Only videos are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.videoPath = event.target.files;
    reader.readAsDataURL(this.vidoes); 
    reader.onload = (_event) => { 
      this.videoURL1 = reader.result; 
    }



}

}
