import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationModule } from './registration/registration.module';
import { HomeModule } from './home/home.module';
// import { DataOwnerModule } from './data-owner/data-owner.module';
// import { DataAccessorModule } from './data-accessor/data-accessor.module';
// import { AdminModule } from './admin/admin.module';
import { DataService } from './data.service';
import { Globals } from './globals';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PublicService } from './Services/PublicService/public.service';
import { PrivateService } from './Services/PrivateService/private.service';
import { ArraylistService } from './Services/ArrayListService/arraylist.service';
import { LoginService } from './Services/PublicService/login.service' ;
import { DemoMaterialModule} from '../material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrivateOtp,Pinsucesspop } from './PrivateData-otp/privateotp';
import { TokenInterceptService } from './Services/PublicService/token-intercept.service';
import { HttpClientModule,HttpHeaders, HttpErrorResponse,HTTP_INTERCEPTORS } from  '@angular/common/http';
import { DataAccesserService } from './Services/PrivateService/DataAccesser/data-accesser.service';
import { DataOwnerService } from './Services/PrivateService/DataOwner/data-owner.service';
import { DragScrollModule } from 'ngx-drag-scroll';
import { AppCustomPreloader } from './AppCustomPreloader';
import { AuthGuard } from './auth.guard';
// import { FiltervaluePipe } from './filtervalue.pipe';
import { NgxAutoScrollModule } from "ngx-auto-scroll";

@NgModule({
  declarations: [
    AppComponent,
    PrivateOtp,
    Pinsucesspop,
    // FiltervaluePipe,
  ],
  entryComponents: [
    PrivateOtp,
    Pinsucesspop
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RegistrationModule,
    HomeModule,
    // DataOwnerModule,
    // DataAccessorModule,
    // AdminModule,
    HttpClientModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    DemoMaterialModule,
    FormsModule, 
    NgxAutoScrollModule,
    ReactiveFormsModule.withConfig({warnOnNgModelWithFormControl: 'never'}),
    DragScrollModule
    
  ],
  providers: [
    DataService,
    DataAccesserService,
    DataOwnerService, 
    LoginService,
    Globals,
    PublicService,
    PrivateService,
    ArraylistService,
    AppCustomPreloader,
    AuthGuard,
    {
      provide:HTTP_INTERCEPTORS,useClass:TokenInterceptService,multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
