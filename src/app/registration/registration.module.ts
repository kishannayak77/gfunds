import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationRoutingModule } from './registration-routing.module';
import { SignupComponent,InitialregComponent, LandingComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { FresherRegComponent, regsuccesspopup } from './fresher-reg/fresher-reg.component';
import { ExperienceRegComponent } from './experience-reg/experience-reg.component';
import { OrganisationRegComponent } from './organisation-reg/organisation-reg.component';
import { SharedComponent,CopyRightPolicyComponent,PageHeaderComponent,PrivacyComponent,PrivacyHubComponent,PrivacyPolicyComponent,PrivacySettingsComponent,SummaryOfChangeComponent,UserAgreementComponent } from './shared/shared.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { MatSelectModule, MatStepperModule, MatInputModule, MatButtonModule,MatFormFieldModule, MatAutocompleteModule } from '@angular/material';
// import {MatDatepickerModule} from '@angular/material/datepicker';
import { DemoMaterialModule} from '../../material-module';
import { ForgotpasswordComponent,ResetPasswordComponent,VerifyPasswordComponent } from './forgotpassword/forgotpassword.component';
import { HttpClientModule,HttpHeaders, HttpErrorResponse,HTTP_INTERCEPTORS} from  '@angular/common/http';
import { TokenInterceptService } from '../Services/PublicService/token-intercept.service';
import { LoginService } from '../Services/PublicService/login.service';
import { Globals } from '../globals';
import { AuthGuard } from '../auth.guard';
import { RegistervalidatorComponent } from './registervalidator/registervalidator.component';


@NgModule({
  declarations: [
    SignupComponent,
    InitialregComponent,
    LoginComponent,
    FresherRegComponent,
    ExperienceRegComponent,
    OrganisationRegComponent,
    SharedComponent,
    ForgotpasswordComponent,
    ResetPasswordComponent,
    VerifyPasswordComponent,
    regsuccesspopup,
    CopyRightPolicyComponent,
    PageHeaderComponent,
    PrivacyComponent,
    PrivacyHubComponent,
    PrivacyPolicyComponent,
    PrivacySettingsComponent,
    SummaryOfChangeComponent,
    UserAgreementComponent,
    LandingComponent,
    RegistervalidatorComponent
  ],
  entryComponents:[
   InitialregComponent,
   ResetPasswordComponent,
   VerifyPasswordComponent,
   regsuccesspopup,
   FresherRegComponent,
   CopyRightPolicyComponent,
   PageHeaderComponent,
   PrivacyComponent,
   PrivacyHubComponent,
   PrivacyPolicyComponent,
   PrivacySettingsComponent,
   SummaryOfChangeComponent,
   UserAgreementComponent,
   LandingComponent

  ],

  imports: [
    CommonModule,
    RegistrationRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // MatStepperModule,
    //  MatInputModule, 
    //  MatButtonModule,
    //  MatAutocompleteModule,
    //  MatFormFieldModule,
    //  MatSelectModule,
    //  MatDatepickerModule,
     DemoMaterialModule,
     HttpClientModule,

  ],
  providers: [Globals,LoginService,AuthGuard,{
    provide:HTTP_INTERCEPTORS,useClass:TokenInterceptService,multi:true
  }],
  exports: []
})
export class RegistrationModule { }
