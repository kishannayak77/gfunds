import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders, HttpErrorResponse} from  '@angular/common/http';
import {Observable} from 'rxjs';
import {Globals} from '../../globals';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public alluserdata;
  private BASE_URL;
  public selecteddata;
  public searchViewProfile;
  public searchQualification;
  public searchExp;
  public searchPrfPic;
  public searchFull_name;

  addComments(arg0: any): any {
    throw new Error("Method not implemented.");
  }
 
  constructor(public  Http:  HttpClient, private globals: Globals) {
  this.BASE_URL =  globals.url;
   }
 
//logout
logout11(){
   let url:string = `${this.BASE_URL}/logoutuser/`;
    return  this.Http.get(url);
}

getsearchPearson(data):Observable <any> {
  console.log("im in search box");
  let url:string =`${this.BASE_URL}/searchVal/`;
  return this.Http.post(url,data);
}

//to get my friend ids
getmyfrds(){
  let url:string = `${this.BASE_URL}/getFriendIds/`;
    return  this.Http.get(url);
}

 // login
  login(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/loginuser`;
    return  this.Http.post(url,data);

  }
  getToken(){
    return localStorage.getItem('token');
  }
  get_msgnotification():Observable <any> {
      console.log("@@@@ ### person data in service : ");
      let url:string = `${this.BASE_URL}/MessageNotify/`;
      return this.Http.get(url);
    }
       

  updatepersonal(data){
    let url:string = `${this.BASE_URL}/updateuser/`
    return this.Http.post(url,data)
  }
  updateeducation(data){
    let url:string=`${this.BASE_URL}/updateusereduu/`
    return this.Http.post(url,data)
  }
  showexp(data){
    console.log("im in update exxxxxxxxxxx",data)
    let url:string=`${this.BASE_URL}/updateuserexpp/`
    return this.Http.post(url,data)
  }
  addededucation(data){
     let url:string=`${this.BASE_URL}/AddQual/`
    return this.Http.post(url,data)

  }

  addedeexp(data){
     let url:string=`${this.BASE_URL}/AddExp/`
    return this.Http.post(url,data)

  }
// forgot password functions
  verifytoken(){
     console.log('hiiiiiiiiiiiiiiiiiii')
    let url:string = `${this.BASE_URL}/demo/`;
    return  this.Http.post(url,{"hi":"hi"});

  }
//forgot password, send otp to mobile number 
  forgotpassword12(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/forgot_password/`;
    return  this.Http.post(url,data);

  }

  //comment on post
  postcomment(data){
    let url:string = `${this.BASE_URL}/Comment_Box/`;
    return this.Http.post(url,data)
  }

  //get comments on post
  getComments(data){
    console.log(data)
    let url:string = `${this.BASE_URL}/Get_Comments/`;
    return this.Http.post(url,data)
  }

  //reply on comment
  PostReply(data){
    let url:string = `${this.BASE_URL}/Post_Reply/`;
    return this.Http.post(url,data)
  }

  //get rplay on comment
  getReply(data){
    console.log(data)
    let url:string=`${this.BASE_URL}/Get_Reply/`;
    return this.Http.post(url,data)
  }

  //resend otp
  resendotp1(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/forgot_password/`;
    return  this.Http.post(url,data);

  }

//verify otp for forgot password
   verify_forgotpasswordotp(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/verify_forgotpasswod_otp/`;
    return  this.Http.post(url,data);

  }

  // verify_forgotpasswordotpblock(data){
  //   console.log('hiiiiiiiiiiiiiiiiiii',data);
  //   let url:string = `${this.BASE_URL}/blockchain_verify_forgotpasswod_otp/`;
  //   return  this.Http.post(url,data);

  // }
// change new password
    confirm_passwordotp(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/reset_password/`;
    return  this.Http.post(url,data);

  }

  resetpassword(data){
    console.log('hiiiiiiiiiiiiiiiiiii',data);
    let url:string = `${this.BASE_URL}/reset_password/`;
    return  this.Http.post(url,data);

  }

// generate otp when we are registering
  generateotp(data):Observable<any>{
    console.log(data)
    return this.Http.post(`${this.BASE_URL}/generateotp/`,data);
  }

  createUser(employee):Observable<any> {
    console.log(employee)
    let url:string =`${this.BASE_URL}/register/`;
   return this.Http.post(url,employee);
 }

//upload image
 upLoadImg(imgData): Observable <any> {
    console.log("im in first serviceeeeeeeeee",imgData);
    let url: string = `${this.BASE_URL}/uploadimage/`;
    return this.Http.post(url,imgData);
  }

 //profiledata
     profiledata1(): Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/profiledata/`;
    return this.Http.get(url);
  }

 //postdata
 postdata(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee",data);
    let url: string = `${this.BASE_URL}/posts/`;
    return this.Http.post(url,data);
  }


getpostdata1():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/getpostdata/`;
    return this.Http.get(url);
  }


  remainingPostData():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/remainingPostData/`;
    return this.Http.get(url);
  }
 //increment likes
Incrementlikes1(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee",data);
    let url: string = `${this.BASE_URL}/Incrementlikes/`;
    return this.Http.post(url,data);
  }

IncrementCommentlikes1(data):Observable <any> {
    console.log("commentlike...",data);
    let url: string = `${this.BASE_URL}/IncrementCommentlikes/`;
    return this.Http.post(url,data);
  }

IncrementRplylikes1(data):Observable <any> {
  console.log("replylike.======",data);
  let url:string=`${this.BASE_URL}/IncrementRplylikes/`;
  return this.Http.post(url,data);
}
 
  //mynetwork
getnetworkmembers1():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/mynetwork/`;
    return this.Http.get(url);
  }

  getCompaniestoConnect():Observable <any> {
    console.log("im in first serviceeeeeeeeee gnc-------------------");
    let url: string = `${this.BASE_URL}/companiesToConnect/`;
    return this.Http.get(url);
  }
 
  getFriendrequest():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/Friendrequest/`;
    return this.Http.get(url);
  }

  getFriendrequestSent():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/GetrequestSent/`;
    return this.Http.get(url);
  }

  //sendfriend request
  sendfriendRequest1(data){
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/sendfriendrequest/`;
    return this.Http.post(url,data);

  }

  sendfollowRequest1(data){
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/followrequestSend/`;
    return this.Http.post(url,data);
  }


  //acceptfriend request

    addfriendrequest1(data){
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/addrequest/`;
    return this.Http.post(url,data);

  } 

  //To delete friend request
   deleteFriendRequest(data){
    console.log("im in first serviceeeeeeeeee delete",data);
    let url: string = `${this.BASE_URL}/deleteFriendRequest/`;
    return this.Http.put(url,data);
  } 

  //To delete friend request sent
  deleteFriendRequestSent(data){
    console.log("im in first serviceeeeeeeeee delete",data);
    let url: string = `${this.BASE_URL}/RequestSentDelete/`;
    return this.Http.put(url,data);
  } 

  //count of connections
  getcount_of_connections():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/Count_of_connections/`;
    return this.Http.get(url);
  }
  getUserMobileno():Observable<any>{
    console.log("oooooooooooooooooooo");
    let url: string = `${this.BASE_URL}/getmobile/`;
    return this.Http.get(url);
  }
  userrole():Observable<any>{
    console.log("roleeeeeeeeeeee iiinnnn");
    let url: string = `${this.BASE_URL}/getrole/`;
    return this.Http.get(url);
  }
  getupdaterole():Observable<any>{
    console.log("orggggggggggggggggggggggg");
    let url: string = `${this.BASE_URL}/getupdaterole/`;
    return this.Http.get(url);
  }

   sharedata(data){
    let url:string = `${this.BASE_URL}/reposts2/`;
    return this.Http.post(url,data);
  }



     userdetails123():Observable <any> {
      console.log("hiiiiiiiiiiiiiiiiiiiiii");
      let url:string = `${this.BASE_URL}/updateform/`;
      return this.Http.get(url);
      
    }
    userdetailss123():Observable <any> {
      console.log("orgggggggggggggggggggggg");
      let url:string = `${this.BASE_URL}/updateorg/`;
      return this.Http.get(url);
    }



    sendmessageser(data):Observable <any> {
      console.log("Service --- Data:", data);
      let url:string = `${this.BASE_URL}/chat/`;
      return this.Http.post(url, data);
    }

    public send_person_details;

    gettFullChatMessage(data):Observable <any> {
      console.log("Service Data for getting:", data);
      let url:string = `${this.BASE_URL}/sortedmess/`;
      return this.Http.post(url, data);
    }

    // ==>get unreaded messages<== //
    // unreadedMsgs(){
    //   console.log("unreaded msg serviceeeee");
    //   let url:string = `${this.BASE_URL}/unreadmsg/`;
    //   return this.Http.get(url);
    // }
    publicprofiledata(data):Observable <any> {
      console.log("im in search box",data);
      let url:string =`${this.BASE_URL}/Public_profile/`;
      return this.Http.post(url,data);
    }

    selectperson1(data):Observable <any> {
      console.log("@@@@ ### person data in service : ", data);
      let url:string = `${this.BASE_URL}/updateform/`;
      return this.Http.get(url);
    }


    public disablePrivateAccount(data):Observable <any>{
      let url:string = `${this.BASE_URL}/updatePrivateAccount`;
      return this.Http.post(url,data);
    }
       

    public selectedPersonComp_serv:any={};

    

}