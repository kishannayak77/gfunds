import { Injectable } from '@angular/core';
import { DataService } from '../../../data.service';
import { Observable } from 'rxjs';
import { PehachainAdmin, AdminVerification } from '../../../gowdanar.pehachain';
import { HttpClient,HttpHeaders, HttpErrorResponse} from  '@angular/common/http';
import {Globals} from '../../../globals';


@Injectable({
  providedIn: 'root'
  
})
export class AdminVerifyService {

  private NAMESPACE = 'PehachainAdmin';
  public organizationData:any = {};
  constructor(	private dataService: DataService<PehachainAdmin>,
                private dataServiceForAdminVerify:DataService<AdminVerification>,
                public http: HttpClient,
                private globals: Globals) {
  };

  private BASE_URL = this.globals.url ;

  
  public addPehachainAdmin(itemToAdd: any): Observable<PehachainAdmin>{
    console.log("userrrrr dataaaaaaaaaa in seeeeeerrvicceee",itemToAdd);
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }

  public adminTransaction(itemToAdd: any): Observable<AdminVerification> {
    console.log("userrrrr dataaaaaaaaaa in seeeeeerrvicceee",itemToAdd);
    let name_space = 'AdminVerification';
    return this.dataServiceForAdminVerify.add(name_space, itemToAdd);
  }

  getdocs():Observable <any> {
   let url:string = `${this.BASE_URL}/adminDashInfo/`;
    return  this.http.get(url);
  }

  adminDataOwnerSer():Observable <any> {
  	console.log("adminnnnnnnnnnnn")
  	let url:string = `${this.BASE_URL}/adminDataOwner/`;
  	return this.http.get(url);
  }

  adminDataVerifierSer():Observable <any> {
    console.log("adminnnnnnnnnnnn")
    let url:string = `${this.BASE_URL}/adminDataVerifier/`;
    return this.http.get(url);
  }
  allOrgsForAdmin():Observable <any> {
    console.log("adminnnnnnnnnnnn")
    let url:string = `${this.BASE_URL}/allOrgsForAdmin/`;
    return this.http.get(url);
  }

  adminChangeOrgStatus(data):Observable <any> {
    console.log('i am in initialregistraion service',data);
    let url:string = `${this.BASE_URL}/adminChangeOrgStatus/`;
    return  this.http.post(url,data);
  }

   adminSeekerPayinfo():Observable <any> {
    console.log("admin payinfooooooooooooooooooooooooooo")
    let url:string = `${this.BASE_URL}/getAdminseekpayinfo/`;
    return this.http.get(url);
  }
   
   adminVerifierPayinfo():Observable <any> {
     console.log("admin verifier payinfoooooooooooooooooooooo")
     let url:string = `${this.BASE_URL}/getAdminverifierpayinfo/`;
     return this.http.get(url);
  }
  allDocumentsForAdmin():Observable <any> {
  	console.log("adminnnnnnnnnnnn")
  	let url:string = `${this.BASE_URL}/allDocumentsForAdmin/`;
  	return this.http.get(url);
  }
  // adminGetDocsUser():Observable <any> {
  //   console.log("adminnnnnnnnnnnn")
  //   let url:string = `${this.BASE_URL}/adminGetDocsUser/`;
  //   return this.http.get(url);
  // }

  // adminDocStatusChangeInPostGre(data):Observable<any>{
  //   let url:string = `${this.BASE_URL}/adminChangeDocStatus`;
  //   return this.http.post(url,data);
  // } 
  adminDocStatusChangeInPostGre(data:any):Observable <any> {
  	console.log("adminnnnnnnnnnnn")
  	let url:string = `${this.BASE_URL}/adminChangeDocStatus/`;
  	return this.http.post(url, data);
  }

  allRegPartenerNodes():Observable <any> {
    let url:string = `${this.BASE_URL}/allRegPartenerNodes/`;
  	return this.http.get(url);
  }
  
}
