import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSeekerComponent } from './manage-seeker.component';

describe('ManageSeekerComponent', () => {
  let component: ManageSeekerComponent;
  let fixture: ComponentFixture<ManageSeekerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSeekerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSeekerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
