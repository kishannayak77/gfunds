import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent,AdminHeaderComponent } from './dashboard/dashboard.component';
import { ManageDataownerComponent } from './manage-dataowner/manage-dataowner.component';
import { ManageVerifierComponent } from './manage-verifier/manage-verifier.component';
import { ManageSeekerComponent } from './manage-seeker/manage-seeker.component';
import { ManagePaymentsComponent } from './manage-payments/manage-payments.component';
import { ManageRequestComponent } from './manage-request/manage-request.component';
import { ManagePostsComponent } from './manage-posts/manage-posts.component';
import { Globals } from '../globals';
import { LoginService } from '../Services/PublicService/login.service';
import { HttpClientModule,HttpHeaders, HttpErrorResponse,HTTP_INTERCEPTORS} from  '@angular/common/http';
import { DemoMaterialModule } from 'src/material-module';
import { TokenInterceptService } from '../Services/PublicService/token-intercept.service';
import { ManageOrgnizationsComponent, organizationDetails } from './manage-orgnizations/manage-orgnizations.component';
import { DataOwnerModule } from '../data-owner/data-owner.module';
import { VerifierPayinfoComponent } from './verifier-payinfo/verifier-payinfo.component';
import { SeekerPayinfoComponent } from './seeker-payinfo/seeker-payinfo.component';
import { DataAccessorModule } from '../data-accessor/data-accessor.module';
import { ManagePartnernodeComponent } from './manage-partnernode/manage-partnernode.component';
import { AddPartnernodeComponent } from './add-partnernode/add-partnernode.component';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';

@NgModule({
	declarations: [
	  	DashboardComponent,
		ManageDataownerComponent,
		ManageVerifierComponent,
		ManageSeekerComponent, 
		ManagePaymentsComponent,
		ManageRequestComponent,
		ManagePostsComponent,
		AdminHeaderComponent,
		ManageOrgnizationsComponent,
		organizationDetails,
		VerifierPayinfoComponent,
		SeekerPayinfoComponent,
		ManagePartnernodeComponent,
		AddPartnernodeComponent,
	],
  imports: [
    CommonModule,
		AdminRoutingModule,
		HttpClientModule,
		DemoMaterialModule,
		DataOwnerModule,
		DataAccessorModule,
		FormsModule,
		ReactiveFormsModule

	],
	entryComponents:[
	  ],
	providers: [Globals,LoginService,{
    provide:HTTP_INTERCEPTORS,useClass:TokenInterceptService,multi:true
  }],
})
export class AdminModule { }
