import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { FormControl} from '@angular/forms';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';
import { LoginService } from '../../Services/PublicService/login.service';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Router } from '@angular/router';
import { CustomService } from '../../Services/PublicService/customservices.service';
import { State } from '../../Services/PublicService/state';
import { District } from '../../Services/PublicService/district';
import { DomSanitizer } from '@angular/platform-browser';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {

  public categories =[{"catName":"Education"},{"catName":"Experience"},{"catName":"Financial"},{"catName":"Social"},{"catName":"Personal"}];
  // checked:any;
  public category1;
  public addedu;


  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  showSpinner = false;

  public courses;
  public organizations;
  get personalform() { return this.firstFormGroup.controls; }
  get personalform1() { return this.secondFormGroup.controls; }
    keyPress(event: any) {
  const pattern = /[0-9]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}
 keyPress1(event: any) {
const pattern = /[0-9]/;
let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault(); 
}
}

emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;


public errorMessage;
public userAllDocs = [];
public userDetails:any = {}
public privateUserId;
public states:State[];
public districts:District[];
public ViewDoucment;
public data25:any;

constructor(private PrivateService:PrivateService,
            private userDocsServ: UserDocsService,
            private loginService: LoginService, 
            private _formBuilder:FormBuilder,
            private router:Router,
            private customsdservice: CustomService,
            ) {}

         public requestSpinner:Boolean = false;

  ngOnInit() {
    this.states = this.customsdservice.getStates();
    this.getAllOrganizations();
    this.edu();
    this.getUserData();
    // this.router.navigate(['/DataOwner'])    

    this.firstFormGroup = this._formBuilder.group({
      // orgName: ['', Validators.required],
      // orgmobnumber: ['', Validators.required],
      orgname: ['', Validators.required],
      // orgmailid: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
      category: ['', Validators.required],
      document: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      orgpin: ['', Validators.required],
      image:['', Validators.required]
    });

    this.secondFormGroup = this._formBuilder.group({
      orgName: ['', Validators.required],
      orgmobnumber: ['', Validators.required],
      orgmailid: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
      
    });

  } 
 edu(){
   this.ViewDoucment = this.PrivateService.ViewDoucments;
   this.data25=localStorage.getItem('val');
    console.log("sgsfdgsdgsdgsdg",this.data25)
   console.log("component three variable",this.data25);
    this.addedu = {'catName':this.data25};
    


 }
  
  public getUserData(){
    console.log("im in get user data methodd")
    let updata = {"msg":"noUpdate"}
    this.loginService.disablePrivateAccount(updata).subscribe(backData =>{
      this.userDetails = backData;
      this.privateUserId = 'O'+this.userDetails['userData'][0]['user_id'].toString();
      console.log("ressspppppppp frommm ",backData);
      this.loadAll(this.privateUserId);
     },
     error => console.log("errooorr",error)
    );
  } 


  public loadAll(userId): Promise<any> {
    console.log("im in loadall methoddd",userId)
    const tempList = [];
    return this.userDocsServ.getUserAllDocs(userId)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      console.log("userrr All Docssss",result)
      result.forEach(asset => {
        tempList.push(asset);
      });
      this.userAllDocs = tempList;
      console.log("userrr All Docssss lengthhh",this.userAllDocs.length)

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }  




  public image;
  userDocumentUpload($event:any){
    this.image = $event.target.files[0];
    console.log("attach-------------------",this.image);
    // this.obj=this.selectedFile;
  } 
    
      
  // userDocumentUpload($event) : void{
  //   console.log("Selectedddd fileeeeee",$event)
  //   this.readThis($event.target);
  // }    
  // readThis(inputValue: any): void {
  //   var file:File = inputValue.files[0];
  //   var myReader:FileReader = new FileReader();
  //   console.log("rrrrrrrrrrrrrrrrrrrrrrrrr",myReader);
    
  //   myReader.onloadend = (e) => {
  //     this.image = myReader.result;
  //     console.log("ooooooooooooooooooooo",myReader.result);
  //     console.log("imggggggggggggggg",this.image,typeof(this.image));
  //   }
  //   myReader.readAsDataURL(file);
  //   // console.log("kkkkkkkkkkkkkkkkkkkkkk",myReader.readAsDataURL(file));
  //   console.log("mmmmmmmmmmmmmmmm",file);

  // }

  docInfoChange(){
      this.filteredorgs.forEach( org =>{
      console.log("forrrrrrrrrrrrr",org['org_name'],this.addedu['orgname']);
      if(this.addedu['orgname']==org['org_name']){
        if(this.addedu['orgname']=='other'){
          console.log("ifffffffffffff", this.addedu['orgName'], this.addedu['orgname']);
          this.addedu['verifierId'] = '';          
          this.addedu['orgMobile'] = "";
          this.addedu['email'] = ""; 
          this.addedu['orgName'] = "";

        } 
        else {
          console.log("ggggggggggggggggggggg",org['org_name']);
          this.addedu['verifierId'] = org["user_id"];
          this.addedu['orgMobile'] = org["cont_phone"];
          this.addedu['email'] = org["cont_email"];
          this.addedu['orgpin'] = org['pincode'];
          this.addedu['orgName'] = '';          
        }
      }
    })
  }

  
  public addDocument(){
     this.requestSpinner = true;
    console.log("addDocument", this.addedu);    
    let docNum = this.userAllDocs.length+1;
    let tdate = new Date();
    let dd = tdate.toString();
    let document = {
      $class: 'gowdanar.pehachain.UserDocs',
      'docId': this.privateUserId+'-'+ docNum.toString(),
      'documentType': this.addedu['catName'],
      'dataOwner': "resource:gowdanar.pehachain.DataOwner#"+this.privateUserId,
      'docName': this.addedu['docName'],
      'document': this.userDetails['userData'][0]['mobile_number']+this.userDetails['userData'][0]['user_id'].toString(),
      'verifierMobile': this.addedu['orgMobile'],
      'verifierEmailId': this.addedu['email'],
      'UploadedDateTime': dd,
      'verifierVerifiedDateTime': "none",
      'adminVerifiedDateTime': "none",
      'verifierStatus': "PENDING",
      'adminVerifyStatus': "PENDING"
    };

    return this.userDocsServ.addDocument(document)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      // this.loadAll(this.privateUserId); 
      this.addDocInfo(document);
          
    })
    .catch((error) => {
      if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
          this.errorMessage = error;
      }
    });

   
    
  }

  addDocInfo(document){

    let f_doc = new FormData();
    f_doc.append("docId",document['docId']);
    f_doc.append("documentType",document['documentType']);
    f_doc.append("docName",document['docName']);
    f_doc.append("document",this.image);
    f_doc.append("UploadedDateTime",document['UploadedDateTime']);
    f_doc.append("dataOwner",this.privateUserId);
    f_doc.append("verifierName",this.addedu['orgname']);
    f_doc.append("orgName",this.addedu['orgName']);
    f_doc.append("verifierMobile",this.addedu['orgMobile']);
    f_doc.append("verifierEmailId",this.addedu['email']);
    f_doc.append("verifierstate",this.addedu['state']);
    f_doc.append("verifierdistrict",this.addedu['district']);
    f_doc.append("verifierpin",this.addedu['orgpin']);
    f_doc.append("verifierid",this.addedu['verifierId']);
    f_doc.append("verifierVerifiedDateTime","none");
    f_doc.append("adminVerifiedDateTime","none");
    f_doc.append("verifierStatus","PENDING");
    f_doc.append("adminVerifyStatus","PENDING");
   
      
    this.PrivateService.addDocument(f_doc).subscribe(backData =>{
      console.log("Doc to Server..", f_doc);
      console.log("Doc to Server..", backData);
      this.requestSpinner = false;
      this.router.navigate(['/DataOwner']); 
     },
     error => console.log("errooorr",error)
    )
  }


public allRegOrgs = [];
public filteredorgs = [];
getAllOrganizations(){
  this.PrivateService.getallOrgs().subscribe(backData =>{
    this.allRegOrgs = backData;
    console.log("all orgs-----&&&&&&&&", this.allRegOrgs);
   },
   error => console.log("errooorr",error)
  )
}


public getOrgsbyType(docType) {
  this.filteredorgs = [];
  this.addedu['state'] = '';
  this.addedu['district'] = '';
  this.addedu['orgpin'] = '';
  this.addedu['orgname'] = '';
  
}

selectDist(statename){
  this.filteredorgs = []; 
  this.addedu['orgpin'] = ''; 
  this.addedu['orgname'] = '';  
  console.log("district:---", statename);
  this.districts = this.customsdservice.getDistrict().filter((item)=> item.mystate == statename);
}

public getOrgsbyDist(dist){
  console.log("@@@@@@", dist);
  this.filteredorgs = [];
  this.addedu['orgpin'] = '';
  this.addedu['orgname'] = '';  
}

public getOrgsbyPin(){
  this.addedu['orgname'] = '';    
  if(this.addedu['orgpin'].length == 6){
    console.log("@@@@@@", this.addedu['orgpin']);
    this.filteredorgs = [];
    this.allRegOrgs.forEach( org => {
      if( org['state'] == this.addedu['state'] && org['district'] == this.addedu['district']
       && org['pincode']==this.addedu['orgpin']){
        this.filteredorgs.push(org);
      }
    })
    this.filteredorgs.push({"org_name":"other"});
    console.log("filtered orgs", this.filteredorgs);
  }  
}
}



// ===> OWNER editData COMPONENT <=== //

@Component({
  selector: 'ownerEditData',
  templateUrl: './editData.html',
  styleUrls: ['./action.component.css']
})
export class EditData implements OnInit {
  public categories =[{"catName":"Education"},{"catName":"Experience"},{"catName":"Financial"},{"catName":"Social"},{"catName":"Personal"}];

  firstFormGroup: FormGroup;
  public courses;
  public organizations;
  get personalform() { return this.firstFormGroup.controls; }
    keyPress(event: any) {
  const pattern = /[0-9]/;
  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}
emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;
public errorMessage;
public userAllDocs = [];
public userDetails:any = {}
public privateUserId;
public states:State[];
public districts:District[];
public editableData : any = {};
public addedu : any = {};

  constructor(private PrivateService:PrivateService,
    private _formBuilder:FormBuilder,
    private customsdservice: CustomService,
    private loginService: LoginService,
    private userDocsServ: UserDocsService,
    private router:Router,) {}

  ngOnInit() {
    this.states = this.customsdservice.getStates();
    this.getAllOrganizations();
    this.getUserData();   
    console.log("im innn edit ngg onn init");

    this.firstFormGroup = this._formBuilder.group({
      orgName: ['', Validators.required],
      orgmobnumber: ['', Validators.required],
      orgname: ['', Validators.required],
      orgmailid: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
      category: ['', Validators.required],
      document: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      orgpin: ['', Validators.required]
    });

   this.editableData =  this.PrivateService.editData
   console.log(this.editableData)

   let data1 = {
    "docId" : this.editableData['docId']
  }
  this.PrivateService.geteditData(data1).subscribe(
    back => {
      console.log(back);
      this.addedu = back[0];
    },
    error => {
      console.log(error)
    }
  )

  this.image = this.editableData.document;
  }

  keyPress1(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault(); 
    }
    }
    
  public allRegOrgs = [];
  getAllOrganizations(){
    this.PrivateService.getallOrgs().subscribe(backData =>{
      this.allRegOrgs = backData;
      console.log("all orgs-----@@@", this.allRegOrgs);
     },
     error => console.log("errooorr",error)
    )
  }

  public filteredorgs = [];
  selectDist(statename){
    this.filteredorgs = []; 
    // this.addedu['verifier_pin'] = ''; 
    // this.addedu['verifier_name'] = '';  
    console.log("district:---", statename);
    this.districts = this.customsdservice.getDistrict().filter((item)=> item.mystate == statename);
  }

  public getOrgsbyDist(dist){
    console.log("@@@@@@", dist);
    this.filteredorgs = [];
    // this.addedu['verifier_pin'] = '';
    // this.addedu['verifier_name'] = '';  
  }

  showOganizationField :boolean = true;
  public getOrgsbyPin(){
    console.log("@@@@@@", this.addedu['verifier_pin']);
    this.showOganizationField = false
    this.addedu['verifier_name'] = '';    
    if(this.addedu['verifier_pin'].length == 6){
      console.log("@@@@@@", this.addedu['verifier_pin']);
      this.filteredorgs = [];
      this.allRegOrgs.forEach( org => {
        if( org['state'] == this.addedu['verifier_state'] && org['district'] == this.addedu['verifier_district']
         && org['pincode']==this.addedu['verifier_pin']){
          this.filteredorgs.push(org);
        }
      })
      this.filteredorgs.push({"org_name":"other"});
      console.log("filtered orgs", this.filteredorgs);
    }  
  }


  docInfoChange(){
    this.filteredorgs.forEach( org =>{
    console.log("forrrrrrrrrrrrr",org['org_name'],this.addedu['verifier_name']);
    if(this.addedu['verifier_name']==org['org_name']){
      if(this.addedu['verifier_name']=='other'){
        console.log("ifffffffffffff", this.addedu['verifier_name'], this.addedu['verifier_name']);
        this.addedu['verifier_id'] = '';          
        this.addedu['verifier_mobile'] = "";
        this.addedu['verifier_email'] = ""; 
        this.addedu['verifier_name'] = "";

      } 
      else {
        console.log("ggggggggggggggggggggg",org['org_name']);
        this.addedu['verifier_id'] = org["user_id"];
        this.addedu['verifier_mobile'] = org["phone"];
        this.addedu['verifier_email'] = org["email"];
        this.addedu['verifier_pin'] = org['pincode'];
        this.addedu['verifier_name'] = ''          
      }
    }
  })
}

public getUserData(){
  console.log("im in get user data methodd")
  let updata = {"msg":"noUpdate"}
  this.loginService.disablePrivateAccount(updata).subscribe(backData =>{
    this.userDetails = backData;
    this.privateUserId = 'O'+this.userDetails['userData'][0]['user_id'].toString();
    console.log("ressspppppppp",backData);
   },
   error => console.log("errooorr",this.privateUserId)
  );
}

public showHide : boolean = true
imageChange(){
this.showHide = false;
this.image = '';
}

public image;
userDocumentUpload($event) : void{
  console.log("Selectedddd fileeeeee",$event)
  this.readThis($event.target);
}    
readThis(inputValue: any): void {
  var file:File = inputValue.files[0];
  var myReader:FileReader = new FileReader();
  console.log("rrrrrrrrrrrrrrrrrrrrrrrrr",myReader);
  
  myReader.onloadend = (e) => {
    this.image = myReader.result;
    console.log("ooooooooooooooooooooo",myReader.result);
    console.log("imggggggggggggggg",this.image,typeof(this.image));
  }
  myReader.readAsDataURL(file);
  // console.log("kkkkkkkkkkkkkkkkkkkkkk",myReader.readAsDataURL(file));
  console.log("mmmmmmmmmmmmmmmm",file);

}

public editDocument(){
  console.log("editDocument", this.addedu);
  let tdate = new Date();
  let dd = tdate.toString();
  let document = {
    $class: 'gowdanar.pehachain.UserDocs',
    'docId': this.addedu['doc_id'],
    'documentType': this.addedu['doc_type'],
    'dataOwner': "resource:gowdanar.pehachain.DataOwner#"+this.privateUserId,
    'docName': this.addedu['doc_name'],
    'document': this.image,
    'verifierMobile': this.addedu['verifier_mobile'],
    'verifierEmailId': this.addedu['verifier_email'],
    'UploadedDateTime': dd,
    'verifierVerifiedDateTime': "none",
    'adminVerifiedDateTime': "none",
    'verifierStatus': "PENDING",
    'adminVerifyStatus': "PENDING"
  };
  console.log("****************",document);
  return this.userDocsServ.editDocument(document['docId'],document)
  .toPromise()
  .then(() => {
    this.errorMessage = null;
    // this.loadAll(this.privateUserId); 
    // this.addDocInfo(document);
    this.router.navigate(['/DataOwner']);    
  })
  .catch((error) => {
    if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
    } else {
        this.errorMessage = error;
    }
  });
 }



}



// ===> OWNER MOBILEVIEW COMPONENT <=== //

@Component({
  selector: 'ownerMobileView',
  templateUrl: './mobileview.html',
  styleUrls: ['./action.component.css']
})
export class OwnerMobileView implements OnInit {

  constructor(
    private PrivateService:PrivateService,
    private router:Router
  ) {}

  public DocmentDetails:any =[];
  ngOnInit() {
    this.DocmentDetails = this.PrivateService.mobileViewDoc;
    console.log("111111111111", this.DocmentDetails);
    
  }
}


// View DocumentComponent //

@Component({
  selector: 'ViewAllDocs',
  templateUrl: './ViewDocument.html',
  styleUrls: ['./action.component.css']
})
export class ViewDocuments implements OnInit {

  constructor(
    private PrivateService:PrivateService,
    private router:Router,
    private sanitizer:DomSanitizer,
    public dialog: MatDialog,
  ) {}
  public documentSearch:string;
  public searchdoc:string;
  p : number = 1;
  pageSize:[8,16,24];

  panelOpenState = false;

  public doucmentLists:any={};
  public doucmentType:any;
  display='none';
  base64Image;
  public ViewDoucment:any;
  ngOnInit() {
    this.ViewDoucment = this.PrivateService.ViewDoucments;
    console.log("4444444444",this.ViewDoucment)
    this.doucmentLists = this.ViewDoucment['data'];
    this.doucmentType = localStorage.getItem('val');
  }
  // viewImg(data){
  //   this.PrivateService.ViewImage = data;
  //   const privateotpref = this.dialog.open( ViewImage, {
  //     width:"1000px",
  //     height:"650px"
  //   });
 
  // }

  mobileview(data){
     this.PrivateService.mobileViewDoc = data;
     console.log("333333333",data);
     this.router.navigate(['/DataOwner/Owner_Mobile_View']);
   }

   // ===> Modal Functions <=== //

  viewdocc(data){
    console.log("popupppppppppppppp",data)
    this.PrivateService.documentv = data;
    const privateotpref = this.dialog.open( ViewImage, {
      width:"1000px",
      height:"650px"
    });
  }

onCloseHandled(){
this.display='none'; 
}

transform(){
return this.sanitizer.bypassSecurityTrustResourceUrl(this.base64Image);
}


// =========================== //

editeducation(data){
  console.log("****************",data);
  console.log("JJJJJJJJJJJJJ",data);
  this.PrivateService.editData = data;
  console.log(this.PrivateService.editData)
}
}

@Component({
  selector: 'ViewImage',
  templateUrl: './viewImages.html',
  styleUrls: ['./action.component.css']
})
export class ViewImage implements OnInit {

  constructor( private PrivateService:PrivateService,
    public dialogRef: MatDialogRef<ViewImage>  ) {}
  
  public viewImgInfo;
  ngOnInit() {    
    this.viewImgInfo=this.PrivateService.documentv;
    console.log("data viewwwwwwwwwwwwwwwwwwwwwww",this.viewImgInfo);
  }
  closeimage(){
    this.dialogRef.close();
  }
 
}
