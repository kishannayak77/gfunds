import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import {Globals} from '../../globals';

@Injectable({
  providedIn: 'root'
})
export class MypostService {
  private BASE_URL;

  constructor(private  Http:  HttpClient, private globals: Globals) {
  this.BASE_URL =  globals.url;
  }

  profiledata1(): Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/profiledata/`;
    return this.Http.get(url);
  }

  getpostdata1():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/getmypostdata/`;
    return this.Http.get(url);
  }

  Incrementlikes1(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee",data);
    let url: string = `${this.BASE_URL}/Incrementlikes/`;
    return this.Http.post(url,data);
  }

  getcount_of_connections():Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.BASE_URL}/Count_of_connections/`;
    return this.Http.get(url);
  }

  deletepostdata(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee to delete data of post",data);
    let url: string = `${this.BASE_URL}/finalpostdelete/`;
    return this.Http.post(url,data);
  }
  selectDisabled(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee to delete data of post",data);
    let url: string = `${this.BASE_URL}/disablingpost/`;
    return this.Http.post(url,data);
  }
  selectenabled(data):Observable <any> {
    console.log("im in first serviceeeeeeeeee to delete data of post",data);
    let url: string = `${this.BASE_URL}/enablingpost/`;
    return this.Http.post(url,data);
  }
  selectediting(data):Observable <any> {
    // let ddata = {id:data.id,text:data.text}
    console.log("im in first serviceeeeeeeeee",data);
    let url: string = `${this.BASE_URL}/editingpost/`;
    return this.Http.post(url,data);
  }

}
