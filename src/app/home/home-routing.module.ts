import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent,HomeUpdateorg} from './home/home.component';
import { NetworkComponent, AcceptFriends} from './network/network.component';
import { MessageComponent,MobileViewComponent } from './message/message.component';
import { MypostComponent } from './mypost/mypost.component';

import { PublicProfileComponent } from './public-profile/public-profile.component';
import { SearchPeopleComponent } from './search-people/search-people.component';
import { ManageviewComponent,ManageviewrecComponent } from './manageview/manageview.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
	{
		path:'home',
		canActivate: [AuthGuard],
		component:HomeComponent,
	    pathMatch: 'full'

		
	},
	{
		path:'updateprofile',
		canActivate: [AuthGuard],
		component:UpdateProfileComponent,
		pathMatch: 'full'
		
	},

	{
		path:'updateorg',
		canActivate: [AuthGuard],
		component:HomeUpdateorg,
		pathMatch: 'full'
		
	},
	{
		path:'network',
		canActivate: [AuthGuard],
		component:NetworkComponent
		
	},
	{
		path:'acceptfriends',
		canActivate: [AuthGuard],
		component:AcceptFriends,
	},
	{
		path:'manageview',
		canActivate: [AuthGuard],
		component:ManageviewComponent,
	},
	{
		path:'manageviewrec',
		canActivate: [AuthGuard],
		component: ManageviewrecComponent,
	  },

	{
		path:'message',
		canActivate: [AuthGuard],
		component:MessageComponent,
	},
	{
		path:'mobileview',
		canActivate: [AuthGuard],
		component:MobileViewComponent,
	},
	{
		path:'mypost',
		canActivate: [AuthGuard],
		component:MypostComponent,
	},
	{
		path:'public-profile',
		canActivate: [AuthGuard],
		component:PublicProfileComponent,
	},
	{
		path:'search-people',
		canActivate: [AuthGuard],
		component:SearchPeopleComponent,
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
