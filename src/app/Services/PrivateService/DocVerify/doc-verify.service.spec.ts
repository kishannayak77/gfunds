import { TestBed } from '@angular/core/testing';

import { DocVerifyService } from './doc-verify.service';

describe('DocVerifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocVerifyService = TestBed.get(DocVerifyService);
    expect(service).toBeTruthy();
  });
});
