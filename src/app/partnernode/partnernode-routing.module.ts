import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartnerDashboardComponent } from './partner-dashboard/partner-dashboard.component';
import { PartnerNavbarComponent } from './partner-navbar/partner-navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { ManageorgsComponent } from './manageorgs/manageorgs.component';
import { PaymentsComponent } from './payments/payments.component';
const routes: Routes = [

{
	path:'',
	component:PartnerNavbarComponent,
	children:[
		{
			path:'',
			component:PartnerDashboardComponent
		},
		{
			path:'profile',
			component:ProfileComponent
		},
		{
			path:'manageorg',
			component:ManageorgsComponent
		},
		{
			path:'payment',
			component:PaymentsComponent
		}
	]
},
        




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PartnernodeRoutingModule { }
