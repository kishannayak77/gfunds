import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import  { LoginService } from '../../Services/PublicService/login.service';
import { from } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {NgForm} from '@angular/forms';
import { CustomValidators } from '../../Services/PublicService/custom-validators';
import { RegistervalidatorComponent } from '../../registration/registervalidator/registervalidator.component';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
  data:any;
  public fuser={};
  constructor(private router:Router,private loginService:LoginService) { }

  ngOnInit() {
  }
  public test1
  public error_mobileno
  public mobileno
  mobile_number:any;


  onSubmit (mobiles): void {
    let mobile_no = {
      'mobile_number':mobiles
    }

localStorage.setItem('mobile',mobiles);
// localStorage.getItem('mobile');
console.log("in component dataaa",mobiles);
console.log("in component dataaa",localStorage.getItem('mobile'));

  this.loginService.forgotpassword12(mobile_no).subscribe(
    backendres =>{
      console.log("response from backend",backendres);
      this.test1 = backendres;
      if( this.test1.a == 1){
        console.log("forgot password otp:",this.test1.otp);
        this.router.navigate(['verify-otp']);
      }
      else{
        this.error_mobileno="Mobile Number Does Not Exist"
      }
      
    },
    error => console.log("errrroooooorrr",error)
  )

}

//validation
keyPress(event: any) {
  const pattern = /[0-9\+\-\ ]/;

  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}
login123(){
  this.router.navigate(['/']);
}
}

@Component({
  selector: 'resetpassword',
  templateUrl: './resetpassword.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ResetPasswordComponent implements OnInit {

  pwdf1=false;
  pwdf2=false;
  pwdf3=false;
  pwdf4=false;
  pwdf5=false;
  mobNum=true;
  public password:any;
  public error_message:  string;
  registrationFormGroup: FormGroup;
  public passwordFormGroup: FormGroup; 

  public registerForm: FormGroup;
  submitted = false;
  memberlandingform:any;
  private mobile = localStorage.getItem('mobileNo');
  get f() {return this.passwordFormGroup.controls;}
 
  constructor(private formBuilder: FormBuilder,
    private router: Router,
   private loginService: LoginService,) {
   this.passwordFormGroup = this.createSignupForm()
  this.passwordFormGroup = this.formBuilder.group({
      password: ['', Validators.required],
      repeatPassword: ['', Validators.required]
    }, {
      validator: RegistervalidatorComponent.validate.bind(this)
    });
    this.registrationFormGroup = this.formBuilder.group({
     
      passwordFormGroup: this.passwordFormGroup
    });
   }

createSignupForm(): FormGroup {
    return this.formBuilder.group(
      {
        repeatPassword: ['', Validators.required],
        mobile: ['', [Validators.required, Validators.minLength(10)]],
        password: [
          null,
          Validators.compose([
            Validators.required,
            // check whether the entered password has a number
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            // check whether the entered password has upper case letter
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalCase: true
            }),
            // check whether the entered password has a lower case letter
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallCase: true
            }),
            // check whether the entered password has a special character
            CustomValidators.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
              {
                hasSpecialCharacters: true
              }
            ),
            Validators.minLength(8)
          ])
        ],
      },
      
    );
  }

passwordcheck(data){
   let regexpNumber:RegExp =/\d/;
   let regexpLower:RegExp=/[a-z]/;
   let regexpUpper:RegExp=/[A-Z]/;
  let regexpSpl:RegExp =/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
  if (data.length>0){
   if (regexpNumber.test(data)){
     this.pwdf2=true;
   }
   else{
     this.pwdf2=false;
   }
   if (regexpSpl.test(data)){
     this.pwdf3=true;
   }
   else{
     this.pwdf3=false;
   }
   if (regexpUpper.test(data)){
     this.pwdf4=true;
   }
   else{
     this.pwdf4=false;
   }
   if (regexpLower.test(data)){
     this.pwdf5=true;
   }
   else{
     this.pwdf5=false;
   }

   // if (data[0]!=' ' && data[data.length-1]!=' '){
   //     this.pwd6=false;
   // }else{
   //      this.pwd6=true;
   // }
   if (data.length>7){
     this.pwdf1=true;
   }
   else{
     this.pwdf1=false;
   }
   }
   else{
      this.pwdf1=false;
      this.pwdf2=false;
      this.pwdf3=false;
      this.pwdf4=false;
      this.pwdf5=false;
      // this.pwdf6=true;
   }
 }

 

  ngOnInit() {


  }

public doesMatchPassword = false;
confirmPasswordcheck(passwordFormGroup):void {

  let repLen = (passwordFormGroup.value.repeatPassword).length;
  if (passwordFormGroup.value.password) {

    if ((passwordFormGroup.value.password).slice(0,repLen) != passwordFormGroup.value.repeatPassword) {
      this.doesMatchPassword = true;
    
    }

     else

     {
      
      this.doesMatchPassword = false;
    
    }

  } 
  else 

  {
    this.doesMatchPassword = true;
  }
 

}

  onclickRegister(data): void {
    console.log("hi..............", this.passwordFormGroup.value.password, this.passwordFormGroup.value.repeatPassword)

    console.log(localStorage.getItem('mobile'));
  
  let obj={'mobile_number':localStorage.getItem('mobile'),'password':this.passwordFormGroup.value.password}
  console.log('@@@@@@@@@@@',obj);
   this.loginService.resetpassword(obj).subscribe( data => {

      this.router.navigate(['login']);

  })
 
 
}



}






@Component({
  selector: 'verifyotp-password',
  templateUrl: './verifyotp.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class VerifyPasswordComponent implements OnInit {

  public votp={
    'otp':''
  }
  public a:any;

  constructor(private router:Router,private loginService:LoginService) { }

  ngOnInit() {

  }
    
// resend otp
resendotp(){
  let myItem = localStorage.getItem('mobile');
      console.log("in locall dataaa",myItem);
      let obj={'mobile_number':myItem}
  
    this.loginService.resendotp1(obj).subscribe(
      backendres =>{
        localStorage.removeItem('mobile');
        console.log("response from backend",backendres);
      },
      error => console.log("errrroooooorrr",error)
    )
  
  }
  public test
  public invalidotp
  enterOTP (data): void {
  
    this.loginService.verify_forgotpasswordotp(data).subscribe(
      backend =>{

         console.log("response from backend",backend);

        this.test = backend;

        if(this.test.a== 1){

        this.router.navigate(['resetpassword']);
  
        } else{

          this.invalidotp="Invalid OTP"
  
        }  

      error => console.log("errrroooooorrr",error)

      })
  }
  
  //validation
  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
  
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

}