import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { InitialregistrationService } from '../../Services/PublicService/initialregistration.service';
import { ErrorStateMatcher } from '@angular/material/core';
// import { States } from '../../Services/PublicService/states';
// import { Districts } from '../../Services/PublicService/districts';
// import  {CustomadapterService} from '../../Services/PublicService/customadapter.service';
// import { routerTransition } from '../../Services/PublicService/animation';
import { regsuccesspopup } from '../fresher-reg/fresher-reg.component';
import {MAT_DIALOG_DATA,MatDialog, MatDialogConfig,MatDialogRef} from '@angular/material';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/Services/PublicService/date.adapter';
import { CustomService} from '../../Services/PublicService/customservices.service';
import { District } from '../../Services/PublicService/district';
import { State } from '../../Services/PublicService/state';
import { Category } from '../../Services/PublicService/categorytype.service';
import { Org } from '../../Services/PublicService/org.service';
import { fbind } from 'q';
@Component({
  selector: 'app-organisation-reg',
  templateUrl: './organisation-reg.component.html',
  styleUrls: ['./organisation-reg.component.css'],
   providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
 
})
export class OrganisationRegComponent implements OnInit {

  firstFormGroup: FormGroup;
  secondFormGroup:FormGroup;
  public districts:District[];
  public state:State[];
  public orgtype:Category[];
  public orgcategory:Org[];
  // isOptional = true;
  

 url:any;
arrayBuffer:any;
error_message :string;
mobpattern=/^([6-9]){1}([0-9]){9}?$/;
ifscpattern=/^([A-Z]){4}([0-9]){7}?$/;
  emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;
  get orgform() { return this.firstFormGroup.controls }
  get orgform1() { return this.secondFormGroup.controls }
  
   keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault(); 
    }
  }
  keyPress1(event: any) {
    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  keyPress2(event: any) {
    const pattern = /[a-zA-Z ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  // keyPress2(event: any) {
  //   const pattern =/[{4}A-Z {7}0-9]/;
  //   // const pattern = /^([A-Z]){4}([0-9]){7}?$/;

  //   let inputChar = String.fromCharCode(event.charCode);
  //   if (event.keyCode != 8 && !pattern.test(inputChar)) {
  //   event.preventDefault();
  //   }
  //  }

  constructor(
    private _formBuilder: FormBuilder,
    private orgregservice:InitialregistrationService,
    private customsdservice:CustomService,
    public dialog: MatDialog, ) 
    {
      this.getuserdetails();
      this.state = this.customsdservice.getStates();  
      console.log("my states list",this.state);
      this.orgcategory = this.customsdservice.getOrgsType();
   }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      orgType: ['', Validators.required],
      orgName: ['', Validators.required],
      contEmail: ['', [Validators.required,Validators.pattern(this.emailpattern)]],
      contPhone: ['', [Validators.required,]],
      address: ['', Validators.required],
      state: ['', Validators.required],
      district: ['', Validators.required],
      pincode: ['', Validators.required],
      dirPhone:['', Validators.required],
      dirEmail:['', [Validators.required,Validators.pattern(this.emailpattern)]],
      dirName:['', Validators.required],
      contName:['', Validators.required],
      bankName:['', Validators.required],
      acNumber:['', Validators.required],
      ifscCode:['', [Validators.required, Validators.pattern(this.ifscpattern)]],
      orgCat:['', Validators.required],
      address1:[''],
    });

    this.secondFormGroup = this._formBuilder.group({
      docName: ['', Validators.required],
      docName1: ['', Validators.required],
      docName2: ['', Validators.required],
      docName3: ['', Validators.required],
      docName4: ['', Validators.required],
      docName5: ['', Validators.required],
      docName6: ['', Validators.required],
      docName7: ['', Validators.required],
      docName8: ['', Validators.required],
      docName9: ['', Validators.required],
      docName10: ['', Validators.required],
      docName11: ['', Validators.required],
      docName12: ['', Validators.required],
      docName13: ['', Validators.required],
      docName14: ['', Validators.required],
      docName15: ['', Validators.required],
      docName16: ['', Validators.required],
      docName17: ['', Validators.required],
      docName18: ['', Validators.required],
      docName19: ['', Validators.required],
      docName20: ['', Validators.required],
      docName21: ['', Validators.required],
      docName22: ['', Validators.required],
      docName23: ['', Validators.required],            
    });
    
  }

  ifscvalid(){
    let x = <HTMLInputElement>document.getElementById("ifsc");
    x.value = x.value.toUpperCase();
  }

  public BankNames:any = ["Allahabad Bank","Andhra Bank","Axis Bank","Bank of Baroda",
    "Bank of India","Bank of Maharashtra","Bandhan Bank","Canara Bank","Catholic Syrian Bank",
    "Central Bank of India","City Union Bank","Corporation Bank","DCB Bank","Dhanlaxmi Bank","Federal Bank","HDFC Bank","ICICI Bank",
    "IDFC First Bank","IDBI Bank","Indian Bank","IndusInd Bank","Indian Overseas Bank",
    "Jammu & Kashmir Bank","Karnataka Bank","Karur Vysya Bank","Kotak Mahindra Bank",
    "Lakshmi Vilas Bank","Nainital Bank","Oriental Bank of Commerce","Punjab and Sind Bank",
    "Punjab National Bank","RBL Bank", "South Indian Bank","State Bank of India","Syndicate Bank",
    "Tamilnad Mercantile Bank Limited","UCO Bank","Union Bank of India","United Bank of India","Yes Bank"]
  

  public profile:any={};

  submitorgreg() {    
    console.log("Hi i am in organisation based ts file",this.firstFormGroup.value);
    if(this.firstFormGroup.value.orgCat === "Educational Institute"){
      this.firstFormGroup.patchValue({orgCat:"Education"});
    } else if(this.firstFormGroup.value.orgCat === "Industry"){
      this.firstFormGroup.patchValue({orgCat:"Experience"});
    } else if(this.firstFormGroup.value.orgCat === "Financial Institute"){
      this.firstFormGroup.patchValue({orgCat:"Financial"});
    } else if(this.firstFormGroup.value.orgCat === "Legal Department"){
      this.firstFormGroup.patchValue({orgCat:"Social"});
    }else if(this.firstFormGroup.value.orgCat === "Personal Department"){
      this.firstFormGroup.patchValue({orgCat:"Personal"});
    }
    this.orgregservice.orgservice(this.firstFormGroup.value).subscribe(
      backendData => {
        console.log("inside if",backendData);
      },
      error => console.log("errooorr",error)
    );
  }

public getuserdetails(){
  this.orgregservice.userdetails1().subscribe(
   data=>{
      console.log("userdata in ts is..",data[0]);
      var user_mobile = data[0].mobile_number;
      this.firstFormGroup.patchValue({contPhone:user_mobile})
    }
  )
}


  public districtsn:any=[];
  getdistricts(dis){
  console.log("statezzzzzzzzzzzzzzz is",dis);
  this.districtsn = this.customsdservice.getDistrict().filter((item)=> item.mystate == dis);
 }

     getToday(): string {
      return new Date().toISOString().split('T')[0]
    }
     getDepth(outlet) {
      return outlet.activatedRouteData['depth'];
  }


  public orgtype1:any=[];
  getCategory(dis){
    console.log("statezzzzzzzzzzzzzzz is",dis);
    this.orgtype1 = this.customsdservice.getCategoryType().filter((item)=> item.org == dis);
    console.log("kjdksjkdjskdjksj",this.orgtype1)
  }



  
  // orgDocumentUpload($event,docName) : void {
  //   this.readThis($event.target,docName);
  // }    
  // readThis(inputValue: any,docName): void {
  //   var file:File = inputValue.files[0];
  //   var myReader:FileReader = new FileReader();
  //   let docInfo = {"docName":docName} 
  //   myReader.onloadend = (e) => {      
  //     docInfo['document'] = myReader.result;    
  //     this.orgregservice.submitOrgRegDocs(docInfo).subscribe( resp =>{
  //       console.log("fffff",resp)      
        
  //     },
  //     error => console.log("errooorr",error)
  //     );

  //   }
  //   myReader.readAsDataURL(file);

  // }

  submitOrgRegDocs(){
    // let data = {"docs":this.allOrgRegDocs}
    this.dialog.open(regsuccesspopup, {
      height: '243px',
      width: '465px',
    });
    // this.orgregservice.submitOrgRegDocs(data).subscribe( resp =>{
    //   console.log("fffff",resp)
    //   if(resp == "successfully_Documents_Saved"){
    //     this.dialog.open(regsuccesspopup, {
    //       height: '243px',
    //       width: '465px',
    //     });  
    //   }
    // },
    // error => console.log("errooorr",error)
    // );

  }

  public selectedFile:File;
  orgDocumentUpload($event,docName){
    this.selectedFile = <File> $event.target.files[0];
    console.log("attach-------------------",  this.selectedFile);
    let fd = new FormData();
    fd.append("document",this.selectedFile);
    fd.append("docName",docName);
    this.orgregservice.submitOrgRegDocs(fd).subscribe( resp =>{
            console.log("fffff",resp)      
            
          },
          error => console.log("errooorr",error)
          );
  }
 
  // sendmessage(message, person){

  //   this.obj=this.selectedFile;
  //    const fd = new FormData();
  //    fd.append("attachment",this.obj)
  //    fd.append("message",message['txt'])
  //    fd.append("person",person['user_id'])
  //    fd.append("full_name",person['full_name'])
  //    this.loginService.sendmessageser(fd).subscribe(backResponse => {
  //      console.log("kkkkkkkkkkkkkkkkkkkkkkkkk", fd);

  //    this.message = {};
  //    this.obj={};
  //   });

}

