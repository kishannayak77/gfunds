import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { LoginService } from '../Services/PublicService/login.service';
import { Router } from '@angular/router';
import { DataAccesserService } from '../Services/PrivateService/DataAccesser/data-accesser.service';
import { DataOwnerService } from '../Services/PrivateService/DataOwner/data-owner.service';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { PrivateService } from '../Services/PrivateService/private.service';
import { AdminVerifyService } from '../Services/PrivateService/AdminVerify/admin-verify.service';

@Component({
  selector: 'app-privateotp',
  templateUrl: './privateotp.html',
  // styleUrls: ['./privateauth.component.css']
})

export class PrivateOtp implements OnInit {
  NewFristFormGroup:FormGroup;

  get personalform(){ return this.NewFristFormGroup.controls; }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault(); 
    }
  }

  constructor(
    public dialog:MatDialog,
    public dialogRef: MatDialogRef<PrivateOtp>, 
    private router:Router,
    private loginService:LoginService,
    private dataAccessor: DataAccesserService,
    private dataOwner: DataOwnerService,
    private _formBuilder:FormBuilder,
    private PrivateService:PrivateService,
    private pehachainAdmin:AdminVerifyService,

    ) {
      // dialogRef.disableClose: true ;
    }

   
    ngOnInit() {

    this.NewFristFormGroup = this._formBuilder.group({
      newpin: ['', Validators.required],
    });

    this.hidshowpinfun();
    this.getprofilepic();    
  };


    public verify: any = {};
    public hideshowdivotp: Boolean = false;
    public hideshowdivpin: Boolean = true;

  // public otpbtn: Boolean = true;



  hidshowotpfun() {
    this.getMobilefun();
      this.hideshowdivotp = true;
      this.hideshowdivpin = false;
      document.getElementById("otpid").style.backgroundColor="#2e6da4";
      document.getElementById("pinid").style.backgroundColor="white";
      
  };

  hidshowpinfun() {
    // this.hideshowdiv = this.hideshowdiv ? false : true;
    this.hideshowdivotp = false;
      this.hideshowdivpin = true;
      document.getElementById("pinid").style.backgroundColor="#2e6da4";
      document.getElementById("otpid").style.backgroundColor='white';
      
  };

  public userMobile;
  public last4digits;
   getMobilefun(){
     this.loginService.getUserMobileno().subscribe(resp =>{
       this.userMobile = resp;
       this.last4digits = resp.slice(6,10);

       this.generateotpfun(resp)
     })
     error => console.log("errroorrr",error);
     
   }

   public potp;
   generateotpfun(mno){
     // this.otpbtn = false;
     let data = {"mobile_number":mno};
     this.loginService.forgotpassword12(data).subscribe(resp1 =>{
       this.potp = resp1['otp'];
       console.log("private data login otp:",this.potp);
     })
     error => console.log("errroorrr",error);
   }


    callotpType(pdata){
    if (pdata.length==6){
      this.otpVerify(pdata);
      
    }
  }

   public pverified = false;
   public pnotverified = false;

   otpVerify(pdata){
     let data = {"otp":pdata}
     this.loginService.verify_forgotpasswordotp(data).subscribe(resp =>{
       console.log("backendddd",resp)
      //  console.log("backendddd",resp['role'][0]['role'])
        try {

          if (resp["msg"]["message"] == "OTP verified") {
             localStorage.setItem('role',resp['role']['role'])
             this.pverified=true;
             this.pnotverified=false;

             this.dialogRef.close();

             if (resp['role']['privateAccount'] === false){
              //  this.createPrivateAccount(resp['email'],resp['role'], resp['userSecret']);
               let updata = {"msg":"update"}
               this.loginService.disablePrivateAccount(updata).subscribe(backData =>{
                 console.log("ressspppppppp",backData);
                 this.createPrivateAccount(resp['role']);
                },
                error => console.log("errooorr",error)
               );
             }
             // this.router.navigate(['PrivateDashboard'])
             if (resp['role']['role']=="experienced"){
               
                this.router.navigate(['/DataOwner'])
             }
             else if(resp['role']['role']=="organization"){
                this.router.navigate(['/DataAccessor'])
             }
           }
        }
        catch(e) {
          this.pnotverified=true;
          this.pverified=false;

        }
       
     })
     error => console.log("errroorrr",error)
   }


   closepop(){
       this.dialogRef.close();

   }

   // --->> Registering the new Participant s in Blockchain <<--- //
   public errorMessage;
   public createPrivateAccount(userData: any): Promise<any>{

    if(userData['role']=="organization"){
      let participant = {
          $class: 'gowdanar.pehachain.DataVerifierOrSeeker',
          'userId': 'V'+userData['user_id'].toString(),
          'fullName': userData['full_name'],
          'emailId': userData['email_id'],
          'mobileNo': userData['mobile_number']
      }
      return this.dataAccessor.addVerifierOrSeeker(participant)
      .toPromise()
      .then(() => {
        this.errorMessage = null;        
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else {
          this.errorMessage = error;
        }
      });      
     
    }
    else {
      let participant = {
        $class: 'gowdanar.pehachain.DataOwner',
        'userId': 'O'+userData['user_id'].toString(),
        'fullName': userData['full_name'],
        'emailId': userData['email_id'],
        'mobileNo': userData['mobile_number'],
      }
      return this.dataOwner.addDataOwner(participant)
      .toPromise()
      .then(() => {
        this.errorMessage = null;        
      })
      .catch((error) => {
        if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
        } else {
          this.errorMessage = error;
        }
      });      
    }
   }

   public profi;
   public full_name;
   getprofilepic(){
     this.loginService.profiledata1().subscribe(backResponse => { 
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrr",error)
    );

   }

   public newpin:any={};
   public pininput:boolean=true;
   public newpindiv:boolean=true;
   generatepin(){
     this.newpindiv=false;
     this.getMobilefun();

   }

   callotpPin(pdata){
    if (pdata.length==6){
      this.otpVerifypin(pdata);
      
    }
  }


  otpVerifypin(pdata){
     let data = {"otp":pdata}
     this.loginService.verify_forgotpasswordotp(data).subscribe(resp =>{
       console.log("backendddd",resp)
        try {

          if (resp["msg"]["message"] == "OTP verified") {
             this.pverified=true;
             this.pnotverified=false;            

             if (resp['role']['privateAccount'] === false){
               let updata = {"msg":"update"}
               this.loginService.disablePrivateAccount(updata).subscribe(backData =>{
                 console.log("ressspppppppp",backData);
                 this.createPrivateAccount(resp['role']);
                },
                error => console.log("errooorr",error)
               );
             }
             
             this.pininput = false;
           }
        }
        catch(e) {
          this.pnotverified=true;
          this.pverified=false;

        }
       
     })
     error => console.log("errroorrr",error)
   }

   newpinfun(pin){
     console.log("pinnnnnnnnnnnnnnnnnn", pin);
     let data:any = {"privatePin":pin}
     this.PrivateService.newpinfun1(data).subscribe(backResponse => { 
        console.log("hello");
        this.dialogRef.close();
        const privateotpref = this.dialog.open( Pinsucesspop, {
           width:"450px",           
         });

      },
      error => console.log("erroorrrr",error)
    );

   }

   public pinErrorMsg:Boolean = false;
   public pinNotSet:Boolean = false;

   verifyPinFun(pin){
     if(pin.length == 6){
      console.log("&&&&&&&&&&&",pin);
      let data ={'pin':pin}
        this.PrivateService.verifyPrivatePin(data).subscribe(backResponse => { 
          console.log("$$$$$$", backResponse);
          if (backResponse["msg"] === "verified") {
            localStorage.setItem('role',backResponse['role'])
            this.dialogRef.close();
            
             if (backResponse['role'] == "experienced"){
                this.router.navigate(['/DataOwner']);
             }
             else if(backResponse['role'] == "organization"){
                this.router.navigate(['/DataAccessor']);
             }

             
          } else if (backResponse["msg"] === "notverified") {
            this.pinErrorMsg = true;
          }
          else if (backResponse["msg"]=="FirstSetPin"){
            this.pinNotSet = true;
          }
          
        },
        error => console.log("erroorrrr",error)
      );
     }
   }

   
}

@Component({
selector: 'pinsucesspop',
templateUrl: 'pinsucesspop.html'
})
export class Pinsucesspop{
constructor(
public successpopupRef: MatDialogRef<Pinsucesspop>,
 public dialog: MatDialog,private router: Router) {}

closepopup(){
this.successpopupRef.close();
}

}


