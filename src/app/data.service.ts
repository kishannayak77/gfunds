/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Injectable } from '@angular/core';
// import { Http, Response, Headers } from '@angular/http';
import { HttpClient,HttpHeaders, HttpErrorResponse,HttpResponse} from  '@angular/common/http';
import { Observable, pipe} from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
import { environment } from '../environments/environment';
import {Globals} from './globals'


// import { shareReplay } from 'rxjs/operators';

@Injectable()
export class DataService<Type> {
    private resolveSuffix = '?resolve=true';
    // private actionUrl: string;
    private headers: HttpHeaders;
    private actionUrl:any;
    constructor(private http: HttpClient, private globals: Globals) {
    this.actionUrl = globals.urlPrivate + '/api/';
        // this.actionUrl = '/api/';
        this.headers = new HttpHeaders();
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    public getAll(ns: string): Observable<Type[]> {
        console.log('GetAll ' + ns + ' to ' + this.actionUrl + ns);
        return this.http.get(`${this.actionUrl}${ns}`)
          .pipe(map(this.extractData),
          catchError(this.handleError));
    }

    // public getSingle(ns: string, id: string): Observable<Type> {
    //     console.log('GetSingle ' + ns);

    //     return this.http.get(this.actionUrl + ns + '/' + id + this.resolveSuffix)
    //       .map(this.extractData)
    //       .catch(this.handleError);
    // }

    public add(ns: string, asset: Type): Observable<Type> {
        console.log('Entered DataService add',asset);
        console.log('Add ' + ns);
        console.log('asset', asset);

        return this.http.post(this.actionUrl + ns, asset)
            .pipe(map(this.extractData),
            catchError(this.handleError));
    }

    public update(ns: string, id: string, itemToUpdate: Type): Observable<Type> {
        console.log('Update ' + ns);
        console.log('what is the id?', id);
        console.log('what is the updated item?', itemToUpdate);
        console.log('what is the updated item?', JSON.stringify(itemToUpdate));
        return this.http.put(`${this.actionUrl}${ns}/${id}`, itemToUpdate)
        .pipe(map(this.extractData),
        catchError(this.handleError));
    }

    // public delete(ns: string, id: string): Observable<Type> {
    //     console.log('Delete ' + ns);

    //     return this.http.delete(this.actionUrl + ns + '/' + id)
    //       .map(this.extractData)
    //       .catch(this.handleError);
    // }

    public getUserDocs(id: string): Observable<Type[]> {
            console.log('Get User all docs ' );
    
            return this.http.get(this.actionUrl + 'queries/selectDocumentsByOwner?dataowner=resource%3Agowdanar.pehachain.DataOwner%23' + id)
            .pipe(map(this.extractData),
            catchError(this.handleError));
    }

    public getDocsByVerifierMobile(mobile: string): Observable<Type[]> {
        console.log("verifierrrr mobileee in data service",mobile);
        return this.http.get(this.actionUrl + 'queries/selectDocumentsByVerifierMobile?verifierMobileNo=' + mobile)
            .pipe(map(this.extractData),
            catchError(this.handleError));

        
    }

    private handleError(error: any): Observable<string> {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
          error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    private extractData(res: HttpResponse<any>): any {
        return res;
    }

}
