
import { Component,OnInit } from '@angular/core';
import{ LoginService } from './Services/PublicService/login.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import {NgForm} from '@angular/forms';
import { PrivateOtp } from './PrivateData-otp/privateotp';
// blockchain
// import { PrivateOtp } from './Blockchaincomponents/privateotp';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Globals } from './globals';
import { Newpost } from './home/Newpost/newpost';
import { InitialregistrationService } from "./Services/PublicService/initialregistration.service";
import { PrivateService } from './Services/PrivateService/private.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements  OnInit {
  title = 'PehachainFrontend';

 
  public LoginHeader ;
  public showHeader ;
  public loginHeader;
  public initialhead;
  public initialtext;
  public freshertext;
  public exptext;
  public orgtext;

getupdaterole(){
  console.log("inside update------")
  this.loginservice.getupdaterole().subscribe(
    data=>{
      console.log(data)
       if(data.role=='orignasition'){
          this.router.navigate(["/updateorg"]);
       }else{
            this.router.navigate(["/updateprofile"]);
       }
    })


}

public msg;
get_msgnotification1(){
  console.log("inside update------")
  this.loginservice.get_msgnotification().subscribe(
    data=>{
      this.msg=data
        console.log("inside update------",this.msg)
    })
}
public msge=true;
msghide(){
  this.router.navigate(['/message'])
  this.msge=false;
}

public userRolee;
 




  public user:any={};

  public data12
    // public showHeader = true;

  public ShowDashboard=false;

keyPress1(event: any) {
  const pattern = /[a-zA-Z]/;

  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}

keyPress(event: any) {
  const pattern = /[0-9\+\-\ ]/;

  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}

  ngOnInit() {
    this.get_msgnotification1();
this.getSeekerRequests1();
    this.initialRegServ.redirectToHome = "No Redirect";
    this.loginerror= false;
    // this.getFriendrequest1();
    console.log("inside ng oninit hvghgfg")
    
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        console.log(event.url,"gytgfrtdrtdrdygtf")
        if ((event.url == '/')){         
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;
        }  
        else if ((event.url == '/signup')){         
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;
        }  
        else if((event.url == '/home')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/page')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/page/copyrightpolicy')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/page/privacypolicy')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/page/useragreementpolicy')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/page/summaryofchange')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/privacyhub')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/privacysettings')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/privacyfaqs')) {
          console.log("inside else iffff")
          this.showHeader = true;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/updateprofile')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }

  else if((event.url == '/updateorg')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/network')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }

         else if((event.url == '/manageview')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }

        else if((event.url == '/manageviewrec')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }
     
      else if((event.url == '/login')) {
        console.log("inside else iffff")
        this.showHeader = false;
        this.ShowDashboard=false;
        this.loginHeader = true;
        this.initialhead = false;
       }
       else if((event.url == '/useragreementpolicy')) {
        console.log("inside else iffff")
        this.showHeader = true;
        this.ShowDashboard=false;
        this.loginHeader = false;
        this.initialhead = false;
       }
       else if((event.url == '/privacypolicy')) {
        console.log("inside else iffff")
        this.showHeader = true;
        this.ShowDashboard=false;
        this.loginHeader = false;
        this.initialhead = false;

       }
       else if((event.url == '/cookiepolicy')) {
        console.log("inside else iffff")
        this.showHeader = true;
        this.ShowDashboard=false;
        this.loginHeader = false;
        this.initialhead = false;

       }
       else if((event.url == '/copyrightpolicy')) {
        console.log("inside else iffff")
        this.showHeader = true;
        this.ShowDashboard=false;
        this.loginHeader = false;
        this.initialhead = false;



       }
        else if((event.url == '/mypost')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=true;
          this.loginHeader = false;
          this.initialhead = false;

        }
        else if((event.url == '/initialreg')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = true;
          this.initialtext = true;
          this.freshertext = false;
          this.exptext = false;
          this.orgtext = false;

        }
        else if((event.url == '/fresherreg')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = true;
          this.initialtext = false;
          this.freshertext = true;
          this.exptext = false;
          this.orgtext = false;

        }
        else if((event.url == '/expreg')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = true;
          this.initialtext = false;
          this.freshertext = false;
          this.exptext = true;
          this.orgtext = false;

        }
        else if((event.url == '/orgreg')) {
          console.log("inside else iffff")
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = true;
          this.initialtext = false;
          this.freshertext = false;
          this.exptext = false;
          this.orgtext = true;

        }
        else if (event.url == "/search-people") {
          console.log("inside else iffff");
          this.showHeader = false;
          this.ShowDashboard = true;
          this.loginHeader = false;
          this.initialhead = false;
        }

        else if (event.url == "/message") {
          console.log("inside else iffff");
          this.showHeader = false;
          this.ShowDashboard = true;
          this.loginHeader = false;
          this.initialhead = false;
        }

        else if (event.url == "/acceptfriends") {
          console.log("inside else iffff");
          this.showHeader = false;
          this.ShowDashboard = true;
          this.loginHeader = false;
          this.initialhead = false;
        }
        else if((event.url == '/public-profile')) {
          this.showHeader = false;
          this.ShowDashboard = true;
          this.loginHeader = false;
          this.initialhead = false;
          console.log("inside else iffff")
        }else if (event.url == "/Search_people") {
          console.log("inside else iffff");
          this.showHeader = false;
          this.ShowDashboard = true;
          this.loginHeader = false;
          this.initialhead = false;
        }         
        else{
          this.showHeader = false;
          this.ShowDashboard=false;
          this.loginHeader = false;
          this.initialhead = false;

        }
  }
    });
  }


        
constructor(private loginservice:LoginService,public router:Router, 
            public dialog: MatDialog, public globals: Globals,
            private initialRegServ: InitialregistrationService,
            private privateService:PrivateService,){

          }
 
public loginerror ;


refreshHome(){          
  this.initialRegServ.redirectToHome = "Redirect";
  this.router.navigate(['/message']); 
}


// refreshPublicProfile(){          
//   this.initialRegServ.redirectToPublicProfile = "Redirect1";
//   this.router.navigate(['/home']); 
// }

signin(data){  
  console.log('@@@@@@@@@@@',data)
    this.loginservice.login(data).subscribe(
    backendres =>{
      console.log("response from backend",backendres);

      this.data12=backendres;
      this.showHeader = false;
      this.ShowDashboard=true;
      localStorage.setItem('token',backendres['token'])
      if(backendres['profile'][0]['gender']){
        this.router.navigate(['home']);
      }
      else {
        this.router.navigate(['initialreg']);
      }
    },
    error => {
        console.log("errrroooooorrr",error)
        this.loginerror= error;
        this.user.password='';
  })
}
sendVal: any;
  searchPearson(data) {
    console.log("Details of Person", data);
    this.globals.matchedSearchRes = [];
    if (data == "") {
      console.log("empty...");
    } else {
      this.sendVal = { val: data };
      this.loginservice.getsearchPearson(this.sendVal).subscribe(
        backend => {
          console.log("Deta....", backend);
          this.globals.matchedSearchRes = backend;
          // this.refreshPublicProfile();
        },

        error => console.log("error for Search", error)
      );
    }
  }


 logout () {
   this.user = {};
   this.loginerror= false;
   console.log("in component dataaa");

   this.loginservice.logout11().subscribe(
     backendres =>{
       console.log("response from backend",backendres);

       localStorage.removeItem('token');
       this.globals.loggedInUserRole='';
       this.router.navigate(['/'])
       
       // window.location.reload();
   
     },
     error => console.log("errrroooooorrr",error)
      
   )

 } 
 
 public count:any;

  getFriendrequest1(){
    this.loginservice.getFriendrequest().subscribe(
      backResponse => {
        console.log('#######',backResponse.count);
        this.count=backResponse.count;      
          
      },
      error => console.log("erroorrrr",error)
    );
     error => console.log("errrroooooorrr",error)
  }

 
public selecteddata;

selectperson(data){
  console.log("person data for search in controller : ", data);
  this.loginservice.selecteddata=data;
      this.loginservice.publicprofiledata(data).subscribe(userdata => {
      console.log("uuuuuuuuuu", userdata)
      this.loginservice.searchViewProfile = userdata.personal;
      this.loginservice.searchQualification = userdata.qual;
      this.loginservice.searchExp = userdata.exp;
      this.loginservice.searchPrfPic = userdata.profile_photo;
      this.loginservice.searchFull_name = userdata.full_name;
        
  this.router.navigate(['public-profile']);
  // this.refreshPublicProfile();
      },
    error => console.log("error",error));
 
}

 // blockchain
 privateOtpVerify(data){
   console.log("#######",data);
   const privateotpref = this.dialog.open( PrivateOtp, {
     width:"450px",
     height:"425px"
   });

 }

 newpost(){
   const newpostref = this.dialog.open( Newpost, {
     width:"700px",
     height:"620px"  
   });

 }



// openDialog(): void {



//      const dialogRef = this.dialog.open(memberfinalregistrationExample, {
//       // width: '50%',
//       data: {name: this.name, spouse: this.name}
//     });

//     dialogRef.afterClosed().subscribe(result => {
//       console.log('The dialog was closed');
//       this.name = result;
//     });
  

// Notification code below; 
public pendingSeekerReq1 = [];
public getSeekerRequests1(){
    this.privateService.getnotification().subscribe(backData =>{
      
    backData.forEach(req => {
      if(req['ownerReqStatus'] === "PENDING"){
        console.log(req);
        this.pendingSeekerReq1.push(req);
      }
    }); 
     },
     error => console.log("errooorr",error)
    );
  }

}
