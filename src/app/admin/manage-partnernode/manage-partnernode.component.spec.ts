import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePartnernodeComponent } from './manage-partnernode.component';

describe('ManagePartnernodeComponent', () => {
  let component: ManagePartnernodeComponent;
  let fixture: ComponentFixture<ManagePartnernodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagePartnernodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagePartnernodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
