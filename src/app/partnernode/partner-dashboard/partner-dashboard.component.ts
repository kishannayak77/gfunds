import { Component, OnInit } from '@angular/core';
import { PrivateService } from './../../Services/PrivateService/private.service';

@Component({
  selector: 'app-partner-dashboard',
  templateUrl: './partner-dashboard.component.html',
  styleUrls: ['./partner-dashboard.component.css']
})
export class PartnerDashboardComponent implements OnInit {

  constructor(private privateserv:PrivateService) { }

  ngOnInit() {
    this.getOrgsList()
  }
  closeNav(){
    
  }

  public orgsCount:any;
  getOrgsList(){
    this.privateserv.getPartnernodedetail().subscribe(backData=>{
      this.orgsCount = backData.info.orgs_list.length;
      console.log("8888888888888", this.orgsCount);
    })
  }

}
