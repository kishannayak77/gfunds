import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FresherRegComponent } from './fresher-reg.component';

describe('FresherRegComponent', () => {
  let component: FresherRegComponent;
  let fixture: ComponentFixture<FresherRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FresherRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FresherRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
