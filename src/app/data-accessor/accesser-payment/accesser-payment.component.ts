import { Component, OnInit } from '@angular/core';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { CustomValidators } from '../../Services/PublicService/custom-validators';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
    const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

    return (invalidCtrl || invalidParent);
  }
}


@Component({
  selector: 'app-accesser-payment',
  templateUrl: './accesser-payment.component.html',
  styleUrls: ['./accesser-payment.component.css']
})
export class AccesserPaymentComponent implements OnInit {

   


  constructor(private privateService:PrivateService) { }

  panelOpenState = false;

  ngOnInit() {
  	this.getSeekerPayDetail();

    

  }

  public seekPayDetils:any = [];
  getSeekerPayDetail(){
  	this.privateService.getSeekerPayDetails().subscribe(backData =>{
      	this.seekPayDetils = backData;
      	console.log("3333333", this.seekPayDetils);
     },
     error => console.log("errooorr",error)
    );
  }



}



// ===> wallet Component <=== //

@Component({
  selector: 'wallet',
  templateUrl: './wallet.html',
  styleUrls: ['./accesser-payment.component.css']
})
export class WalletsComponent implements OnInit {

  constructor(private privateService:PrivateService) {}

  ngOnInit() {
    this.getwallet();

   	let header = document.getElementById("myDIV");
  	let btns = header.getElementsByClassName("btncls");
  	for (var i = 0; i < btns.length; i++) {
  	  btns[i].addEventListener("click", function() {
  	  var current = document.getElementsByClassName("active");
  	  current[0].className = current[0].className.replace(" active", "");
  	  this.className += " active";
  	  });
  	}
  }


  public wallet:any=[];
  public RealisedAmount = 0;
  public CreditedAmount = 0;
  getwallet(){
    this.privateService.getwalletdetails().subscribe(backData =>{
        this.wallet = backData['walletData'];
        this.wallet.forEach( org => {
          this.RealisedAmount += Number(org['realised_amount']);
        }) ;
        this.wallet.forEach(org => {
          this.CreditedAmount += Number(org['credit_amount'])
        });
     },
     error => console.log("errooorr",error)
    );
  }



}


// ===> RealizedAmount Component <=== //

@Component({
  selector: 'app-manage-seeker',
  templateUrl: './RealizedAmount.html',
  styleUrls: ['./accesser-payment.component.css']
})
export class RealizedAmountComponent implements OnInit {


public realisedata:FormGroup;
matcher = new MyErrorStateMatcher();

  constructor(private privateService:PrivateService, private formBuilder: FormBuilder) {
    this.realisedata = this.formBuilder.group({
      Name: ['', [Validators.required]],
      bank_name : ['', [Validators.required]],
      ifsc_code : ['', [Validators.required]],
      enter_amount : ['', [Validators.required]],
      account_number: ['', [Validators.required]],
      confirm_accountnum: ['']
    }, { validator: this.checkPasswords });
  }

  ngOnInit() {
    this.getRealisedWalletDetails();

    // this.realisedata = new FormGroup({
    //   Name: new FormControl('',[Validators.required]),
    //   bank_name : new FormControl('',[Validators.required]),
    //   ifsc_code : new FormControl('',[Validators.required]),
    //   account_number : new FormControl('',[Validators.required]),
    //   confirm_accountnum : new FormControl('',[Validators.required]),
    //   enter_amount : new FormControl('',[Validators.required]),
    //  });    
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.account_number.value;
    let confirmPass = group.controls.confirm_accountnum.value;

    return pass === confirmPass ? null : { notSame: true }
  }



  

  

 keyPress1(event: any) {
const pattern = /[0-9]/;
let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault(); 
}
}

keyPress(event: any) {
const pattern = /[a-zA-Z ]/;

let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault();
}
}

keyPress11(event: any) {
const pattern =/[{4}A-Z {7}0-9]/;

let inputChar = String.fromCharCode(event.charCode);
if (event.keyCode != 8 && !pattern.test(inputChar)) {
event.preventDefault();
}
}

  public resWallets:any=[];
  getRealisedWalletDetails(){
    this.privateService.getwalletdetails().subscribe(backData =>{
        this.resWallets = backData['walletData'];
        console.log("22222222222222222222", this.resWallets);
     },
     error => console.log("errooorr",error)
    );
  }
  

  walletbankdetails(){
    console.log("rlise dataaaaaaaaa",this.realisedata.value);
    this.privateService.bankdetails(this.realisedata.value).subscribe(backData=>{
    console.log("sucesssssssssssssssssssssssssss",backData);
    alert("request sent sucessfully ");

    });
  }

  public BankNames:any = ["Allahabad Bank","Andhra Bank","Axis Bank","Bank of Baroda",
"Bank of India","Bank of Maharashtra","Bandhan Bank","Canara Bank","Catholic Syrian Bank",
"Central Bank of India","City Union Bank","Corporation Bank","DCB Bank","Dhanlaxmi Bank","Federal Bank","HDFC Bank","ICICI Bank",
"IDFC First Bank","IDBI Bank","Indian Bank","IndusInd Bank","Indian Overseas Bank",
"Jammu & Kashmir Bank","Karnataka Bank","Karur Vysya Bank","Kotak Mahindra Bank",
"Lakshmi Vilas Bank","Nainital Bank","Oriental Bank of Commerce","Punjab and Sind Bank",
"Punjab National Bank","RBL Bank", "South Indian Bank","State Bank of India","Syndicate Bank",
"Tamilnad Mercantile Bank Limited","UCO Bank","Union Bank of India","United Bank of India","Yes Bank"]
}


// ===> CreditAmount Component <=== //

@Component({
  selector: 'app-manage-seeker',
  templateUrl: './CreditAmount.html',
  styleUrls: ['./accesser-payment.component.css']
})
export class CreditAmountComponent implements OnInit {

  constructor(private privateService:PrivateService) {}

  ngOnInit() {
    this.getCreditWalletDetails();
  }

  public creditWallets:any=[];
  getCreditWalletDetails(){
    this.privateService.getwalletdetails().subscribe(backData =>{
        this.creditWallets = backData['walletData'];
        console.log("22222222222222222222", this.creditWallets);
     },
     error => console.log("errooorr",error)
    );
  }

  public Cwallets:any=[];
  getcreditwalletdetails(){
    this.privateService.getwalletdetails().subscribe(backData =>{
        this.Cwallets = backData['walletData'];
        console.log("22222222222222222222", this.Cwallets);
     },
     error => console.log("errooorr",error)
    );
  }

}