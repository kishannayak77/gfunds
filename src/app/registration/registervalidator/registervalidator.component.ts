import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-registervalidator',
  templateUrl: './registervalidator.component.html',
  styleUrls: ['./registervalidator.component.css']
})
export class RegistervalidatorComponent implements OnInit {
 ngOnInit() {


  }
     static validate(registrationFormGroup: FormGroup) {
        let password = registrationFormGroup.controls.password.value;
        let repeatPassword = registrationFormGroup.controls.repeatPassword.value;
        console.log(password);
        console.log(repeatPassword);
        if (repeatPassword.length <= 0) {
           
            return null;
        }
 
        if (repeatPassword !== password) {
           
            return {
               
                doesMatchPassword: true
            };
        }
 
        return null;
 
    }
}