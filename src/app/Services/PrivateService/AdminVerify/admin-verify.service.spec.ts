import { TestBed } from '@angular/core/testing';

import { AdminVerifyService } from './admin-verify.service';

describe('AdminVerifyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdminVerifyService = TestBed.get(AdminVerifyService);
    expect(service).toBeTruthy();
  });
});
