import { Injectable } from '@angular/core';
import { DataService } from '../../../data.service';
import { Observable } from 'rxjs';
import { DataVerifierOrSeeker } from '../../../gowdanar.pehachain';

@Injectable({
  providedIn: 'root'
})
export class DataAccesserService {

  private NAMESPACE = 'DataVerifierOrSeeker';

  constructor(private dataService: DataService<DataVerifierOrSeeker>) {
  };  

  public addVerifierOrSeeker(itemToAdd: any): Observable<DataVerifierOrSeeker>{
    return this.dataService.add(this.NAMESPACE, itemToAdd);

  }

  
}
