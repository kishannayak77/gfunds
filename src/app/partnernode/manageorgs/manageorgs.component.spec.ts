import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageorgsComponent } from './manageorgs.component';

describe('ManageorgsComponent', () => {
  let component: ManageorgsComponent;
  let fixture: ComponentFixture<ManageorgsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageorgsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageorgsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
