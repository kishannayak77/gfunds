import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent,InitialregComponent, LandingComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { FresherRegComponent } from './fresher-reg/fresher-reg.component';
import { ExperienceRegComponent } from './experience-reg/experience-reg.component';
import { OrganisationRegComponent } from './organisation-reg/organisation-reg.component';
import { SharedComponent,CopyRightPolicyComponent,PageHeaderComponent,PrivacyComponent,PrivacyHubComponent,PrivacyPolicyComponent,PrivacySettingsComponent,SummaryOfChangeComponent,UserAgreementComponent } from './shared/shared.component';
import { ForgotpasswordComponent,ResetPasswordComponent,VerifyPasswordComponent } from './forgotpassword/forgotpassword.component';
import { AuthGuard } from '../auth.guard'; 


const routes: Routes = [
	{
		path: '',
		component:LandingComponent
	},
	{
		path:'signup',
		component:SignupComponent
	},
	{
		path:'login',
		component:LoginComponent
	},
	{
		path:'fresherreg',
		canActivate:[AuthGuard],
		component:FresherRegComponent,
		
	},
	{
		path:'expreg',
		canActivate:[AuthGuard],
		component:ExperienceRegComponent,
	},
	{
		path:'orgreg',
		canActivate:[AuthGuard],
		component:OrganisationRegComponent,
	},
	{
		path:'shared',
		canActivate:[AuthGuard],
		component:SharedComponent,
	},
	{
		path:'initialreg',
		canActivate:[AuthGuard],
		component:InitialregComponent,
	},
	{
		path:'forgotpassword',
		// canActivate:[AuthGuard],
		component:ForgotpasswordComponent
	},
	{
		path:'resetpassword',
		// canActivate:[AuthGuard],
		component:ResetPasswordComponent
	},
	{
		path:'verify-otp',
		// canActivate:[AuthGuard],
		component:VerifyPasswordComponent
	},
	{
    	path: 'privacyhub', 
    	component: PrivacyHubComponent, 
    },
    {
    	path: 'privacysettings', 
    	component: PrivacySettingsComponent, 
    },
    {
    	path: 'privacyfaqs', 
    	component: PrivacyComponent, 
    },
	{
    	path: 'page',
    	component: PageHeaderComponent,
    	children:[
      	{
        	path: '',  
        	component: SharedComponent
      	},
      	{
        	path: 'copyrightpolicy',  
        	component: CopyRightPolicyComponent, 
      	},
      	{
        	path: 'privacypolicy',  
        	component: PrivacyPolicyComponent, 
      	},
      	{
        	path: 'useragreementpolicy',  
        	component: UserAgreementComponent, 
      	},
      	{
        	path: 'summaryofchange',  
        	component: SummaryOfChangeComponent, 
      	},
  
    	]
  	},   
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
