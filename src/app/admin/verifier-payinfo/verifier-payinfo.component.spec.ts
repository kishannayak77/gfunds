import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifierPayinfoComponent } from './verifier-payinfo.component';

describe('VerifierPayinfoComponent', () => {
  let component: VerifierPayinfoComponent;
  let fixture: ComponentFixture<VerifierPayinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifierPayinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifierPayinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
