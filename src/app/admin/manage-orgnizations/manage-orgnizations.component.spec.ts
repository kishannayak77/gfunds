import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOrgnizationsComponent } from './manage-orgnizations.component';

describe('ManageOrgnizationsComponent', () => {
  let component: ManageOrgnizationsComponent;
  let fixture: ComponentFixture<ManageOrgnizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOrgnizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOrgnizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
