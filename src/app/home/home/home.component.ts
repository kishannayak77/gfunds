import { Component, OnInit, ViewChild } from '@angular/core';
import {LoginService} from '../../Services/PublicService/login.service';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { State } from '../../Services/PublicService/state';
import { District } from '../../Services/PublicService/district';
import { HighestQualification } from '../../Services/PublicService/highest-qualification';
import { Courses } from '../../Services/PublicService/courses';
import { NgForm, FormArray, FormControl } from '@angular/forms';
import { CustomService } from '../../Services/PublicService/customservices.service'
import * as $ from 'jquery';
import * as _moment from 'moment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import { DragScrollComponent } from 'ngx-drag-scroll/lib';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { InitialregistrationService } from "../../Services/PublicService/initialregistration.service";


export interface NoticePeriods {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  
})
export class HomeComponent implements OnInit {

  @ViewChild('nav', {read: DragScrollComponent}) ds: DragScrollComponent;ngxAutoScroll: NgxAutoScroll;
   
    // @ViewChild(NgxAutoScroll) ngxAutoScroll: NgxAutoScroll;

  startDate = new Date(1990, 0, 1);
public obj:any ={};

public selectedId;
onSelect(person){
  console.log("person.id: ", person.user_id);
  this.loginService.send_person_details=person;
}

  isCollapsed:boolean =true;
  comments: Comment[];
  postComment2: { 'reply_text': any; 'post_id': any; };
  postReply: { 'reply_text': any; 'comment_id': any; };
  
firstFormGroup:FormGroup;

 public imagePath;
   public videoPath;
  imgURL: any;
  imgURL1: any;
  videoURL: any;
  videoURL1: any;
  public message: string;
  preview(files) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.imgURL = reader.result; 
    }
    var mimeType = files[0].type;
    if (mimeType.match(/video\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.videoPath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
      this.videoURL = reader.result; 
    }
  }
   public forceScrollDown(): void {
        this.ngxAutoScroll.forceScrollDown();
    }

  constructor(public loginService:LoginService,public router:Router, 
              private formBuilder: FormBuilder, public activeRouter: ActivatedRoute,
              private initialRegServ:InitialregistrationService) 
            { 
console.log(localStorage.getItem('data'));
let data = localStorage.getItem('data')
if(data){
 this.router.navigate(['/mypost'])
 localStorage.removeItem('data')
}
            }

public show22:boolean = false;

  public buttonName:any = 'Start a Post';
  public buttonName1:any = 'Post';

   toggle() {
    this.show22 = !this.show22;

    if(this.show22)  
      this.buttonName1 = "Post";
    else
      this.buttonName = "Start a Post";
  }


  addForm: FormGroup;
  public startingImg;
  ngOnInit() {  
    console.log("imm innn homee ngOnInit ngOnInit ngOnInit");
    this.initialRegServ.redirectToHome = "No Redirect";
    this.profiledata();
    this.getpostdata12();
    // this.ngxAutoScroll();
    this.startingImg = this.profilepostdata[0];
    // this.unreadedMsg()


}


public imageUrls: any;
  public state = 'void';
  public disableSliderButtons: boolean = false;

  imageRotate(arr, reverse) {
    if (reverse) {
      for( let i=0; i<11; i++){
        arr.unshift(arr.pop());
      }
    }
    else {
      for( let i=0; i<11; i++){
        arr.push(arr.shift());
      }
    }
    return arr;
  }

  moveLeft(data) { 
    console.log("1111111111111111111111111111111" , data);
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'right';
    this.imageRotate(this.profilepostdata, true);

  }

  moveRight() {
    console.log("11111111111111111111111111");
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'left';
    this.imageRotate(this.profilepostdata, false);

  }

  onFinish($event) {
    this.state = 'void';
    this.disableSliderButtons = false;
  }

  onStart($event) {
    this.disableSliderButtons = true;
  }


// unreadedMsg(){
//   console.log("unreadeeedddejjj msg componentttttt");

//   this.loginService.unreadedMsgs().subscribe(res =>{
//     console.log("unreadeeedddejjj msg",res);
//   })
//   error => console.log("eeeeerrrrrrr",error)
// }

  public selectedFile: File = null;
  public selectedFile12: File = null;
  public profi:File = null;
  public full_name:any;
  public data:any;
  public vidoes:File = null;
  public images:File = null;
  comments1=false;
  postComment:any;
  allcomments:any;
  commentOnPost={};
  obj1=''

  ViewPost(data){
    this.obj1=data
  }

  addComments(obj) {
    obj.commentBox=!obj.commentBox;
    obj.comment_text='';
  };
  addReplys(obj){
    obj.replyBox=!obj.replyBox;
    obj.reply_text='';
  }
  public show1
  addReply(cmnt){
    this.show1=cmnt.id;
    console.log("replay",cmnt)
  };
  
  saveComments(obj){
    console.log(obj.value)
    this.comments1= true
    console.log(this.comments1)  
  }

  onFileUploaded(event){
    this.selectedFile = <File> event.target.files[0];
        const fd = new FormData();
    fd.append("image",this.selectedFile)
    this.loginService.upLoadImg(fd).subscribe(
      backResponse => {
        this.profi=backResponse;        
      },
      error => console.log("erroorrrr",error)
    );   
  }
public dis:boolean = false
  doSomething(event){
    this.dis=true;
    console.log(event);
  }

public vidshow:boolean=true
public imgshow:boolean=true
postbtn:boolean=true;
filechange(event){
  this.selectedFile = <File> event.target.files[0];
 
   
   if(this.selectedFile.size <= 5000000){
     this.postbtn=false;
     this.vidshow = false;
     this.images =this.selectedFile;
     this.show22=true;
     console.log("imagesizeeeeee......",this.images)
   }
   else{
     this.message = "The image size should be less than 5MB";
   }
  if (event.target.files[0].length === 0)
      return;
 
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = "Only images are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]); 
    reader.onload = (_event) => { 
      this.imgURL1 = reader.result; 
    }
}

onCloseclick(){
   this.imgURL1=false;
   this.videoURL1=false;
   this.vidshow = true;
   this.imgshow =true;
   this.vidoes=null;
   this.images=null;
   this.selectedFile=null;
   this.selectedFile12=null;
   if (this.str==''){
     this.postbtn=true;
   }
 }
filechange12(event){
  this.selectedFile12 = <File> event.target.files[0];
 
 if(this.selectedFile12.size <= 50000000){
  this.postbtn=false;
   this.imgshow = false;
    this.vidoes = this.selectedFile12;
    this.show22=true;
    console.log("size",this.vidoes)
    
 }
 else{
   this.message = "The video size should be less than 50MB";
 }
  if (event.target.files[0].length === 0)
      return;
 
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/video\/*/) == null) {
      this.message = "Only videos are supported.";
      return;
    }
 
    var reader = new FileReader();
    this.videoPath = event.target.files;
    reader.readAsDataURL(this.vidoes); 
    reader.onload = (_event) => { 
      this.videoURL1 = reader.result; 
    }
}

txtcontent(data){
  if (data!=''){
    console.log("c--------",data, data.target.value)
    this.str=data.target.value.trim()
    this.postbtn=false; 
  }
  else{
    if (this.images!=null){
      this.postbtn=false;
    }else if(this.vidoes!=null){
      this.postbtn=false;
    }else{
      this.postbtn=true;
    }
  }
}

postcomment(obj){
  this.postComment={
    'comment_text':obj.comment_text,
    'post_id':obj.id
  }
  this.loginService.postcomment(this.postComment).subscribe(
    backendResponse=>{
      console.log(backendResponse)
      obj.comment_list=backendResponse;
      obj.comment_text="";
    },
    error=>{
      console.log(error)
    }
  )
   obj.commentBox=!obj.commentBox;
}

postreply(obj){

  console.log('test',obj);
  this.postReply={
    'reply_text':obj.reply_text,
    'comment_id':obj.id
  }
  this.loginService.PostReply(this.postReply).subscribe(
    backendResponse=>{
      obj.reply_list=backendResponse;
      console.log("return msg",backendResponse)
      obj.reply_text="";
          },
    error=>{
      console.log(error)
    }
  )
      obj.replyBox=!obj.replyBox;

}

  profiledata(){
      this.loginService.profiledata1().subscribe(backResponse => { 
      console.log("profile data",backResponse)       
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrr",error)
    );
  }
  public profilepostdata:any = [];
  public startingPost;
  public str:string;
  getpostdata12(){
    this.str = '';
    this.vidshow = true;
    this.imgshow = true;
     this.loginService.getpostdata1().subscribe(backResponse => { 
      console.log("profile data",backResponse)       
        this.profilepostdata=backResponse;
        this.obj1=this.profilepostdata[0]
        console.log("profile data",this.profilepostdata) 
        for (var i in this.profilepostdata){
          this.profilepostdata[i].commentBox=false;
          for (var j in this.profiledata[i].comment_list){
            this.profiledata[i].comment_list[j].replyBox=false;
          }
          console.log(this.profilepostdata[i]);
        }
        this.startingPost =  this.profilepostdata[0]['profile_photo']
      },
      error => console.log("erroorrrr",error)
    );

    this.remainingPostData();
  }

  remainingPostData(){
    this.str = '';
    this.vidshow = true;
    this.imgshow = true;
     this.loginService.remainingPostData().subscribe(backResponse => { 
      console.log("remainingPostData remainingPostData",backResponse)       
        this.profilepostdata=backResponse;

        console.log("remainingPostData remainingPostData",this.profilepostdata) 
        for (var i in this.profilepostdata){
          this.profilepostdata[i].commentBox=false;
          for (var j in this.profiledata[i].comment_list){
            this.profiledata[i].comment_list[j].replyBox=false;
          }
          console.log(this.profilepostdata[i])
        } 
      },
      error => console.log("erroorrrr",error)
    );

  }

  Incrementlikes(data){
    console.log('$$$$$$$',data);
    this.loginService.Incrementlikes1(data).subscribe(backResponse => { 
      console.log("profile data",backResponse)
      data.count=backResponse.count;
      data.likedIds=backResponse.likedIds;     
      },
      error => console.log("erroorrrr",error)
    );
  }
   
  IncrementCommentlikes(data){
    console.log('@@@@',data);
    this.loginService.IncrementCommentlikes1(data).subscribe(backResponse => { 
      console.log("comment data",backResponse)
      data.count=backResponse.count;
      data.comment_likedIds=backResponse.comment_likedIds; 
      },
      error => console.log("err!!!!!!!!!",error)
    );
  }


IncrementRplylikes(data){
  console.log('####',data);
  this.loginService.IncrementRplylikes1(data).subscribe(backResponse => {
    console.log("reply data",backResponse)
    data.count=backResponse.count;
    data.reply_likedIds=backResponse.reply_likedIds;
  },
  error => console.log("error============",error)
  );
}

  public count:any;
   public talks1:any;
  getcount_of_connections1(){
    this.loginService.getcount_of_connections().subscribe(backResponse => { 
      console.log("profile data",backResponse)
      this.count=backResponse.count;
      this.talks1=backResponse.connections;
      },
      error => console.log("erroorrrr",error)
    );

  }
}

// ===================update org========




@Component({
  selector: 'app-updateorg',
  templateUrl: './updateorg.html',
  styleUrls: ['./home.component.css']
})


export class HomeUpdateorg implements OnInit {


alluserdata;

getuser1(){
  console.log("jjjjjjjjjjjjjjjjjjj");
  this.loginService.userdetails123().subscribe(userorganization =>{
     console.log("uuuuuuuuuu",userorganization)
     this.loginService.alluserdata = userorganization;
     this.router.navigate(['/updateorg'])
    });
  
}

ngOnInit(){
  console.log("innnnn ngoninittttttttttttt")
    this.getUserData1();

}


  public personaldetails
  public states:any;
  public districts:District[];
  firstFormGroup:FormGroup;

  // datePickerConfig:Partial<BsDatepickerConfig>;



  mobpattern=/^([6-9]){1}([0-9]){9}?$/;
  emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;
  constructor(public loginService:LoginService,public router:Router, 
    private _formBuilder: FormBuilder,private customsdservice: CustomService,
     public activeRouter: ActivatedRoute) {  
    this.states = this.customsdservice.getStates();
 console.log(this.states);

this.firstFormGroup=this._formBuilder.group({

      org_type: [''],
      org_name: [''],
      email: [''],
      phone: [''],
      street: [''],
      city: [''],
      state: [''],
      district: [''],
      pincode: [''],
      org_start_date:[''],
      
  
      
})


  }

public showorg=[];
 showuserorganization;
  showorganization;
  public showuserdata = {};



  getUserData1(){
  this.loginService.userdetailss123().subscribe(userdata =>
  {
    console.log("uuuuuuuuuuiiiiiiiiiiiiiii",userdata)
     this.showuserdata = userdata;
    this.showorg = userdata;
 console.log("orggggggggggggggggggggggg",this.showorg)
  });

}
 

 onSelect(statename) {
          console.log("state is",statename);
          this.districts = this.customsdservice.getDistrict().filter((item)=> item.mystate == statename);
          console.log(this.districts);
      }


}
