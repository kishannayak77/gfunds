import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import {Router,NavigationEnd, RouterModule } from '@angular/router';

@Component({
  selector: 'app-add-partnernode',
  templateUrl: './add-partnernode.component.html',
  styleUrls: ['./add-partnernode.component.css']
})
export class AddPartnernodeComponent implements OnInit {
	firstFormGroup:FormGroup;
  
  

  constructor(private _formBuilder: FormBuilder,
              private PrivateService:PrivateService,
              private router: Router ) { }

  get personalform() { return this.firstFormGroup.controls; }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  keyPress1(event: any) {
    const pattern = /[a-zA-Z ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  
    emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;


  

  ngOnInit() {

    this.getAllOrganizations();

  	this.firstFormGroup = this._formBuilder.group({
      partnername: ['',Validators.required],
      orgs: [[],Validators.required],
      email: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
      mobnum: ['',Validators.required],
      password: ['',Validators.required],
  });
}
  submitPartnerNodeReg(){
     console.log("hfsduhfuhsufhudshf",this.firstFormGroup.value);
     this.PrivateService.post(this.firstFormGroup.value).subscribe( backdata =>{
       console.log("fffff", backdata);
       alert("Partner node added success fully");
       this.router.navigate(["/Admin/Manage-partnernode"]);
     },
     error => console.log("errooorr",error)
    )
  }

  public allRegOrgs = [];
  getAllOrganizations(){
  this.PrivateService.getallOrgs().subscribe(backData =>{
    this.allRegOrgs = backData;
    console.log("all orgs-----&&&&&&&&", this.allRegOrgs);
   },
   error => console.log("errooorr",error)
  )
}

}
