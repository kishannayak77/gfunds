import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageDataownerComponent } from './manage-dataowner.component';

describe('ManageDataownerComponent', () => {
  let component: ManageDataownerComponent;
  let fixture: ComponentFixture<ManageDataownerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageDataownerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageDataownerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
