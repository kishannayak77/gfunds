import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import { NONE_TYPE } from '@angular/compiler/src/output/output_ast';
import {Globals} from '../../globals'


@Injectable({
  providedIn: 'root'
})
export class FreshersregService {
  private BASE_URL;

  constructor(private  http:  HttpClient, private globals: Globals) { 
  this.BASE_URL =  globals.url;
  }

    freshsubmit(data):Observable <any>{
      console.log('fresher reg service',data);
      console.log("ddddddd",data.personal.edu.permanent_addline1)
      if(data.personal.edu.permanent_addline1 != undefined){
        data.permanent_addline1 = "different";
      }
      else {
        data.permanent_addline1 = "same";
      }
      console.log('fresher reg service',data);
      let url:string = `${this.BASE_URL}/Fresherreg/`;
      let data1 = this.http.post(url,data);
      console.log("check data",data1);
      return data1;
    }
    userdetails():Observable <any> {
      let url:string = `${this.BASE_URL}/userdata/`;
      let mydata = this.http.get(url);
      console.log("my userdata in service",mydata);
      return mydata;
    }
}
