import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/PublicService/login.service';
import { Globals } from 'src/app/globals';

@Component({
  selector: 'app-search-people',
  templateUrl: './search-people.component.html',
  styleUrls: ['./search-people.component.css']
})
export class SearchPeopleComponent implements OnInit {

  constructor(public loginService:LoginService, public globals: Globals ) { }

  ngOnInit() {
    this.getmyFrds()
  }
  // public data:any;
  public myFriendsIds:any;
  public friendRequestRecieved:any;
  public friendRequestSent:any;
  public followingCompIds:any;
  getmyFrds(){
    this.loginService.getmyfrds().subscribe(
      backendResponse=>{
        console.log(backendResponse)
        this.myFriendsIds=backendResponse['myFriends'];
        this.friendRequestRecieved=backendResponse['requestRecieved'];
        this.friendRequestSent=backendResponse['requestsSent'];
        this.followingCompIds=backendResponse['followingCompIds']
      },
      error=>{
        console.log(error)
      }
    )
  }

   sendfriendRequest(data){
     console.log("sending REQUEST")
     console.log(data)
    this.loginService.sendfriendRequest1(data).subscribe(
      backResponse => {
        console.log('#################',backResponse);
        // this.data=backResponse; 
        this.getmyFrds(); 
      },
      error => console.log("erroorrrr",error)
    );

  }

  addfriendrequest(data){
    // let obj={"id":data.id};
    this.loginService.addfriendrequest1(data).subscribe(
      backResponse => {
        console.log('###############',backResponse);
        this.getmyFrds();
      },
      error => console.log("erroorrrr",error)
    );
  }

deleteFriendRequest1(data){
    console.log("delete request",data)
    this.loginService.deleteFriendRequest(data).subscribe(
      backResponse => {
        console.log('#######',backResponse);
        this.getmyFrds();        
      },
      error => console.log("erroorrrr",error)
    );
  }


  deleteRequestSent(data){
    console.log("ddddddddd",data)
    this.loginService.deleteFriendRequestSent(data).subscribe(
      backResponse => {
        this.getmyFrds();        
      },
      error => console.log("erroorrrr",error)
    );
  }

}
