import { Component, OnInit } from '@angular/core';
import { PrivateService } from '../../Services/PrivateService/private.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';
import { LoginService } from '../../Services/PublicService/login.service';
import { DataOwnerService } from '../../Services/PrivateService/DataOwner/data-owner.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  display='none';
  base64Image;
public categories =[{"catName":"Education"},{"catName":"Experience"},{"catName":"Financial"},{"catName":"Social"},{"catName":"Personal"}];

public errorMessage;
public userAllDocs = [];
public educationDocs = [];
public experienceDocs = [];
public financialDocs = [];
public socialDocs = [];
public personalDocs = [];

public userDetails:any = {}
public privateUserId;
public profi;
public full_name;

  constructor(private router : Router,
    private userDocsServ: UserDocsService,
    private dataOwnerServ: DataOwnerService,
    private loginService: LoginService,
    private privateService:PrivateService,
    private sanitizer:DomSanitizer,
    private dataownerService : DataOwnerService
    ) { }

  ngOnInit() {
    console.log("im innn Dataa owner ngg onn init");
    this.getUserData();
    this.getSeekerRequests();
    // this.viewDocuments();

    // this.privateService.notifypendingreq().subscribe(data =>{
      // console.log("lllllll", data);
    // })

    
  }



  public getUserData(){
    console.log("im in get user data methodd")
    let updata = {"msg":"noUpdate"}
    this.loginService.disablePrivateAccount(updata).subscribe(backData =>{
      this.userDetails = backData;
      this.privateUserId = 'O'+this.userDetails['userData'][0]['user_id'].toString();
      console.log("ressspppppppp",backData);
      this.getDocsFromPostGre();
      
     },
     error => console.log("errooorr",error)
    );
  } 

  public docDataPostGre = [];
  public getDocsFromPostGre(){
    this.privateService.getDocsFromPost().subscribe(respData => {
      console.log("dataaaaa from postgresssssss",respData);
      this.docDataPostGre = respData['userAllDo'];
      this.loadAll(this.privateUserId);
    },
    error => console.log("errooorr",error)
    );
  }

  public loadAll(userId): Promise<any> {
    console.log("im in loadall methodddddd",userId)
    const tempList = [];
    return this.userDocsServ.getUserAllDocs(userId)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      console.log("userrr All Docssss",result)
      result.forEach(asset => {
        tempList.push(asset);
        this.docDataPostGre.forEach(psDoc => {
          if (asset['docId'] == psDoc['docId']) {
            asset['document'] = psDoc['document'];
            asset['comment'] = psDoc['comment'];
            if (asset['documentType'] == "Education") {
              this.educationDocs.push(asset);
            }
            else if (asset['documentType'] == "Experience") {
              this.experienceDocs.push(asset);
            }
            else if (asset['documentType'] == "Financial") {
              this.financialDocs.push(asset);
            }
            else if (asset['documentType'] == "Social") {
              this.socialDocs.push(asset);
            }
            else  {
              this.personalDocs.push(asset);
            }
          }
        })
       
      });
      this.userAllDocs = tempList;
      console.log("this.userAllDocs",this.userAllDocs);
      console.log("this.educationDocs",this.educationDocs);
      console.log("this.experienceDocs",this.experienceDocs);
      console.log("this.financialDocs",this.financialDocs);
      console.log("this.socialDocs",this.socialDocs);
      console.log("this.personalDocs",this.personalDocs);
      this.requestsByList();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  public eduPending=[];
  public eduApproved=[];
  public eduDeclined=[];

  public expPending=[];
  public expApproved=[];
  public expDeclined=[];

  public finPending=[];
  public finApproved=[];
  public finDeclined=[];

  public socPending=[];
  public socApproved=[];
  public socDeclined=[];

  public persPending=[];
  public persApproved=[];
  public persDeclined=[];

  requestsByList(){
    this.educationDocs.forEach(eduD=>{
      if(eduD['verifierStatus'] === "PENDING"){
        this.eduPending.push(eduD);
      }
      else if(eduD['verifierStatus'] === "APPROVED"){
        this.eduApproved.push(eduD);
      }
      else if(eduD['verifierStatus'] === "DECLINED"){
        this.eduDeclined.push(eduD);
      }
    });
    this.experienceDocs.forEach(expD => {
      if(expD['verifierStatus'] === "PENDING"){
        this.expPending.push(expD);
      }
      else if(expD['verifierStatus'] === "APPROVED"){
        this.expApproved.push(expD);
      }
      else if(expD['verifierStatus'] === "DECLINED"){
        this.expDeclined.push(expD);
      }
    });
    this.financialDocs.forEach(finD => {
      if(finD['verifierStatus'] === "PENDING"){
        this.finPending.push(finD);
      }
      else if(finD['verifierStatus'] === "APPROVED"){
        this.finApproved.push(finD);
      }
      else if(finD['verifierStatus'] === "DECLINED"){
        this.finDeclined.push(finD);
      }
    });
    this.socialDocs.forEach(socD => {
      if(socD['verifierStatus'] === "PENDING"){
        this.socPending.push(socD);
      }
      else if(socD['verifierStatus'] === "APPROVED"){
        this.socApproved.push(socD);
      }
      else if(socD['verifierStatus'] === "DECLINED"){
        this.socDeclined.push(socD);
      }
    });
    this.personalDocs.forEach(persD => {
      if(persD['verifierStatus'] === "PENDING"){
        this.persPending.push(persD);
      }
      else if(persD['verifierStatus'] === "APPROVED"){
        this.persApproved.push(persD);
      }
      else if(persD['verifierStatus'] === "DECLINED"){
        this.persDeclined.push(persD);
      }
    });

  }  

  addData(){
  	console.log("add data");
  	this.router.navigate(['DataOwner/action']);
  }
  public doctype:any;
public doucmentType:any;
  // public doucmentType1:
  viewDocuments(data, type){
    console.log("%%%%%%%%%%%", data);
    this.doctype = {"data":data,"type":type}
    console.log("jhsuahfuadhfsahdj",this.doctype)
    this.privateService.ViewDoucments = this.doctype;
    localStorage.setItem('val',this.doctype.type);
    // this.doucmentType = localStorage.getItem('val')

    // this.recall(doctype);
    // this.router.navigate(['DataOwner/View_Documents'])
  }

  public allSeekerReq = [];
  public pendingSeekerReq = [];
  public approvedSeekerReq = [];
  public declinedSeekerReq = [];

  public getSeekerRequests(){
    this.privateService.getSeekerRequests().subscribe(backData =>{
      this.allSeekerReq = backData;
      console.log("reeeeeeeeeeeeeee",this.allSeekerReq);
      this.seekerReqSeparate(this.allSeekerReq);
     },
     error => console.log("errooorr",error)
    );
  }

  public seekerReqSeparate(data){
    data.forEach(req => {
      if(req['ownerReqStatus'] === "PENDING"){
        this.pendingSeekerReq.push(req);
      }
      else if(req['ownerReqStatus'] === "APPROVED"){
        this.approvedSeekerReq.push(req);
      }
      else if(req['ownerReqStatus'] === "DECLINED"){
        this.declinedSeekerReq.push(req);
      }
    });

  }
}



// ===> Header Component <=== //

@Component({
  selector: 'PrivateHeader',
  templateUrl: './PrivateHeader.html',
  styleUrls: ['./dashboard.component.css']
})
export class PrivateHeaderComponent implements OnInit {

  constructor(private router : Router,
    private privateService:PrivateService) { }

  ngOnInit() {
     this.getprofiledata();
  }

   public profi;
   public full_name;
   getprofiledata(){
     this.privateService.profiledata1().subscribe(backResponse => { 
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrrooooo",error)
    );

   }

   logoutPrivate(){
     localStorage.removeItem('role');
     this.router.navigate(['home'])
   }

}


// ===> Owner Requests Component <=== //

@Component({
  selector: 'OwnerRequests',
  templateUrl: './OwnerRequests.html',
  styleUrls: ['./dashboard.component.css']
})
export class OwnerRequestsComponent implements OnInit {

  constructor(private router : Router,
    private privateService:PrivateService) { }

  ngOnInit() {
    this.getSeekerRequests1();

      let header = document.getElementById("myDIV");
    let btns = header.getElementsByClassName("btncls");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
      });
    }

    this.notifications();
    
  } 

  public allSeekerReq1 = [];
  public pendingSeekerReq1 = [];
  public approvedSeekerReq1 = [];
  public declinedSeekerReq1 = [];

  public getSeekerRequests1(){
    this.privateService.getSeekerRequests().subscribe(backData =>{
      this.allSeekerReq1 = backData;
      console.log("reeeeeeeeeeeeeee",this.allSeekerReq1);
      this.seekerReqSeparate1(this.allSeekerReq1);
     },
     error => console.log("errooorr",error)
    );
  }

  public seekerReqSeparate1(data){
    this.pendingSeekerReq1 = [];
    this.approvedSeekerReq1 = [];
    this.declinedSeekerReq1 = [];
    data.forEach(req => {
      if(req['ownerReqStatus'] === "PENDING"){
        this.pendingSeekerReq1.push(req);
      }
      else if(req['ownerReqStatus'] === "APPROVED"){
        this.approvedSeekerReq1.push(req);
      }
      else if(req['ownerReqStatus'] === "DECLINED"){
        this.declinedSeekerReq1.push(req);
      }
    });
    this.pendingreqfun();
  }

  public ownerReqs = []
  pendingreqfun(){
    this.ownerReqs = this.pendingSeekerReq1;
    // document.getElementById("pendingbtn").style.backgroundColor="#2e6da4";
    // document.getElementById("approvedbtn").style.backgroundColor="white";
    // document.getElementById("declinedbtn").style.backgroundColor="white";
    // document.getElementById("pendingbtn").style.color="white";
    // document.getElementById("approvedbtn").style.color="black";
    // document.getElementById("declinedbtn").style.color="black";
  }
  approvereqfun(){
    this.ownerReqs = this.approvedSeekerReq1;
    // document.getElementById("pendingbtn").style.backgroundColor="white";
    // document.getElementById("approvedbtn").style.backgroundColor="#2e6da4";
    // document.getElementById("declinedbtn").style.backgroundColor="white";
    // document.getElementById("pendingbtn").style.color="black";
    // document.getElementById("approvedbtn").style.color="white";
    // document.getElementById("declinedbtn").style.color="black";
  }
  declinedreqfun(){
    this.ownerReqs = this.declinedSeekerReq1;
    // document.getElementById("pendingbtn").style.backgroundColor="white";
    // document.getElementById("approvedbtn").style.backgroundColor="white";
    // document.getElementById("declinedbtn").style.backgroundColor="#2e6da4";
    // document.getElementById("pendingbtn").style.color="black";
    // document.getElementById("approvedbtn").style.color="black";
    // document.getElementById("declinedbtn").style.color="white";
  }

  public changeReqStatus(status,id){
    let data = {"status": status,"id": id};
    this.privateService.changeReqStatus(data).subscribe(backData =>{      
      console.log("reeeeeeeeeeeeeee",backData);
      this.ngOnInit();
      
     },
     error => console.log("errooorr",error)
    );    

  }

  notifications(){
    this.privateService.ownerNotifications().subscribe(backData =>{      
    	console.log("notificationss", backData);
  	})
  }


}