import { Injectable } from '@angular/core';
import { HttpClient } from  '@angular/common/http';
import { Observable } from 'rxjs';
import {Globals} from '../../globals';

@Injectable({
  providedIn: 'root'
})
export class InitialregistrationService {
  private BASE_URL;


  constructor(private http:  HttpClient, private globals: Globals) { 
  this.BASE_URL =  globals.url;
  }
    orgservice(data):Observable <any> {
      console.log('i am in initialregistraion service',data);
      let url:string = `${this.BASE_URL}/orgregister`;
      return  this.http.post(url,data);
    }

    expregservice(data):Observable <any> {
      console.log('final data in service',data);
      if(data.personal.userData.addressline3 != undefined){
        data.addressline3 = "different";
      }
      else {
        data.addressline3 = "same";
      }
      
      let url:string = `${this.BASE_URL}/Professionalreg/`;
      return  this.http.post(url,data);
    }

    userdetails1():Observable <any> {
      let url:string = `${this.BASE_URL}/userdata/`;
      let mydata = this.http.get(url);
      console.log("my userdata in service",mydata);
      return mydata;
    }

    indivPersonalData(data):Observable <any> {
      console.log('final data in service',data);
      if(data.personal.userData.addressline3 != undefined){
        data.addressline3 = "different";
      }
      else {
        data.addressline3 = "same";
      }
      
      let url:string = `${this.BASE_URL}/indivPersonalData/`;
      return  this.http.post(url,data);
    }
    
    indivExperData(data):Observable <any> {
      console.log('final data in service',data);
      let url:string = `${this.BASE_URL}/indivExperData/`;
      return  this.http.post(url,data);
    }

    indivEducationData(data):Observable <any> {
      console.log('final data in service',data);
      let url:string = `${this.BASE_URL}/indivEducationData/`;
      return  this.http.post(url,data);
    }
    
    
    submitOrgRegDocs(data):Observable <any> {
      console.log('final data in service',data);
      let url:string = `${this.BASE_URL}/submitOrgRegDocs/`;
      return  this.http.post(url,data);
    }

    public redirectToHome: String = "No Redirect";
}
