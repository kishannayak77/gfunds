import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccesserPaymentComponent } from './accesser-payment.component';

describe('AccesserPaymentComponent', () => {
  let component: AccesserPaymentComponent;
  let fixture: ComponentFixture<AccesserPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccesserPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccesserPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
