import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MypostService } from '../../Services/PublicService/mypost.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import {LoginService} from '../../Services/PublicService/login.service';
import { DragScrollComponent } from 'ngx-drag-scroll/lib';

// import {MAT_DIALOG_DATA} from '@angular/material'
export interface DialogData {

  text: string;
}
@Component({
  selector: 'app-mypost',
  templateUrl: './mypost.component.html',
  styleUrls: ['./mypost.component.css']
})
export class MypostComponent implements OnInit {

  @ViewChild('nav', {read: DragScrollComponent}) ds: DragScrollComponent;

  onFileUploaded:any;
  constructor(public mypostservice: MypostService,public loginService:LoginService, public dialog: MatDialog) { 
    this.getpostdata12();}



  ngOnInit() {
    this.profiledata();
    
    this.getcount_of_connections1();
   }



   public imageUrls: any;
  public state = 'void';
  public disableSliderButtons: boolean = false;

  imageRotate(arr, reverse) {
    if (reverse) {
      for( let i=0; i<11; i++){
        arr.unshift(arr.pop());
      }
    }
    else {
      for( let i=0; i<11; i++){
        arr.push(arr.shift());
      }
    }
    return arr;
  }

  moveLeft() { 
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'right';
    this.imageRotate(this.profilepostdata, true);

  }

  moveRight() {
    console.log("1111111");
    if (this.disableSliderButtons) {
      return;
    }
    this.state = 'left';
    this.imageRotate(this.profilepostdata, false);

  }

  onFinish($event) {
    this.state = 'void';
    this.disableSliderButtons = false;
  }

  onStart($event) {
    this.disableSliderButtons = true;
  }


  private text;
  public selectedFile: File = null;
  public selectedFile12: File = null;
  public profi: File = null;
  public full_name: any;
  public data: any;
  public postdata = {};
  public mytext: any;
  obj1=''
showDropdown() {
    document.getElementById("myDropdown").classList.toggle("show");
  }


   addComments(obj) {
    obj.commentBox=!obj.commentBox;
    obj.comment_text='';
  };

 
  postcomment(obj){
  let postComment={
    'comment_text':obj.comment_text,
    'post_id':obj.id
  }
  this.loginService.postcomment(postComment).subscribe(
    backendResponse => {
      console.log(backendResponse)
      obj.comment_list=backendResponse;
      obj.comment_text="";
    },
    error=>{
      console.log(error)
    }
  )
}

addReplys(obj){
    obj.replyBox=!obj.replyBox;
    obj.reply_text='';
  }

profiledata() {
    this.mypostservice.profiledata1().subscribe(backResponse => {
      console.log("profile data", backResponse)
      this.profi = backResponse.profile_photo;
      this.full_name = backResponse.full_name;
    },
      error => console.log("erroorrrr", error)
    );
  }

  ViewPost(data){
    this.obj1=data
  }
public profilepostdata: any;
  getpostdata12() {
    this.mypostservice.getpostdata1().subscribe(backResponse => {
      // alert(123);
      console.log("profile data", backResponse)
      this.profilepostdata = backResponse;
      this.obj1=this.profilepostdata[0]
    },
      error => console.log("erroorrrr", error)
    );
  }
  Incrementlikes(data){
    console.log('$$$$$$$',data);
    this.loginService.Incrementlikes1(data).subscribe(backResponse => { 
      console.log("profile data",backResponse)
      data.count=backResponse.count;
      data.likedIds=backResponse.likedIds;     
      },
      error => console.log("erroorrrr",error)
    );
  }
public count: any;
  getcount_of_connections1() {
    this.mypostservice.getcount_of_connections().subscribe(backResponse => {
      console.log("profile data", backResponse)
      this.count = backResponse.count;
    },
      error => console.log("erroorrrr", error)
    );
}
  deletepost(data) {
    this.mypostservice.deletepostdata(data).subscribe(backResponse => {
      console.log("profile data", backResponse)
    this.getpostdata12();
    },
      error => console.log("erroorrrr", error)
    );
  }
  public deleteposttt:any

  delete(data){
    console.log("fordelete",data)
    this.deleteposttt=data;

   const dialogRef = this.dialog.open( deletemypost, {
     width:"450px",
     height:"30%",
      data: {
     dataKey:  this.deleteposttt
    
  }

   });

 }
  selectDisabled(data) {
    console.log("im in ts part", data)
this.mypostservice.selectDisabled(data).subscribe(backResponse => {
      data.comment=backResponse.comment;
      console.log("profile data", backResponse)
    }
    );
  }
selectenabled(data) {
    console.log("im in ts part", data)
this.mypostservice.selectenabled(data).subscribe(backResponse => {
      data.comment=backResponse.comment;
      console.log("profile data", backResponse)
    }
    );
  }
  public editeddd: any = {};
  editing(data): void {
    console.log("incoming data is",data)
    const dialogConfig = new MatDialogConfig();
    dialogConfig.data = data;
    dialogConfig.width='600px';
    // dialogConfig.height='210px';
    console.log("editing data is", data);
    const dialogRef = this.dialog.open(editmypost, dialogConfig);
   dialogRef.afterClosed().subscribe(result => {
      this.postdata = { 'id': data.id, 'result': result }
      console.log('The dialog was closed', this.postdata);
      this.mypostservice.selectediting(this.postdata).subscribe(backResponse => {
        console.log("profile data", backResponse)
      });
    });
  }

public userRolee : any;
 sharepop11(data){
console.log('@@@@@@@@@',data);
  this.loginService.sharedata(data).subscribe(
      backResponse => {
        console.log("hgfffffghfgjty",backResponse);
        this.getpostdata12();
      },
      error =>{ 
       console.log("you already shared this post")
      });
}



}




@Component({
  selector: 'editmypost',
  templateUrl: 'editmypost.html',
})
export class editmypost {
  editFormGroup: FormGroup;
constructor(
    public dialogRef: MatDialogRef<editmypost>,
    private _formBuilder: FormBuilder,
    public mypostservice: MypostService,
    @Inject(MAT_DIALOG_DATA) public data: any) 
    {
      this.editFormGroup = this._formBuilder.group({
      edittext: [''],
    });
    }
onNoClick(): void {
    this.dialogRef.close();
  }
}







@Component({
  selector: 'deletemypost',
  templateUrl: 'deletemypost.html',
})
export class deletemypost {
  public full_name: any;
  obj1=''
  editFormGroup: FormGroup;
constructor(
    public dialogRef: MatDialogRef<deletemypost>,
    private _formBuilder: FormBuilder,
    public mypostservice: MypostService,
    private router:Router,
    @Inject(MAT_DIALOG_DATA) public data: any) 
    {
    }
    ngOnInit() {
    this.getpostdata12();
    console.log(this.data)
   }
   ViewPost(data){
    this.obj1=data
  }
public profilepostdata: any;
  getpostdata12() {
    this.mypostservice.getpostdata1().subscribe(backResponse => {
      this.profilepostdata = backResponse;
      this.obj1=this.profilepostdata[0];
      console.log("profile data", backResponse)
    },
      error => console.log("erroorrrr", error)
    );
  }
    deletepost(data) {
      console.log("photooopost",data);
    this.mypostservice.deletepostdata(data).subscribe(backResponse => {
    console.log("profile data", backResponse)
    // this.getpostdata12();
     localStorage.setItem('data', backResponse);
    this.dialogRef.close();
    this.router.navigate(['/home'])
    },
      error => console.log("erroorrrr", error)
    );
  }
onNoClick(): void {
    this.dialogRef.close();
  }
}