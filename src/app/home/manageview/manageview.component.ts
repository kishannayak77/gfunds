import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/PublicService/login.service';

@Component({
  selector: 'app-manageview',
  templateUrl: './manageview.component.html',
  styleUrls: ['./manageview.component.css']
})
export class ManageviewComponent implements OnInit {
d:any;
  constructor(public loginService:LoginService) { }

  ngOnInit() {
    this.select('1')
    this.getFriendrequest1();
  } 
public selected;
public buttonColor;
public text1;
public tabColor2;
public text2;
public tabColor3;
public text3;
public tabColor4;
public text4;


select(d){             
   
  console.log("@@@@@@@", d);
   if(d=='1'){
     console.log("ddddddddddd111111",d);
    this.selected=d;
    this.buttonColor = "#337ab7";
    this.text1 = 'white';
    this.tabColor2 = 'white';
    this.text2 = "#337ab7";
    this.tabColor3 = 'white';
    this.text3 = "#337ab7";
    this.tabColor4 = 'white';
    this.text4 = "#337ab7";
    
  }

   else if(d=='2'){
     console.log("ddddddddddd22222",d);
    this.selected=d;
    this.buttonColor = 'white';
    this.text1 = "#337ab7";
    this.tabColor2 = "#337ab7";
    this.text2 = 'white';
    this.tabColor3 = 'white';
    this.text3 = "#337ab7";
    this.tabColor4 = 'white';
    this.text4 = "#337ab7";
  }

   else if(d=='3'){
      console.log("ddddddddddd33333",d);
    this.selected=d;
    this.buttonColor = 'white';
    this.text1 ="#337ab7";//desired Color
    this.tabColor2 = 'white';
    this.text2 = "#337ab7";
    this.tabColor3 = "#337ab7";
    this.text3 = 'white';
    this.tabColor4 = 'white';
    this.text4 = "#337ab7";
  }

   else if (d=='4'){
      console.log("ddddddddddd444444",d);
    this.selected=d;
    this.buttonColor = 'white' ;
    this.text1 ="#337ab7";
    this.tabColor2 = 'white';
    this.text2 = "#337ab7";
    this.tabColor3 = 'white';
    this.text3 = "#337ab7"; //desired Color
    this.tabColor4 = "#337ab7";
    this.text4 = 'white';
  }

}


requestdata:any=[];

getFriendrequest1(){
  this.loginService.getFriendrequest().subscribe(
    backResponse => {
      console.log('#######',backResponse);
      this.requestdata=backResponse.requestdetails;        
    },
    error => console.log("erroorrrr",error)
  );
}

deleteFriendRequest(data){
  console.log("delete request",data)
  this.loginService.deleteFriendRequest(data).subscribe(
    backResponse => {
      console.log('#######',backResponse);
      this.getFriendrequest1();
    },
    error => console.log("erroorrrr",error)
  );
}

addfriendrequest(data){
  this.loginService.addfriendrequest1(data).subscribe(
    backResponse => {
      console.log('#######',backResponse);
      this.getFriendrequest1();
    },
    error => console.log("erroorrrr",error)
  );
}

}


@Component({
  selector: 'app-manageviewrec',
  templateUrl: './manageviewrec.component.html',
  styleUrls: ['./manageview.component.css']
})
export class ManageviewrecComponent implements OnInit {

  constructor(public loginService:LoginService) { }

  ngOnInit() {
    this.getRequestSent()
  }

  requestSent:any=[];
  getRequestSent(){
    this.loginService.getFriendrequestSent().subscribe(
      backResponse => {
        console.log('#######',backResponse);
        this.requestSent=backResponse.requestsentdetails;        
      },
      error => console.log("erroorrrr",error)
    );
  }

  
  deleteRequestSent(data){
    console.log("ddddddddd",data)
    this.loginService.deleteFriendRequestSent(data).subscribe(
      backResponse => {
        this.getRequestSent()       
      },
      error => console.log("erroorrrr",error)
    );
  }
  
}
