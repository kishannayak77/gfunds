import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationRegComponent } from './organisation-reg.component';

describe('OrganisationRegComponent', () => {
  let component: OrganisationRegComponent;
  let fixture: ComponentFixture<OrganisationRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
