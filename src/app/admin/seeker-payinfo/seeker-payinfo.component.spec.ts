import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeekerPayinfoComponent } from './seeker-payinfo.component';

describe('SeekerPayinfoComponent', () => {
  let component: SeekerPayinfoComponent;
  let fixture: ComponentFixture<SeekerPayinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeekerPayinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeekerPayinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
