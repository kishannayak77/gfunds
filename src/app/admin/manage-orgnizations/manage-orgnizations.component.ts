import { Component, OnInit } from '@angular/core';
import { AdminVerifyService } from '../../Services/PrivateService/AdminVerify/admin-verify.service';
import { LoginService } from '../../Services/PublicService/login.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { Globals } from '../../globals';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { ViewImage } from '../../data-owner/action/action.component';

@Component({
  selector: 'app-manage-orgnizations',
  templateUrl: './manage-orgnizations.component.html',
  styleUrls: ['./manage-orgnizations.component.css']
})
export class ManageOrgnizationsComponent implements OnInit {
  panelOpenState = false;
  public orgsData:any = [];

  constructor( private adminVerifyService:AdminVerifyService,
    private globals:Globals,
    private loginservice:LoginService,
    private router: Router) { }

  ngOnInit() {
    // console.log("immm in manage orggggggg");
    this.allOrgsForAdmin();

    let header = document.getElementById("myDIV");
  	let btns = header.getElementsByClassName("btncls");
  	for (var i = 0; i < btns.length; i++) {
  	  btns[i].addEventListener("click", function() {
  	  var current = document.getElementsByClassName("active");
  	  current[0].className = current[0].className.replace(" active", "");
  	  this.className += " active";
  	  });
  	}
  }
  
  public pendingOrgs:any= [];
  public approvedOrgs:any = [];
  public declinedOrgs:any =[];

  allOrgsForAdmin(){
    this.adminVerifyService.allOrgsForAdmin().subscribe(resp => {
      console.log("dataaaaaaaaaaaaa",resp);
      // this.orgsData = resp;
      resp.forEach( obj =>{
        if(obj['admin_status'] == "Pending"){
          this.pendingOrgs.push(obj)
        }
        else if(obj['admin_status'] == "Approved"){
          this.approvedOrgs.push(obj)
        }
        else if(obj['admin_status'] == "Declined"){
          this.declinedOrgs.push(obj)
        }
        else{
          console.log("No Organizations");         
        }
      });
      this.pendingOrgsList();
    },
    error => console.log("errooorrrrr",error)
    );
  }

  pendingOrgsList(){
    this.orgsData=this.pendingOrgs;
    console.log("111111", this.orgsData);
  }
  approvedOrgsList(){
    this.orgsData=this.approvedOrgs;
        console.log("111111", this.orgsData);

  }
  declinedOrgsList(){
    this.orgsData=this.declinedOrgs;
        console.log("111111", this.orgsData);
  }

  openOrgFullDetails(data){
    this.adminVerifyService.organizationData = data;
    this.router.navigate(['Admin/orgData']);
  }


}



// ===> Organization FUll Details Component <=== //

@Component({
  selector: 'Organization-Details',
  templateUrl: './organization-details.html',
  styleUrls: ['./manage-orgnizations.component.css']
})
export class organizationDetails implements OnInit {
  public orgDetails:any=[];
  constructor(  private adminVerifyService:AdminVerifyService,
                private privateService:PrivateService,
                public dialog: MatDialog,
                private router: Router) { }

  ngOnInit() {
    this.orgDetails = this.adminVerifyService.organizationData;
  }

  viewOrgDoc(data){
    console.log("popupppppppppppppp",data)
    this.privateService.documentv = data;
    const privateotpref = this.dialog.open( ViewImage, {
      width:"1000px",
      height:"700px"
    });
  }

  adminChangeOrgStatus(adStatus){
    console.log("33333333333333", adStatus);
    let data = {"status": adStatus, "userId": this.orgDetails.user_id};
    console.log("55555", data);
    this.adminVerifyService.adminChangeOrgStatus(data).subscribe(resp => {
      console.log("dataaaaaaaaaaaaa",resp);
      this.router.navigate(['Admin/manage-orgnizations']);
    },
    error => console.log("errooorrrrr",error)
    );
  }
  

}