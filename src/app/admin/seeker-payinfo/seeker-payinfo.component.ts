import { Component, OnInit } from '@angular/core';
import { AdminVerifyService } from '../../Services/PrivateService/AdminVerify/admin-verify.service'


@Component({
  selector: 'app-seeker-payinfo',
  templateUrl: './seeker-payinfo.component.html',
  styleUrls: ['./seeker-payinfo.component.css']
})
export class SeekerPayinfoComponent implements OnInit {

  constructor(private adminservice:AdminVerifyService) { }
 // public adminPayinfo;
  ngOnInit() {
  	this.adminSeekerPayinfo();
  }

public adminPayinfo:any = [];

adminSeekerPayinfo(){
	this.adminservice.adminSeekerPayinfo().subscribe(backData=>{
    console.log("adminseekerpayinfo",backData)
     this.adminPayinfo=backData;


	});
	}
// openPayinfoFullDetails(data){
//     this.adminVerifyService.payinfo = data;
//     this.router.navigate(['Admin/payInfo']);
//   }

}


