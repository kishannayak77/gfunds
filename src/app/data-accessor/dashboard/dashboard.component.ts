import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { FormControl} from '@angular/forms';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';
import { DocVerifyService } from '../../Services/PrivateService/DocVerify/doc-verify.service';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(
              private _formBuilder:FormBuilder,
              private router: Router,
              private privateService:PrivateService,
              private userDocServ: UserDocsService,
              private docVerifyServ: DocVerifyService,
              public dialog: MatDialog ) { }

  ngOnInit() {
    this.verifierfun();
  }

  public verifierdata : boolean = true;

  verifierfun() {
  	this.verifierdata = true;
	  document.getElementById("verifier").style.backgroundColor="#2e6da4";
	  document.getElementById("seeker").style.backgroundColor="white";
	  document.getElementById("verifier").style.color="white";
	  document.getElementById("seeker").style.color="black";
    this.router.navigate(['/DataAccessor']);
  };

  seekerfun() {
  		this.verifierdata  = false;
      document.getElementById("seeker").style.backgroundColor="#2e6da4";
      document.getElementById("verifier").style.backgroundColor='white';
      document.getElementById("seeker").style.color="white";
      document.getElementById("verifier").style.color="black";
      this.router.navigate(['/DataAccessor/Seeker']);
  };
}






@Component({
  selector: 'org-header',
  templateUrl: './orgheader.html',
  styleUrls: ['./dashboard.component.css']
})
export class OrgHeaderComponent implements OnInit {

  constructor(private router : Router,
    private privateService:PrivateService) { }

  ngOnInit() {
     this.getprofiledata();
  }

   public profi;
   public full_name;
   getprofiledata(){
     this.privateService.profiledata1().subscribe(backResponse => { 
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrrooooo",error)
    );

   }

   logoutPrivate(){
    localStorage.removeItem('role');
    this.router.navigate(['home'])
  }

}