import { Component, OnInit } from '@angular/core';
import { PrivateService } from './../../Services/PrivateService/private.service';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { Subscriber } from 'rxjs';
import { LoginService } from './../../Services/PublicService/login.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	partnerform: FormGroup;

  constructor(public formBuilder:FormBuilder,private privateserv:PrivateService,public loginService:LoginService) { }

  ngOnInit() {
		this.PartnernodeDetail();

  	this.partnerform = this.formBuilder.group({
      partnername: ['',Validators.required],
      mobile_number:['',Validators.required],
      email:['',Validators.required],
  });




	}
 public partnernode :any ={};
 PartnernodeDetail(){
 this.privateserv.getPartnernodedetail().subscribe(backData=>{
	console.log("geuuuuuuuuuuuuuuuuus",backData)
	this.partnernode=backData["info"];
	this.profi=backData["profile"];
	console.log("getpartnerdeatils",this.partnernode)
 })
 }

 public profi:any;
 public selectedFile;
 onProfileUpload(event){
		 this.selectedFile = <File> event.target.files[0];
		 const fd = new FormData();
		 fd.append("image",this.selectedFile)
		 this.loginService.upLoadImg(fd).subscribe(
			 backResponse => {
			 	console.log("ressssssssssssss",backResponse)
				 this.profi=backResponse;
			 },
			 error => console.log("erroorrrr",error)
		 );
	 }



	public editableData:any;

	editData(id){
		this.editableData = id;
	}

	cancel(){
		this.editableData = '';
	}


	saveData(partnernode){
     console.log("dattaaaaaaa editing",partnernode)
     this.privateserv.savePdata(partnernode).subscribe(data=>{
     	this.editableData = '';
	});

 }
}




