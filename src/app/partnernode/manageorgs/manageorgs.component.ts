import { Component, OnInit } from '@angular/core';
import { PrivateService } from './../../Services/PrivateService/private.service';


@Component({
  selector: 'app-manageorgs',
  templateUrl: './manageorgs.component.html',
  styleUrls: ['./manageorgs.component.css']
})
export class ManageorgsComponent implements OnInit {

	public orgsList: any = [];

  constructor(private privateserv:PrivateService) { }

  ngOnInit() {
  	this.getOrgsDetails();
  }

  getOrgsDetails(){
  	this.privateserv.getPartnernodedetail().subscribe(backData=>{
  		console.log(backData);
  		this.orgsList =  backData.info.orgsData;
  		console.log(this.orgsList);
  	})
  }

}
