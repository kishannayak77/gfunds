import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPartnernodeComponent } from './add-partnernode.component';

describe('AddPartnernodeComponent', () => {
  let component: AddPartnernodeComponent;
  let fixture: ComponentFixture<AddPartnernodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPartnernodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPartnernodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
