import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DemoMaterialModule} from '../../material-module';
import { DataAccessorRoutingModule } from './data-accessor-routing.module';
import { DashboardComponent, OrgHeaderComponent} from './dashboard/dashboard.component';
import { DataService } from '../data.service';
import { VerifierDashboardComponent, DocView, ApproveDeclineModel} from './verifier-dashboard/verifier-dashboard.component';
import { SeekerDashboardComponent, ownerFullInfoComponent, SeekerDocView, Sendrequestpopup } from './seeker-dashboard/seeker-dashboard.component';
import { AccesserPaymentComponent, WalletsComponent, RealizedAmountComponent, CreditAmountComponent  } from './accesser-payment/accesser-payment.component';
import { FilterPipeValue } from '../filtervalue.pipe';
import { NgxPaginationModule} from 'ngx-pagination'; 

@NgModule({
  declarations: [
    DashboardComponent,OrgHeaderComponent,
    DocView,
    ApproveDeclineModel,
    VerifierDashboardComponent,
    SeekerDashboardComponent,
    ownerFullInfoComponent,
    SeekerDocView,
    AccesserPaymentComponent, 
    WalletsComponent, 
    RealizedAmountComponent, 
    CreditAmountComponent,
    Sendrequestpopup,
    FilterPipeValue,
  ],
  imports: [
    CommonModule,
    DataAccessorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    NgxPaginationModule,
  ],
  providers: [
    DataService, 
  ],
  entryComponents:[
    DocView, 
    ApproveDeclineModel,
    OrgHeaderComponent, 
    ownerFullInfoComponent, 
    SeekerDocView, WalletsComponent, 
    RealizedAmountComponent, 
    CreditAmountComponent,
    Sendrequestpopup
  ]
})
export class DataAccessorModule { }
