import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl} from '@angular/forms';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';
import { DocVerifyService } from '../../Services/PrivateService/DocVerify/doc-verify.service';
import { MatDialog, MatDialogConfig,MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

export interface AprvDclnData{
  status:string;
}

@Component({
  selector: 'app-verifier-dashboard',
  templateUrl: './verifier-dashboard.component.html',
  styleUrls: ['./verifier-dashboard.component.css']
})
export class VerifierDashboardComponent implements OnInit {

  constructor(
              private _formBuilder:FormBuilder,
              private router: Router,
              private privateService:PrivateService,
              private userDocServ: UserDocsService,
              private docVerifyServ: DocVerifyService,
              public dialog: MatDialog ) { }
         public searchRequest:string;
         public ownerRequest:string;
         p : number = 1;
         pageSize:[8,16,24];
  ngOnInit() {
    this.getOrgDetails();


    
     let header = document.getElementById("myDIV");
    let btns = header.getElementsByClassName("btncls");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
      });
    }
    
  }

  public commentFormGroup = new FormGroup({
    comment : new FormControl(''),
    attachfile : new FormControl('')

  })


  public orgAllData:any = {}
  getOrgDetails(){   
    this.privateService.profiledata1().subscribe(backData =>{
        
      this.orgAllData = backData['orgData']; 
      console.log("orgggggggg dataaaa$$$$$",backData,this.orgAllData)
      this.getAllRequests();
         
     },
     error => console.log("errooorr",error)
    );
  }

 

  public searchreq : any ={};
  public dtaownerdiv:boolean = false;

  searchmobifun(searchreq){
  	console.log("search moile number", searchreq);
  	this.dtaownerdiv =true;
  }

  public docsDetails=[];
  public docsTitle;
  public approvedRow:boolean = false;
  public declinedRow:boolean = false;
  
   pendingfun(){
    this.docsDetails = this.pendingDocs;
    this.docsTitle = 'Pending Requests';
    console.log("######", this.docsDetails)
      // document.getElementById("pending").style.backgroundColor="#2e6da4";
      // document.getElementById("approved").style.backgroundColor="white";
      // document.getElementById("declined").style.backgroundColor="white";
      // document.getElementById("pending").style.color="white";
      // document.getElementById("approved").style.color="black";
      // document.getElementById("declined").style.color="black";
      this.approvedRow = false;
      this.declinedRow = false;
   }
   approvedfun(){
    this.docsDetails = this.approvedDocs;
    this.docsTitle = 'Approved Requests';
      // document.getElementById("pending").style.backgroundColor="white";
      // document.getElementById("approved").style.backgroundColor="#2e6da4";
      // document.getElementById("declined").style.backgroundColor="white";
      // document.getElementById("pending").style.color="black";
      // document.getElementById("approved").style.color="white";
      // document.getElementById("declined").style.color="black";
      this.approvedRow = true;
      this.declinedRow = false;
   }
   declinedfun(){
    this.docsDetails = this.declinedDocs;
    this.docsTitle = 'Declined Requests';
       // document.getElementById("pending").style.backgroundColor="white";
       //  document.getElementById("approved").style.backgroundColor="white";
       //  document.getElementById("declined").style.backgroundColor="#2e6da4";
       //  document.getElementById("pending").style.color="black";
       //  document.getElementById("approved").style.color="black";
       //  document.getElementById("declined").style.color="white";
      this.approvedRow = false;
      this.declinedRow = true;
   }
  

   routing(){
     console.log("******************************")
    this.router.navigate(['','/DataAccessor']);
   }

   public errorMessage;
   public allDocuments = []

   public loadAllDocs(): Promise<any> {
      const tempList = [];
      let orgMo = this.orgAllData['cont_phone'].toString()
      return this.userDocServ.getAllDocsOfVerifier(orgMo)
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        result.forEach(asset => {
          tempList.push(asset);
        });
        this.allDocuments = tempList;
        console.log("Alllll Documentsssss",this.allDocuments);
        this.getVerifierAllDocs();
        
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

   public allRequests = [];
   public getAllRequests(){
    console.log("im in get user data methodd")
    this.privateService.getRequests().subscribe(backData =>{
      this.allRequests = backData; 
      this.loadAllDocs();      
      console.log("Alllll Requestttss",this.allRequests)     
     },
     error => console.log("errooorr",error)
    );
  } 

  public pendingDocs = [];
  public approvedDocs = [];
  public declinedDocs = [];
  public getVerifierAllDocs(){
    this.pendingDocs = [];
    this.approvedDocs = [];
    this.declinedDocs = [];
    this.allRequests.forEach(rDoc => {
      console.log("in forrr aaaDoccc",rDoc['docId']);
      this.allDocuments.forEach(aDoc =>{
        console.log("in forrr rrrDoccc",aDoc['doc_id']);
        if(rDoc['doc_id'] == aDoc['docId']){ 
          aDoc["ownerName"] = rDoc['owner_fullname'];
          aDoc["ownerMobile"] = rDoc['owner_mobile'];
          aDoc["document"] = rDoc['document'];  
          if ( aDoc['verifierStatus'] == "PENDING"){                        
            this.pendingDocs.push(aDoc);
          }
          else if( aDoc['verifierStatus'] == "APPROVED" ){            
            this.approvedDocs.push(aDoc);
          }
          else if( aDoc['verifierStatus'] == "DECLINED" ){           
            this.declinedDocs.push(aDoc);
          }
        }
      });     
    });
    console.log("Onlyyyyyy Verifier Docssssss",this.pendingDocs,this.approvedDocs,this.declinedDocs);
    this.pendingfun();
  }
     

  public docStatusChange(doc:any): Promise<any> {
    console.log("yuuuuuuuuuuuutr",doc);
    let vdate = new Date();
    let vd = vdate.toString();
    let transaction = {
      $class: 'gowdanar.pehachain.DocumentVerification',
      'userDocId': doc["docId"],
      'verifierId': "resource:gowdanar.pehachain.DataVerifierOrSeeker#"+"V"+this.orgAllData['user_id'].toString(),
      'verifiedDateTime': vd,
      'orgMobile': this.orgAllData['cont_phone'],
      'orgEmailId': this.orgAllData['cont_email'],
      'verifierStatus': doc["status"],
      
    };    

    return this.docVerifyServ.addTransaction(transaction)
    .toPromise()
    .then(() => {
      this.docStatusChangeInPostGre(doc, vd)
      this.errorMessage = null;
      this.ngOnInit();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
        this.errorMessage = error;
      }
    });



  }


  public docStatusChangeInPostGre(doc, vd){
    doc["veriDate"] = vd;

     let docStatusData = new FormData();
    docStatusData.append("veriDate",vd);
    docStatusData.append("docId",doc['docId']);
    docStatusData.append("status",doc['status']);
    docStatusData.append("comment",doc['comment']);
    docStatusData.append("refDoc",doc['refDoc']);


    this.privateService.docStatusChangeInPostGre(docStatusData).subscribe(backData =>{
      console.log("Alllll Requestttss",backData);
     },
     error => console.log("errooorr",error)
    );
  }

  viewImg(data){
    this.privateService.docImage = data;
    const privateotpref = this.dialog.open( DocView, {
      width:"1000px",
      height:"650px"
    });
  }

  public docInfoTrans:any = {}
  public status:string;
  openAprvDclnmodel(id, status){
      console.log("dataaaaaaaaaaaaaaaaaaaaa",id,status);
      this.docInfoTrans["docId"] = id;
      this.docInfoTrans["status"] = status; 
      this.status = status;
      const openAprvDclnmodelref = this.dialog.open( ApproveDeclineModel, {
        width:"500px",
        // height:"650px",
        data:{status:this.status}
      });
      openAprvDclnmodelref.afterClosed().subscribe(comm =>{
        console.log("commenttttttt@@@@@%%%%%%%%%%%",comm);
        if (comm){
          this.docInfoTrans["comment"] = comm['comment'];
          this.docInfoTrans["refDoc"] = comm['attachment']
          this.docStatusChange(this.docInfoTrans)
        }
        

      })
  }


 
}



@Component({
  selector: 'docView',
  templateUrl: './docView.html',
  styleUrls: ['./verifier-dashboard.component.css']
})
export class DocView implements OnInit {

  constructor( private PrivateService:PrivateService,
    public dialogRef: MatDialogRef<DocView>  ) {}

    public verifierDoc;
  ngOnInit() {
    this.verifierDoc =this.PrivateService.docImage
  }
  closeimage(){
    this.dialogRef.close();
  }

}




@Component({
  selector: 'ApproveDecline',
  templateUrl: './ApproveDecline.html',
  styleUrls: ['./verifier-dashboard.component.css']
})
export class ApproveDeclineModel implements OnInit {

  constructor( private PrivateService:PrivateService,
    public dialogRef: MatDialogRef<ApproveDeclineModel>,
    @Inject(MAT_DIALOG_DATA) public data:AprvDclnData  ) {}

  ngOnInit() {
    console.log("$$$$$$$$$$$$$$$", this.data['status']);
  }

  public commentFormGroup = new FormGroup({
    comment : new FormControl(''),
    attachfile :  new FormControl('')

  })

 closePop(){
   this.dialogRef.close();
 }

 public selectedFile:any = {};
  public obj;
  attachfile(event:any){
    this.selectedFile["attachment"] = <File> event.target.files[0];
    console.log("attachfile-------------------",this.selectedFile);
    // this.obj=this.selectedFile;
  }


 public noComment:any = {"comment":"Document Approved"}
}






