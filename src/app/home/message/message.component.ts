import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../Services/PublicService/login.service';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { InitialregistrationService } from "../../Services/PublicService/initialregistration.service";


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {



  constructor(public loginService:LoginService,
              private initialRegServ: InitialregistrationService,
              private router: Router) {
  }

  ngOnInit() {

    if (this.initialRegServ.redirectToHome === "Redirect") {
        this.router.navigate(['/home']);
      }

    this.getcount_of_connections1();
  }
 public selectedFile = "";
  // public obj;
  attachfilefun(event:any){
    this.selectedFile = event.target.files[0];
    console.log("attach-------------------",this.selectedFile);
    // this.obj=this.selectedFile;
  }

  public message:any ={};
  public currentmsg;
  sendmessage(message, person){

    // this.scrolldiv();
    // this.obj=this.selectedFile;
     let fd = new FormData();
     fd.append("attachment",this.selectedFile)
     fd.append("message",message['txt'])
     fd.append("person",person['user_id'])
     fd.append("full_name",person['full_name'])
     this.loginService.sendmessageser(fd).subscribe(backResponse => {
       console.log("kkkkkkkkkkkkkkkkkkkkkkkkk", fd);
    // let msgdiv = document.getElementById("msgdiv");
    // window.scrollTo(0, msgdiv.innerHeight);
     this.selectedFile = ""
     this.message = {};
    });

    if (this.currentmsg == false){
        this.gchat();
    }
  }


  public chatdata;
  public count:any;
  public talks1:any;

  getcount_of_connections1(){
    this.loginService.getcount_of_connections().subscribe(backResponse => {
      this.count=backResponse.count;
      this.talks1=backResponse.connections;
      console.log("chat list: ", this.talks1);
      },
      error => console.log("erroorrrr",error)
    );
  }


  public selectedPerson:any = {};
  public showchat:boolean=false;
  // public selectedPersonComp_serv;

  onSelect(person){
    this.showchat = true;
    this.selectedPerson=person;
    this.loginService.selectedPersonComp_serv = person;
    // this.scrolldiv();
    this.loginService.gettFullChatMessage(this.selectedPerson).subscribe(backResponse => {
        this.chatdata =backResponse['msg'];
    // console.log("chatdatatttttttttttttttttttttttt",chatdata);

      });
    this.gchat();
  }

  gchat(){
    console.log("setinterval");
    
    this.profiledata();
    let msgset = setInterval(() => {this.loginService.gettFullChatMessage(this.selectedPerson).subscribe(backResponse => {
        if (backResponse['msg'] == "userloggedout") {
          clearInterval(msgset);
          // message.scrollTop(message[0].scrollHeight);
          this.currentmsg == false;
        } else{
          this.chatdata =backResponse['msg'];
        }
      });
      // this.getcount_of_connections1();
    }, 2000)
  }



  public profi;
  public full_name;
  profiledata(){
      this.loginService.profiledata1().subscribe(backResponse => {
      console.log("profile data",backResponse)
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrr",error)
    );
  }
  deleteMessage(){
    
  }


}









// ===> MOBILE VIEW COMPONENT <=== //

@Component({
  selector: 'MobileView',
  templateUrl: './MobileView.html',
  styleUrls: ['././message.component.css']
})
export class MobileViewComponent implements OnInit {

  constructor(public loginService:LoginService) { }

  public selectedPersonServ_Comp:any = {};

  ngOnInit() {
    this.selectedPersonServ_Comp = this.loginService.selectedPersonComp_serv;
    this.gchat();
  }

  public profi;
  public full_name;
  profiledata(){
      this.loginService.profiledata1().subscribe(backResponse => {
      console.log("profile data",backResponse)
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrr",error)
    );
  }


  public currentmsg;
  public chatdata;
  gchat(){

    console.log("setinterval-mobile", this.selectedPersonServ_Comp );
    this.profiledata();
    let msgset = setInterval(() => {this.loginService.gettFullChatMessage(this.selectedPersonServ_Comp).subscribe(backResponse => {
        if (backResponse['msg'] == "userloggedout") {
          clearInterval(msgset);
          this.currentmsg == false;
        } else{
          this.chatdata =backResponse['msg'];
        }
      });
    }, 2000)
  }

  public message:any ={};
  sendmessage(message, person){
    // this.scrolldiv();
    let chat = {'message':message,'person':person}
    this.loginService.sendmessageser(chat).subscribe(backResponse => {
        this.message = {}
      });
    if (this.currentmsg == false){
        this.gchat();
    }
  }

}
