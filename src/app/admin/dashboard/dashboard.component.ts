import { Component, OnInit } from '@angular/core';
import { AdminVerifyService } from '../../Services/PrivateService/AdminVerify/admin-verify.service';
import { LoginService } from '../../Services/PublicService/login.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { Globals } from '../../globals';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(  private adminVerifyService:AdminVerifyService,
                private globals:Globals,
                private loginservice:LoginService) { }

  ngOnInit() {
    this.getCount();
  }
  closeNav(){
     document.getElementById("mySidenav").style.width = "0px";
   }

   public adminCounts:any = { "partnerNodes":0,"alldocs":0,
                              "individuals":0,"organizations":0};
    // public alldocs;
    // public indi;
    // public orga;
     getCount(){
       this.adminVerifyService.getdocs().subscribe(dte =>{
         this.adminCounts = dte
        //  this.alldocs=dte['alldocs'];
        //  this.indi=dte['individuals'];
        //  this.orga=dte['organizations'];
        //  console.log("individual",this.indi);
        //  console.log("organisation",this.orga);
        //  console.log("alldocs",this.alldocs);

       })
     }

}



// ===> ADMIN HEADER COMPONENT <=== //

@Component({
  selector: 'adminheader',
  templateUrl: './AdminHeader.html',
  styleUrls: ['./dashboard.component.css']
})
export class AdminHeaderComponent implements OnInit {

  constructor(  private router : Router,
                private loginService:LoginService,
                private globals:Globals
                ) { }

  ngOnInit() {
    this.getprofiledata();
    
    // document.getElementById("mySidenav").style.width = "250px";
  }

   public profi;
   public full_name;
   getprofiledata(){
     this.loginService.profiledata1().subscribe(backResponse => { 
        this.profi=backResponse.profile_photo;
        this.full_name=backResponse.full_name;
      },
      error => console.log("erroorrrrooooo",error)
    );

   }


   openNav(){
     document.getElementById("mySidenav").style.width = "250px";
   }
   closeNav(){
     document.getElementById("mySidenav").style.width = "0px";
   }

   public user:any = {}
   logoutPrivate () {
    this.user = {};    
    console.log("in component dataaa"); 
    this.loginService.logout11().subscribe(
      backendres =>{
        console.log("response from backend",backendres); 
        localStorage.removeItem('token');
        this.globals.loggedInUserRole='';
        this.router.navigate(['/'])
        
        // window.location.reload();
    
      },
      error => console.log("errrroooooorrr",error)
       
    )
 
  } 

}
