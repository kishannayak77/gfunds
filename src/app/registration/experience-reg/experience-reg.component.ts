import { Component, OnInit,ElementRef,ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { HighestQualification } from '../../Services/PublicService/highest-qualification';

import { Course3 } from '../../Services/PublicService/highestqualification';
import { Course } from '../../Services/PublicService/course';
import { District } from '../../Services/PublicService/district';
import { CustomService} from '../../Services/PublicService/customservices.service';
import { State } from '../../Services/PublicService/state';
import { FormControl} from '@angular/forms';
import { InitialregistrationService } from '../../Services/PublicService/initialregistration.service';
import { trigger, transition, group, query, style, animate } from '@angular/animations';
import * as _moment from 'moment';
declare var $: any;
import { regsuccesspopup } from '../../registration/fresher-reg/fresher-reg.component';
import { MAT_DIALOG_DATA,MatDialog, MatDialogConfig,MatDialogRef} from '@angular/material';
import { NativeDateAdapter, DateAdapter, MAT_DATE_FORMATS } from "@angular/material";
import { AppDateAdapter, APP_DATE_FORMATS} from '../../Services/PublicService/date.adapter';

export interface Genders {
  value: string;
  viewValue: string;
}
export interface NoticePeriods {
  value: string;
  viewValue: string;
}
export interface Coursetypes {
  value: string;
  viewValue: string;
}
export interface Passingyears {
  value: string;
  viewValue: string;
}

export interface Courses1 {
  value: string;
  viewValue: string;
}
// export interface Annualsalarythousands {
//   value: string;
//   viewValue: string;
// }
export interface Annualsalarylacs {
  value: string;
  viewValue: string;
}
// export interface Totalexperienceyears {
//   value: string;
//   viewValue: string;
// }

// export interface Totalexperiencemonths {
//   value: string;
//   viewValue: string;
// }

@Component({
  selector: 'app-experience-reg',
  templateUrl: './experience-reg.component.html',
  styleUrls: ['./experience-reg.component.css'],
  providers: [
    {
        provide: DateAdapter, useClass: AppDateAdapter
    },
    {
        provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }
    ]
})
export class ExperienceRegComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
   thirdFormGroup: FormGroup;
  checked:any;
  isEditable = true;
  isLinear = false;
  userData:any={};
  public personaldetails;
  public expdetails;
  public states:State[];
  public states1:State[];
  // public districts:Districts[];
  public districts1:District[];
  public highestQualifications:HighestQualification[];
  public highestQualifications1:HighestQualification[];
  public highestQualifications2:HighestQualification[];
  public highestQualifications3:HighestQualification[];
  public highestQualifications4:HighestQualification[];


  public courses:Course[];
  public courses1:Course[];
  public courses2:Course[];
  public coursesList:Course3[];
  items:FormArray;
  empitems:FormArray;
  get personalform() { return this.firstFormGroup.controls; }
  get employmentform() { return this.empitems.controls; }
  get educationform() { return this.empitems.controls; }

  keyPress(event: any) {
    const pattern = /[0-9]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  keyPressPercent(event: any) {
    const pattern = /[0-9.]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  keyPress2(event: any) {
  const pattern = /[0-9\+\-\ ]/;

  let inputChar = String.fromCharCode(event.charCode);
  if (event.keyCode != 8 && !pattern.test(inputChar)) {
    event.preventDefault();
  }
}

  keyPress1(event: any) {
    const pattern = /[a-zA-Z ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  keyPress3(event: any) {
    const pattern = /[a-zA-Z  ,]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }
  emailpattern=/^[a-zA-Z]+[a-zA-Z0-9._]+@[a-zA-Z]+\.[a-zA-Z]{2,5}$/;
  
  @ViewChild('myModal') myModal:ElementRef;
public birthdate: any;
public age: number;
private str:string="";
genders: Genders[] = [
    {value: 'Male', viewValue: 'Male'},
    {value: 'Female', viewValue: 'Female'}, 
    {value: 'Other', viewValue: 'Other'}
    ];
  noticeperiods: NoticePeriods[] = [
    {value: 'lessthan15', viewValue: '15 days or less'},
    {value: 'onemonth', viewValue: '1 month'},
    {value: 'twomonths', viewValue: '2 months'},
    {value: 'threemonths', viewValue: '3 months'},
    {value: 'greaterthanthree', viewValue: 'more than 3 months'}
  ];
  coursetypes: Coursetypes[] = [
    {value: 'parttime', viewValue: 'Part Time'},
    {value: 'fulltime', viewValue: 'Full Time'},
    {value: 'correspondence', viewValue: 'Correspondence'}
  ];
  


  public yearsList:any = [];
  getYears(){

    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    let endYear = (currentYear - 60);

    for (let i = currentYear; i > endYear; i-- ){
      this.yearsList.push(i);
    }
    console.log("yearslist", this.yearsList, currentDate, currentYear);

  }


  // constructor(private _formBuilder: FormBuilder) {}
  constructor(private router: Router,private _formBuilder: FormBuilder, private expregservice:InitialregistrationService,
    private customsdservice:CustomService,
    public dialog: MatDialog) {
    this.highestQualifications = this.customsdservice.getHighestQualifications();
    this.highestQualifications1 = this.customsdservice.getHighestQualifications();
    this.highestQualifications2 = this.customsdservice.getHighestQualifications();
    this.highestQualifications3 = this.customsdservice.getHighestQualifications();
    this.highestQualifications4 = this.customsdservice.getHighestQualifications();
    this.states = this.customsdservice.getStates();
    this.states1 = this.customsdservice.getStates();
    this.getuserdetails();
  }
  ngOnInit() {

    this.getYears();

    this.firstFormGroup = this._formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      emailid: ['',[Validators.required,Validators.pattern(this.emailpattern)]],
      mobilenumber: ['', Validators.required],
      addressline1: ['', Validators.required],
      addressline2: ['', Validators.required],
      permanentstate: ['', Validators.required],
      permanentdistrict: ['', Validators.required],
      permanentcity: ['', Validators.required],
      permanentpincode: ['', Validators.required],
      addressline3: ['', Validators.required],
      addressline4: ['', Validators.required],
      presentstate: ['', Validators.required],
      presentdistrict: ['', Validators.required],
      presentcity: ['', Validators.required],
      presentpincode: ['', Validators.required],

      checked:[''],

    });
    this.thirdFormGroup = this._formBuilder.group({
           items: this._formBuilder.array([ this.createItem() ])

    });
       this.secondFormGroup = this._formBuilder.group({
           empitems: this._formBuilder.array([ this.empcreateItem() ])

    });
  }
    createItem(): FormGroup {
  return this._formBuilder.group({
    highest: ['', Validators.required],
    course: [''],
    othercourse: [''],
    specialization: [''],
    board12: [''],
    yop12: [''],
    medium12: [''],
    percent12: [''],
    school12: [''],
    percent: [''],
    board10: [''],
    yop10: [''],
    boardless10: [''],
    yopless10: [''],
    medium10: [''],
    percent10: [''],
    school10: [''],
    mediumbelow10: [''],
    percentbelow10: [''],
    schoolbelow10: [''],
    degpassingyear:[''],
    degcoursetype:[''],
    univname:['']
  });
}

  empcreateItem(): FormGroup {
  return this._formBuilder.group({
    designation: ['', Validators.required],
    organisation: ['', Validators.required],
    skills: ['',Validators.required],
    // experienceyears: ['', Validators.required],
    // experiencemonths: ['', Validators.required],
    salarylacs: ['', Validators.required],
    // salarythousands: ['', Validators.required],
    noticeperiod: ['', ],
    Iscurrentcompany: ['', ],
    stdate: ['', Validators.required],
    edate:['',],
    // enddate: ['', ],
    // startdate:['',]
      });
}
additem1(): void {
  console.log("hbdsahbsahdb");
  this.empitems = this.secondFormGroup.get('empitems') as FormArray;
  var i=this.empitems.length;
  // if (i<5){
      this.empitems.push(this.empcreateItem());

  // }
  // else{
  //   alert("maximum limit reached");
  // }
}
removeitem1(i): void {
  console.log(i)
  this.empitems = this.secondFormGroup.get('empitems') as FormArray;
  if(i!= 0){
    this.empitems.removeAt(i);
  }

}

additem(): void {
  console.log("hbdsahbsahdb");
  this.items = this.thirdFormGroup.get('items') as FormArray;
  var i=this.items.length;
  if (i<6){
      this.items.push(this.createItem());

  }
  else{
    alert("maximum limit reached");
  }
}
removeitem(i): void {
  console.log(i)
  this.items = this.thirdFormGroup.get('items') as FormArray;
  if(i!= 0){
    this.items.removeAt(i);
  }
  }
getcourses(qualification){
  console.log("my qualification is",qualification);
  this.courses = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification);
 }

 getcourses1(qualification2){
  console.log("my qualification and id is",qualification2);
  this.courses1 = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification2);
  console.log('cordesddddddddd',this.courses2);
 }
 getcourses2(qualification5){
  console.log("my qualification is",qualification5);
  this.courses2 = this.customsdservice.getCourses().filter((item)=> item.highQualify == qualification5);
  console.log("mycourses for graduation",this.courses2);
 }
 public districtsn:any={};
  getdistricts(dis){
  console.log("statezzzzzzzzzzzzzzz is",dis);
  this.districtsn = this.customsdservice.getDistrict().filter((item)=> item.mystate == dis);
 }
 
 public districtsm:any={}
 getdistricts1(dist){
  console.log("statezzzzzzzzzzzzzzz is",dist);
  this.districtsm = this.customsdservice.getDistrict().filter((item)=> item.mystate == dist);
 }
  submitpersonaldetails(userData){
   console.log("user personal details",userData);
   this.personaldetails = {userData,'dob':this.birthdate}   
   console.log(this.personaldetails)
   let perData = {'personal': this.personaldetails}

   this.expregservice.indivPersonalData(perData).subscribe(
    backendData => {
      console.log("inside if",backendData);
    },
    error => console.log("errooorr",error)
    );
  }
  submitexpdetails(data){
   
   for (let i in data.value['empitems']){
    //  console.log("i----------------------",data.value['empitems'][i])
     data.value['empitems'][i]['startdate']=(_moment((data.value['empitems'][i]['stdate'])).format("DD/MM/YYYY"));
     if (data.value['empitems'][i]['edate']==""){
      data.value['empitems'][i]['enddate']='';
     }else{
     data.value['empitems'][i]['enddate']=(_moment((data.value['empitems'][i]['edate'])).format("DD/MM/YYYY"));
    }
   }
   this.expdetails = data.value;
   console.log("after mod---------------",data.value);
   this.expregservice.indivExperData(this.expdetails).subscribe(
    backendData => {
      console.log("inside if",backendData);
    },
    error => console.log("errooorr",error)
    );
 }

  submiteducationaldetails(data){
   console.log("user edu details",data.value);
    this.expregservice.indivEducationData(data.value).subscribe(
      backendData => {
        // alert(backendData)
        console.log("inside if",backendData);
        if(backendData["resp"] == "successfully_saved"){
          console.log(JSON.stringify(backendData));
          this.dialog.open(regsuccesspopup, {
            height: '243px',
            width: '465px',
          });
  
        }
        // this.router.navigate(['home']);
      },
      error => console.log("errooorr",error)
      );

 }

 finalSkipofIndividualReg(){
  this.dialog.open(regsuccesspopup, {
    height: '243px',
    width: '465px',
  });
 }

 getToday(): string {
  return new Date().toISOString().split('T')[0]
  
}

getValidDate(){
  let a=new Date();
  return new Date(a.getFullYear()-16,a.getMonth(),a.getDate())
}


public getuserdetails(){
  this.expregservice.userdetails1().subscribe(
    userdata =>{
      console.log("userdata in ts is..",userdata[0]);
      var user_mobile = userdata[0].mobile_number;
      var user_name = userdata[0].full_name;
      var last_name=user_name.split(" ");
      console.log(last_name);
      var n =last_name.length;
        console.log(last_name.length);
      for(var i=0;i<n;i++){
        if(i==0){
                this.firstFormGroup.patchValue({firstname:last_name[i],mobilenumber:user_mobile})
       } 
       else{
            this.str=this.str+last_name[i]+" ";
            console.log("data",this.str);
       }
      }
      console.log("final data",this.str);
      this.firstFormGroup.patchValue({lastname:this.str,mobilenumber:user_mobile})
    }
  )
}

 calculateAge(data){ 
if (data) {
  let dob=(_moment((data)).format("DD/MM/YYYY"));
this.birthdate = dob;

  }
 }

}
