import { TestBed } from '@angular/core/testing';

import { ArraylistService } from './arraylist.service';

describe('ArraylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArraylistService = TestBed.get(ArraylistService);
    expect(service).toBeTruthy();
  });
});
