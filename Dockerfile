FROM node:10-alpine as builder
WORKDIR /app
COPY . .
RUN npm install
RUN npm add @ng-bootstrap/ng-bootstrap@4
RUN npm add @ng-bootstrap/schematics
RUN npm install --save @angular/material @angular/cdk @angular/animations bootstrap@3 jquery angular-moment moment ngx-bootstrap
RUN npm install ngx-drag-scroll@7.1.5 --save
RUN $(npm bin)/ng build --prod

# stage 2
FROM nginx:alpine
COPY --from=builder /app/dist/pehachainfrontend /usr/share/nginx/html

