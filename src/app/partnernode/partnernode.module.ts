import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PartnernodeRoutingModule } from './partnernode-routing.module';
import { PartnerDashboardComponent } from './partner-dashboard/partner-dashboard.component';
import { PartnerNavbarComponent } from './partner-navbar/partner-navbar.component';
import { ProfileComponent } from './profile/profile.component';
import { ManageorgsComponent } from './manageorgs/manageorgs.component';
import { PaymentsComponent } from './payments/payments.component';
import { DemoMaterialModule} from '../../material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [PartnerDashboardComponent, PartnerNavbarComponent, ProfileComponent, ManageorgsComponent, PaymentsComponent],
  imports: [
    CommonModule,
    PartnernodeRoutingModule,
    DemoMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PartnernodeModule { }
