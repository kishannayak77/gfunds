import { TestBed } from '@angular/core/testing';

import { DataAccesserService } from './data-accesser.service';

describe('DataAccesserService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataAccesserService = TestBed.get(DataAccesserService);
    expect(service).toBeTruthy();
  });
});
