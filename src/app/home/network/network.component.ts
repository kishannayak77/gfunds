import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../Services/PublicService/login.service';
import { Globals } from 'src/app/globals';

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {

  constructor(public loginService:LoginService, public globals:Globals) { 
    
  }

  ngOnInit() {
    if (this.globals.loggedInUserRole!='orignasition'){
      this.getFriendrequest1();
      this.getnetworkmembers();
    }else{
      this.getFriendrequest1();
    } 
    // document.getElementById("target").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
    // document.getElementById("target3").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
  }
// public talks=['d','d','g','a'];
public showPeop = true;
public showGro = false;
public showCompa = false;
public showPeop1 = true;
public showGro1 = false;
public showCompa1 = false;

showPeoples(){
// document.getElementById("target").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target1").style.cssText="color:#7A7472;border-bottom:none";
// document.getElementById("target2").style.cssText="color:#7A7472;border-bottom:none";
this.getnetworkmembers();

this.showPeop = true;
this.showGro = false;
this.showCompa = false;
}
// -------------------------Responsive part-------------------------------------
showPeoples1(){
// document.getElementById("target3").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target4").style.cssText="color:#7A7472;border-bottom:none";
// document.getElementById("target5").style.cssText="color:#7A7472;border-bottom:none";
this.getnetworkmembers();

this.showPeop = true;
this.showGro = false;
this.showCompa = false;
}
// -----------------------------------END----------------------------------------------




showGroups(){
// document.getElementById("target1").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target2").style.cssText="color:#7A7472;border-bottom:none";
// document.getElementById("target").style.cssText="color:#7A7472;border-bottom:none";
this.showGro = true;
this.showPeop = false;
this.showCompa = false;
}

// -------------------------Responsive part-------------------------------------
showGroups1(){
// document.getElementById("target4").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target5").style.cssText="color:#7A7472;border-bottom:none";
// document.getElementById("target3").style.cssText="color:#7A7472;border-bottom:none";
this.showGro = true;
this.showPeop = false;
this.showCompa = false;
}
// -----------------------------------END----------------------------------------------
showCompanies(){
// document.getElementById("target2").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target").style.cssText="color:#7A7472; border-bottom:none";
// document.getElementById("target1").style.cssText="color:#7A7472; border-bottom:none";
this.getNewCompanies()
this.showCompa = true;
this.showGro = false;
this.showPeop = false;


}
// -------------------------Responsive part-------------------------------------
showCompanies1(){
// document.getElementById("target5").style.cssText="color:black; border-bottom:2px solid #0073b1; margin:0px 20px";
// document.getElementById("target3").style.cssText="color:#7A7472;border-bottom:none";
// document.getElementById("target4").style.cssText="color:#7A7472;border-bottom:none";
this.getNewCompanies()
this.showCompa = true;
this.showGro = false;
this.showPeop = false;
}
// -----------------------------------END----------------------------------------------
public talks:any;
  getnetworkmembers(){
    this.loginService.getnetworkmembers1().subscribe(backResponse => {
        console.log('#######88888888888',backResponse.userdata);
        this.talks=backResponse.userdata;        
      },
      error => console.log("erroorrrr",error)
    );

  }

  getNewCompanies(){
    this.loginService.getCompaniestoConnect().subscribe(
      backResponse => {
        console.log('#######GGGGG',backResponse.userdata);
        this.talks=backResponse.userdata;        
      },
      error => console.log("erroorrrr",error)
    );
  }

  sendfriendRequest(data){
     console.log(data)
    this.loginService.sendfriendRequest1(data).subscribe(
      backResponse => {
        console.log('#######',backResponse);
        this.talks=backResponse; 
        this.getnetworkmembers(); 
      },
      error => console.log("erroorrrr",error)
    );

  }

  sendfollowRequest(data){
    console.log(data)
   this.loginService.sendfollowRequest1(data).subscribe(
     backResponse => {
       console.log('#######',backResponse);
       this.talks=backResponse; 
       this.getNewCompanies(); 
     },
     error => console.log("erroorrrr",error)
   );

 }

 public requestdata:any;

  getFriendrequest1(){
    this.loginService.getFriendrequest().subscribe(
      backResponse => {
        console.log('#######77777777777777777777',backResponse);
        this.requestdata=backResponse.requestdetails;        
      },
      error => console.log("erroorrrr",error)
    );
  }

  addfriendrequest(data){
    // let obj={"id":data.id};
    this.loginService.addfriendrequest1(data).subscribe(
      backResponse => {
        console.log('#######',backResponse);
        this.getFriendrequest1();
      },
      error => console.log("erroorrrr",error)
    );
  }

  deleteFriendRequest(data){
    console.log("delete request",data)
    this.loginService.deleteFriendRequest(data).subscribe(
      backResponse => {
        console.log('#######',backResponse);
        if (this.showPeop){
          this.getFriendrequest1();
          this.getnetworkmembers(); 
        }
        else{
          this.getFriendrequest1();
          this.getNewCompanies(); 
        }
               
      },
      error => console.log("erroorrrr",error)
    );
  }
}


@Component({
  selector: 'app-group',
  templateUrl: './group.html',
  styleUrls: ['./network.component.css']
})
export class NetworkGroup implements OnInit {
talks:any;
  constructor() { 
  }

  ngOnInit() {
    
  }
}



// @Component({
//   selector: 'app-people',
//   templateUrl: './people.html',
//   styleUrls: ['./network.component.css']
// })


// export class NetworkPeople implements OnInit {

//   constructor(public loginService:LoginService) { 

//   }

//   ngOnInit() {
//     this.getnetworkmembers();
//   }
//  public talks:any;
//   getnetworkmembers(){
//     this.loginService.getnetworkmembers1().subscribe(
//       backResponse => {
//         console.log('#######',backResponse.userdata);
//         this.talks=backResponse.userdata;        
//       },
//       error => console.log("erroorrrr",error)
//     );

//   }


 

//    sendfriendRequest(data){
//      console.log(data)
//     this.loginService.sendfriendRequest1(data).subscribe(
//       backResponse => {
//         console.log('#######',backResponse);
//         this.talks=backResponse; 
//         this.getnetworkmembers(); 
//       },
//       error => console.log("erroorrrr",error)
//     );

//   }

// }


@Component({
  selector: 'app-companies',
  templateUrl: './companies.html',
  styleUrls: ['./network.component.css']
})
export class NetworkCompanies implements OnInit {

  constructor() { 

  }

  ngOnInit() {
  }

  public talks=['d','d','g','g', 'd','a','b','c','d','a','b','c'];

}

@Component({
  selector: 'app-acceptfriend',
  templateUrl: './acceptedfriends.html',
  styleUrls: ['./network.component.css']
})
export class AcceptFriends implements OnInit {

  constructor(public loginService:LoginService, public globals:Globals) { 

  }

  ngOnInit() {
    this.getcount_of_connections1();
  }

public talks1:any;

  public count:any;
  getcount_of_connections1(){
    this.loginService.getcount_of_connections().subscribe(backResponse => { 
      console.log("profile data",backResponse)
      this.count=backResponse.count;
       this.talks1=backResponse.connections;
      },
      error => console.log("erroorrrr",error)
    );

  }

}



