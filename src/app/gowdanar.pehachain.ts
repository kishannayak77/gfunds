import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace gowdanar.pehachain{
   export abstract class PehachainUsers extends Participant {
      userId: string;
      fullName: string;
      emailId: string;
      mobileNo: string;
   }
   export class PehachainAdmin extends PehachainUsers {
   }
   export class DataOwner extends PehachainUsers {
   }
   export class DataVerifierOrSeeker extends PehachainUsers {
   }
   export class UserDocs extends Asset {
      docId: string;
      documentType: string;
      dataOwner: DataOwner;
      docName: string;
      document: string;
      verifierMobile: string;
      verifierEmailId: string;
      UploadedDateTime: string;
      verifierVerifiedDateTime: string;
      adminVerifiedDateTime: string;
      verifierStatus: DocStatus;
      adminVerifyStatus: DocStatus;
   }
   export enum DocStatus {
      PENDING,
      APPROVED,
      DECLINED,
   }
   export class DocumentVerification extends Transaction {
      userDocId: UserDocs;
      verifierId: DataVerifierOrSeeker;
      verifiedDateTime: string;
      orgMobile: string;
      orgEmailId: string;
      verifierStatus: DocStatus;
   }
   export class DocumentVerifyEvent extends Event {
      uDocId: UserDocs;
      oldStatus: string;
      newStatus: string;
   }
   export class AdminVerification extends Transaction {
      userDocId: UserDocs;
      verifiedDateTime: string;
      adminStatus: DocStatus;
   }
   export class AdminVerifyEvent extends Event {
      uDocId: UserDocs;
      oldStatus: string;
      newStatus: string;
   }
// }
