import { Component, OnInit } from '@angular/core';
/*COOKIE POLICY CSS*/
@Component({
  selector: 'app-shared',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.css']
})
export class SharedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
/*COPY RIGHT POLICY*/
@Component({
  selector: 'app-shared',
  templateUrl: './copyrightpolicy.html',
  styleUrls: ['./shared.component.css']
})
export class  CopyRightPolicyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*PAGE HEADER*/
@Component({
  selector: 'app-shared',
  templateUrl: './pageheader.html',
  styleUrls: ['./shared.component.css']
})
export class PageHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*PRIVACY FAQS*/
@Component({
  selector: 'app-shared',
  templateUrl: './privacyfaqs.html',
  styleUrls: ['./shared.component.css']
})
export class PrivacyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*PRIVACY HUB*/
@Component({
  selector: 'app-shared',
  templateUrl: './privacyhub.html',
  styleUrls: ['./shared.component.css']
})
export class PrivacyHubComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
/*PRIVACY POLICY*/
@Component({
  selector: 'app-shared',
  templateUrl: './privacypolicy.html',
  styleUrls: ['./shared.component.css']
})
export class PrivacyPolicyComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*PRIVACY SETTINGS*/
@Component({
  selector: 'app-shared',
  templateUrl: './privacysettings.html',
  styleUrls: ['./shared.component.css']
})
export class PrivacySettingsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*SUMMARY OF CHANGE*/
@Component({
  selector: 'app-shared',
  templateUrl: './summaryofchange.html',
  styleUrls: ['./shared.component.css']
})
export class SummaryOfChangeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

/*USER AGREEMENT*/
@Component({
  selector: 'app-shared',
  templateUrl: './useragreement.html',
  styleUrls: ['./shared.component.css']
})
export class UserAgreementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

