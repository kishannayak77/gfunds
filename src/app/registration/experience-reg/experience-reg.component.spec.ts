import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceRegComponent } from './experience-reg.component';

describe('ExperienceRegComponent', () => {
  let component: ExperienceRegComponent;
  let fixture: ComponentFixture<ExperienceRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
