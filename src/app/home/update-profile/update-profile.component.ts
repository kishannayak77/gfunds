import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormControl } from '@angular/forms';
import { LoginService } from '../../Services/PublicService/login.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.css']
})
export class UpdateProfileComponent implements OnInit {

  constructor( public formBuilder:FormBuilder,
               private loginService:LoginService ) { }


   ownerUpdateProfile: FormGroup;
 isLinear = false;
 hide=true;

  ngOnInit() {


   this.ownerUpdateProfile = new FormGroup({
      first_name : new FormControl('', [Validators.required]),
      last_name : new FormControl('', [Validators.required]),
      gender : new FormControl('', [Validators.required]),
      dob : new FormControl('', [Validators.required]),
      emailid : new FormControl('', [Validators.required]),
      mobile : new FormControl('', [Validators.required,Validators.maxLength(10)]),
      Address1 : new FormControl('', [Validators.required]),
      Address2 : new FormControl('', [Validators.required]),
      state : new FormControl('', [Validators.required]),
      district : new FormControl('', [Validators.required]),
      city : new FormControl('', [Validators.required]),
      pincode : new FormControl('', [Validators.required]),
      Address11 : new FormControl('', [Validators.required]),
      Address12 : new FormControl('', [Validators.required]),
      state1 : new FormControl('', [Validators.required]),
      district1 : new FormControl('', [Validators.required]),
      city1: new FormControl('', [Validators.required]),
      pincode1 : new FormControl('', [Validators.required]),
    });
 
    this.profileData();
  }

profileData(){
  this.loginService.profiledata1().subscribe(
    res => {
      this.profi=res['profile_photo'];
    },
    error => console.log("erroorrrr",error)
  );
}  
public profi:any;
public selectedFile;
  onFileUploaded(event){
    this.selectedFile = <File> event.target.files[0];
    const fd = new FormData();
    fd.append("image",this.selectedFile)
    this.loginService.upLoadImg(fd).subscribe(
      backResponse => {
        this.profi=backResponse;
      },
      error => console.log("erroorrrr",error)
    );
  }
}

