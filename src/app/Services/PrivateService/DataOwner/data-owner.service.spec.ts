import { TestBed } from '@angular/core/testing';

import { DataOwnerService } from './data-owner.service';

describe('DataOwnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataOwnerService = TestBed.get(DataOwnerService);
    expect(service).toBeTruthy();
  });
});
