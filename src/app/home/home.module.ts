import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent,HomeUpdateorg} from './home/home.component';
import { NetworkComponent,AcceptFriends , NetworkGroup, NetworkCompanies} from './network/network.component';
import { MessageComponent,MobileViewComponent } from './message/message.component';
import { MypostComponent, editmypost, deletemypost } from './mypost/mypost.component';
import { PublicProfileComponent } from './public-profile/public-profile.component';
import { SearchPeopleComponent } from './search-people/search-people.component';
import { Globals } from '../globals';
import { LoginService } from '../Services/PublicService/login.service';
import { TokenInterceptService } from '../Services/PublicService/token-intercept.service';
import { HttpClientModule,HttpHeaders, HttpErrorResponse,HTTP_INTERCEPTORS} from  '@angular/common/http';
import { DemoMaterialModule} from '../../material-module';
import { ManageviewComponent,ManageviewrecComponent } from './manageview/manageview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragScrollModule } from 'ngx-drag-scroll';
import { Newpost } from './Newpost/newpost';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { AuthGuard } from '../auth.guard';
// import { NgxAutoScrollModule } from "ngx-auto-scroll";


@NgModule({
  declarations: [
	  HomeComponent, 
	  NetworkComponent, 
	  MessageComponent, 
	  MypostComponent, 
    PublicProfileComponent,
    SearchPeopleComponent,
    AcceptFriends,
    ManageviewComponent,
    ManageviewrecComponent,
    MobileViewComponent,
    HomeUpdateorg,
    NetworkGroup,
    NetworkCompanies,
    editmypost,
    Newpost,
    deletemypost,
    UpdateProfileComponent,

  ],
  imports: [FormsModule, ReactiveFormsModule ,
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    DemoMaterialModule,
    DragScrollModule,
    // NgxAutoScrollModule,
  ],
  entryComponents: [
    AcceptFriends,
    ManageviewrecComponent,
    MobileViewComponent,
    HomeUpdateorg,
    NetworkGroup,
    NetworkComponent, 
    NetworkCompanies,
    editmypost,
    Newpost,
    deletemypost,
    
  ],
  providers: [Globals,LoginService,AuthGuard,{
    provide:HTTP_INTERCEPTORS,useClass:TokenInterceptService,multi:true
  }],
})
export class HomeModule { }
