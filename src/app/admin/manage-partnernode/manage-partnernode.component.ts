import { Component, OnInit } from '@angular/core';
import { AdminVerifyService } from '../../Services/PrivateService/AdminVerify/admin-verify.service';
import { LoginService } from '../../Services/PublicService/login.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-manage-partnernode',
  templateUrl: './manage-partnernode.component.html',
  styleUrls: ['./manage-partnernode.component.css']
})
export class ManagePartnernodeComponent implements OnInit {

  constructor(private adminService:AdminVerifyService) { }

  ngOnInit() {
    this.allRegPartenerNodes();
  }

  public allPartnerNodes = [];
  allRegPartenerNodes(){
    this.adminService.allRegPartenerNodes().subscribe(res => {
      console.log("resssssssssssss",res);
      this.allPartnerNodes = res;
    })
  }

}
