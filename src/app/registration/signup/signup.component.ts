import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import {
	FormBuilder,
	FormGroup,
	Validators,
	FormControl
} from "@angular/forms";
import { CustomValidators } from "../../Services/PublicService/custom-validators";
import { LoginService } from "../../Services/PublicService/login.service";
import { Router } from "@angular/router";
import {
	MAT_DIALOG_DATA,
	MatDialog,
	MatDialogConfig,
	MatDialogRef
} from "@angular/material";
declare var $: any;

@Component({
	selector: "app-signup",
	templateUrl: "./signup.component.html",
	styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
	@ViewChild("someModal") someModal: ElementRef;
	public registerForm: FormGroup;
	submitted = false;
	public user = "";
	public sendotp = true;
	public otp;
	public verified = false;
	public mobilenum_exist: string = "";
	public regstatus;
	public regstatus1;
	public errormsg;
	public pwdf1: boolean = false;
	pwdf2 = false;
	pwdf3 = false;
	pwdf4 = false;
	pwdf5 = false;
	mobNum = true;

	// pwdf6=true;
	keyPress(event: any) {
		const pattern = /[0-9]/;
		let inputChar = String.fromCharCode(event.charCode);
		if (event.keyCode != 8 && !pattern.test(inputChar)) {
			event.preventDefault();
		}
	}

	keyPress1(event: any) {
		const pattern = /[a-zA-Z ]/;

		let inputChar = String.fromCharCode(event.charCode);
		if (event.keyCode != 8 && !pattern.test(inputChar)) {
			event.preventDefault();
		}
	}

	mobnum(event: any) {
		const pattern = /[0-9]/;
		let inputChar = String.fromCharCode(event.charCode);
		if (event.keyCode != 8 && !pattern.test(inputChar)) {
			event.preventDefault();
		}
	}

	constructor(
		private formBuilder: FormBuilder,
		private router: Router,
		private loginService: LoginService,
		public dialog: MatDialog
	) {
		this.registerForm = this.createSignupForm();
	}

	ngOnInit() {
		$(document).click(function(e) {
			// console.log("click event------")
			$(".collapse").collapse();
		});
	}

	checknum(data: any) {
		if (data.length == 10) {
			this.mobNum = false;
		} else {
			this.mobNum = true;
		}
	}

	passwordcheck(data) {

		let regexpNumber: RegExp = /\d/;
		let regexpLower: RegExp = /[a-z]/;
		let regexpUpper: RegExp = /[A-Z]/;
		let regexpSpl: RegExp = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
		if (data.length > 0) {
			if (regexpNumber.test(data)) {
				this.pwdf2 = true;
				this.collape();
			} else {
				this.pwdf2 = false;
			}
			if (regexpSpl.test(data)) {
				this.pwdf3 = true;
				this.collape();
			} else {
				this.pwdf3 = false;
			}
			if (regexpUpper.test(data)) {
				this.pwdf4 = true;
				this.collape();
			} else {
				this.pwdf4 = false;
			}
			if (regexpLower.test(data)) {
				this.pwdf5 = true;
				this.collape();
			} else {
				this.pwdf5 = false;
			}
			if (data.length > 7) {
				this.pwdf1 = true;
				this.collape();
			} else {
				this.pwdf1 = false;
			}
		} else {
			this.pwdf1 = false;
			this.pwdf2 = false;
			this.pwdf3 = false;
			this.pwdf4 = false;
			this.pwdf5 = false;
			// this.pwdf6=true;
		}
		if((data.length) == 8){

		}
	}
	collape(){
		if((this.pwdf1) == true && (this.pwdf2) == true && (this.pwdf3) == true && (this.pwdf4) == true && (this.pwdf5) == true){
			$('.collapse').collapse('hide')
			// document.getElementById("collapse").style.visibility="hidden";
		}else{
			$('.collapse').collapse('show')
		}

	}

	generateotp(data) {
		console.log(data);
		this.mobilenum_exist = "";
		this.loginService.generateotp(data).subscribe(
			data => {
				console.log("response from backend", data);
				if (data == "Already registered") {
					this.mobilenum_exist = "This Mobile number Already registered";
				} else {
					this.sendotp = false;
					this.otp = data;
					console.log(this.otp);
				}
				// this.router.navigate(['forgot_veriotp']);
			},
			error => console.log("errrroooooorrr", error)
		);
	}
	public otperror: string;
	callType(data) {
		console.log(data);
		if (data == this.otp) {
			this.otperror = " ";
			this.verified = true;
		} else {
			console.log(data);
			if (data.length == 6) {
				this.otperror = "Invalid OTP";
			}
		}
	}

	createSignupForm(): FormGroup {
		return this.formBuilder.group(
			{
				fullname: ["", Validators.required],
				mobilenumber: ["", [Validators.required, Validators.minLength(10)]],
				OTP: ["", Validators.required],
				password: [
					null,
					Validators.compose([
						Validators.required,
						// check whether the entered password has a number
						CustomValidators.patternValidator(/\d/, {
							hasNumber: true
						}),
						// check whether the entered password has upper case letter
						CustomValidators.patternValidator(/[A-Z]/, {
							hasCapitalCase: true
						}),
						// check whether the entered password has a lower case letter
						CustomValidators.patternValidator(/[a-z]/, {
							hasSmallCase: true
						}),
						// check whether the entered password has a special character
						CustomValidators.patternValidator(
							/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
							{
								hasSpecialCharacters: true
							}
						),
						Validators.minLength(8)
					])
				]
				// confirmPassword: [null, Validators.compose([Validators.required])]
			},
			{
				// check whether our password and confirm password match
				// validator: CustomValidators.passwordMatchValidator
			}
		);
	}

	get memberlandingform() {
		return this.registerForm.controls;
	}

	saveData(data) {
		this.submitted = true;
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			return;
		}
		this.loginService.createUser(data.value).subscribe(data => {
			this.sendotp = false;
			if (data == "Registration success") {
				this.regstatus = "You're successfully registered with PEHACHAIN";
				this.regstatus1 = "To complete the profile please Login";
				$(this.someModal.nativeElement).modal("show");
				this.errormsg = "";
			} else if (
				data == "User already exists" ||
				data == "Registration failed"
			) {
				this.regstatus = "";
				this.errormsg = data;
				$(this.someModal.nativeElement).modal("show");
			}
		});
	}
}
@Component({
	selector: "initialreg",
	templateUrl: "./initialreg.html",
	styleUrls: ["./signup.component.css"]
})
export class InitialregComponent implements OnInit {
	constructor() {}

	ngOnInit() {}
}

// ===> Landing Video Page component <=== //

@Component({
	selector: "initialreg",
	templateUrl: "./landingvideo.html",
	styleUrls: ["./signup.component.css"]
})
export class LandingComponent implements OnInit {
	constructor() {}
	onloadedmetadata = "this.muted=true";
	ngOnInit() {}
}
