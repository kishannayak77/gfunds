import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {MatDialog} from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import{LoginService} from '../../Services/PublicService/login.service';
import { Globals } from 'src/app/globals';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 loginForm: FormGroup;
  loading = false;
  submitted = false;
    returnUrl: string;
    constructor( private formBuilder: FormBuilder, private loginservice:LoginService,private route: ActivatedRoute,private router: Router,public globals: Globals){}
    username: string;
    password: string;
    loginerror:any;
    type= "password";

 
    show = false;

    ngOnInit() {
      this.loginerror='';
       this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }
    login() : void {
      this.router.navigate(['home']);
    }
    get f() { return this.loginForm.controls; }

    onSubmit(data) {
        console.log("login dataaaaa",data)
        this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        this.loginservice.login(data).subscribe(
        backendres =>{
          console.log("response from backend",backendres, backendres['profile'][0]['role']);
          localStorage.setItem('token',backendres['token']);
          if(backendres['profile'][0]['role'] === "PEHACHAIN"){
            console.log("insideeeeee ifff",backendres['profile'][0]['role']);
            this.globals.loggedInUserRole=backendres['profile'][0]['role']
            this.router.navigate(['Admin']);
            return;
          }

          else if(backendres['profile'][0]['role'] === "PartnerNode"){
            console.log("insideeeeee ifff",backendres['profile'][0]['role']);
            this.globals.loggedInUserRole=backendres['profile'][0]['role']
            this.router.navigate(['Partnernode']);
            return;
          }

          else if(backendres['profile'][0]['gender']){
            this.globals.loggedInUserRole=backendres['profile'][0]['role']
            this.router.navigate(['home']);
          }
          else {
            this.router.navigate(['initialreg']);
          }
        },
        error => {console.log("errrroooooorrr",error)
        this.loginerror= error;
      }
      )

    }
    mobnum(event: any) {
      const pattern = /[0-9]/;
      let inputChar = String.fromCharCode(event.charCode);
      if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault(); 
      }
    }

    

  public shows:boolean = false;
  public buttonName:any = 'visibility_off';

  showpassword() {
 this.shows = !this.shows;
    if(this.shows)  
      this.buttonName = "visibility";
    else
      this.buttonName = "visibility_off";
  }

}

 



