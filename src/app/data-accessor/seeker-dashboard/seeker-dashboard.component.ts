import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import { FormControl} from '@angular/forms';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';
import { DocVerifyService } from '../../Services/PrivateService/DocVerify/doc-verify.service';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { LoginService } from '../../Services/PublicService/login.service';
import { DataOwnerService } from '../../Services/PrivateService/DataOwner/data-owner.service';


@Component({
  selector: 'app-seeker-dashboard',
  templateUrl: './seeker-dashboard.component.html',
  styleUrls: ['./seeker-dashboard.component.css']
})
export class SeekerDashboardComponent implements OnInit {

    myControl = new FormControl();
    options: string[]=[];
    filteredOptions: Observable<string[]>;

    keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

   constructor(
              private _formBuilder:FormBuilder,
              private router: Router,
              private privateService:PrivateService,
              private userDocServ: UserDocsService,
              private docVerifyServ: DocVerifyService,
              public dialog: MatDialog ) { }

  ngOnInit() {
    this.getAllOwnersMobile();
    this.getSeekerOwners();
    this.newreqfun();

  let header = document.getElementById("myDIV");
    let btns = header.getElementsByClassName("btncls");
    for (var i = 0; i < btns.length; i++) {
      btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
      });
    }


  }

     
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  public ownermobile;
  public dtaownerdiv;
  public ownerDetails:any={};

  getownerdetails(ownermobile){
    console.log("search moile number****", ownermobile);
    
    let ownerMob = { "mobile": ownermobile }

    this.privateService.getAllOwnerDetails(ownerMob).subscribe(backData =>{
      console.log("owwwnneeerrrr daaatatt",backData)
      if (backData['info']){
          this.ownerDetails = backData['info'];
          this.ownerDetails['profile_pic'] = backData['pic'];
          this.dtaownerdiv =true;
      }else{
        console.log("Mobile Number Doesnot Exist.");
        this.dtaownerdiv = false;
      }
      console.log("$$$$$$$",this.ownerDetails) 
     },
     error => console.log("errooorr",error)
    );
  }

  // public AllOwnersMobile=[];
  getAllOwnersMobile(){
    console.log("99999999999")
    this.privateService.getAllOwnersMobile().subscribe(backData =>{
      this.options = backData.mobi; 
      console.log("!!!!!!!!!!",this.options) 

      this.filteredOptions = this.myControl.valueChanges.pipe(
        startWith(''),
        map(value => this._filter(value))
      );    
     },
     error => console.log("errooorr",error)
    );
  }

  sendSeekReq(data){
    console.log("request to owner..", data);

    this.privateService.sendSeekRequest(data).subscribe(backData =>{
      
      console.log("!!!!!!!!!!",backData, data.mobile_number) ;
       const seekerrequest = this.dialog.open( Sendrequestpopup, {
           width:"450px",  
         });
      // alert('Request sent successfully to');      
      this.ownermobile = '';
      this.dtaownerdiv='empty';
      this.ownerDetails = {};
      this.pendingfun();
      this.ngOnInit();
      // this.router.navigate(['/DataAccessor/Seeker']);
     },
     error => console.log("errooorr",error)
    );

  }

  public pendingSeekerReq = [];
  public approvedSeekerReq = [];
  public declinedSeekerReq = [];
  public completedSeekerReq = [];
  getSeekerOwners(){
    this.privateService.getSeekerOwners().subscribe(backData =>{
      console.log("**********************&&&&&&&&&",backData);
      this.separateSeekerReq(backData);
     },
     error => console.log("errooorr",error)
    );
  }

  separateSeekerReq(allReq){
    this.pendingSeekerReq = [];
    this.approvedSeekerReq = [];
    this.declinedSeekerReq = [];
    this.completedSeekerReq = [];
    allReq.forEach(req => {
      if(req['ownerReqStatus'] == 'PENDING'){
        this.pendingSeekerReq.push(req);
      }
      else if(req['ownerReqStatus'] == 'APPROVED'){
        this.approvedSeekerReq.push(req);
      }
      else if(req['ownerReqStatus'] == 'DECLINED'){
        this.declinedSeekerReq.push(req);
      }
      else if(req['seekerReqStatus'] == 'COMPLETED'){
        this.completedSeekerReq.push(req);
      }

    })

  }

  public newrequestdiv:boolean = true;
  public ownerReqDetails:any=[];
  public enableAction:boolean = false;
  newreqfun(){
    this.ownermobile = '';
    this.dtaownerdiv='empty';
    this.ownerDetails = {};
    
    this.newrequestdiv = true;
    this.enableAction = false;
    // document.getElementById("newreq").style.backgroundColor="#2e6da4";
    // document.getElementById("pending").style.backgroundColor="white";
    // document.getElementById("approved").style.backgroundColor="white";
    // document.getElementById("declined").style.backgroundColor="white";
    // document.getElementById("completed").style.backgroundColor="white";
    // document.getElementById("newreq").style.color="white";
    // document.getElementById("pending").style.color="black";
    // document.getElementById("approved").style.color="black";
    // document.getElementById("declined").style.color="black";
    // document.getElementById("completed").style.color="black";

  }
  pendingfun(){
    this.newrequestdiv = false;
    this.enableAction = false;
    this.ownerReqDetails = this.pendingSeekerReq;

    // document.getElementById("newreq").style.backgroundColor="white";
    // document.getElementById("pending").style.backgroundColor="#2e6da4";
    // document.getElementById("approved").style.backgroundColor="white";
    // document.getElementById("declined").style.backgroundColor="white";
    // document.getElementById("completed").style.backgroundColor="white";
    // document.getElementById("newreq").style.color="black";
    // document.getElementById("pending").style.color="white";
    // document.getElementById("approved").style.color="black";
    // document.getElementById("declined").style.color="black";
    // document.getElementById("completed").style.color="black";
  }
  approvedfun(){
    this.newrequestdiv = false;
    this.enableAction = true;
    this.ownerReqDetails = this.approvedSeekerReq;

    // document.getElementById("newreq").style.backgroundColor="white";
    // document.getElementById("pending").style.backgroundColor="white";
    // document.getElementById("approved").style.backgroundColor="#2e6da4";
    // document.getElementById("declined").style.backgroundColor="white";
    // document.getElementById("completed").style.backgroundColor="white";
    // document.getElementById("newreq").style.color="black";
    // document.getElementById("pending").style.color="black";
    // document.getElementById("approved").style.color="white";
    // document.getElementById("declined").style.color="black";
    // document.getElementById("completed").style.color="black";
  }
  declinedfun(){
    this.newrequestdiv = false;
    this.enableAction = false;
    this.ownerReqDetails = this.declinedSeekerReq;

    // document.getElementById("newreq").style.backgroundColor="white";
    // document.getElementById("pending").style.backgroundColor="white";
    // document.getElementById("approved").style.backgroundColor="white";
    // document.getElementById("declined").style.backgroundColor="#2e6da4";
    // document.getElementById("completed").style.backgroundColor="white";
    // document.getElementById("newreq").style.color="black";
    // document.getElementById("pending").style.color="black";
    // document.getElementById("approved").style.color="black";
    // document.getElementById("declined").style.color="white";
    // document.getElementById("completed").style.color="black";
  }
  completedfun(){
    this.newrequestdiv = false;
    this.enableAction = false;
    this.ownerReqDetails = this.completedSeekerReq;

    // document.getElementById("newreq").style.backgroundColor="white";
    // document.getElementById("pending").style.backgroundColor="white";
    // document.getElementById("approved").style.backgroundColor="white";
    // document.getElementById("declined").style.backgroundColor="white";
    // document.getElementById("completed").style.backgroundColor="#2e6da4";
    // document.getElementById("newreq").style.color="black";
    // document.getElementById("pending").style.color="black";
    // document.getElementById("approved").style.color="black";
    // document.getElementById("declined").style.color="black";
    // document.getElementById("completed").style.color="white";
  }
  ownerInfoFun(Oid, Omobi){
    console.log("33333");
    let inf = {"oid":Oid,"omobi":Omobi};
    this.privateService.OwneridNmobi = inf;
     this.router.navigate(['/DataAccessor/ownerinfo']);
  };

public merchant_id;
public currency;
public redirect_url;
public cancel_url;
public language;
public amount;
public gst;
public encReq;
public xscode;

  moveToPayment(data) {
       console.log("Payment..........", data)
        this.merchant_id = "210756";
        this.currency = "INR";
        this.redirect_url = "https://www.pehachain.com:8001/ccavResponseHandler";
        this.cancel_url = "https://www.pehachain.com:8001/ccavResponseHandler";
        this.language = "EN";
        let obj={'merchant_id':this.merchant_id,'owner_id': data.owner_id,'currency':this.currency,'redirect_url':this.redirect_url,'cancel_url':this.cancel_url,'language':this.language }
        this.privateService.moveToPayment(obj).subscribe(data =>{
         console.log("dataa from backend------------------",obj);
         this.encReq = data.encReq;
         this.xscode = data.xscode;
         console.log("this.encReq", this.encReq)
         console.log("this.xscode", this.xscode)
         window.location.href = "https://www.pehachain.com:8001/sampletext/" + data.encReq + "/" + data.xscode
       });
      } 


}




@Component({
  selector: 'ownerFullInfo',
  templateUrl: './ownerFullInfo.html',
  styleUrls: ['./seeker-dashboard.component.css']
})
export class ownerFullInfoComponent implements OnInit {

  constructor(private privateService:PrivateService,
    private userDocsServ: UserDocsService,
    private dataOwnerServ: DataOwnerService,
    public dialog: MatDialog) { }

  public ownerId;
  public ownerMobi;
  public privateUserId;

  ngOnInit() {
    this.ownerId = this.privateService.OwneridNmobi['oid'];
    this.ownerMobi = this.privateService.OwneridNmobi['omobi'];    
    this.getprofile();
  }

  public ownerDetails : any={};
  getprofile(){
    console.log("DDDDDDD");
    let ownerMob = { "mobile": this.ownerMobi }
    this.privateService.getAllOwnerDetails(ownerMob).subscribe(backData =>{
      console.log("$$$$$$$",backData);
      this.ownerDetails = backData['info'];
      this.ownerDetails['profile_pic'] = backData['pic'];
      this.getDocsFromPostGre();
     },
     error => console.log("errooorr",error)
    );
  }

  public educationDocs = [];
  public experienceDocs = []; 
  public financialDocs = []; 
  public socialDocs = [];
  public personalDocs = [];
  public userAllDocs = [];

  public errorMessage;

  viewDocumnet(data){
    this.privateService.viewDoc = data;
    const privateotpref = this.dialog.open( SeekerDocView, {
      width:"1000px",
      height:"650px"
    });
  }



  public selectedd = "";
  public iconName:any = 'expand_more';
  
  public docCategs = ["Education Documents","Experience Documents","Financial Documents","Social Documents","Personal Documents"]

  showtable(depdocs){
     // this.TransDetails=i
     if(this.selectedd != depdocs[0]['documentType']){
       this.selectedd = depdocs[0]['documentType'];
       this.iconName = "expand_less";
     }else {
       this.selectedd = "";
       this.iconName = "expand_more";
     }
     console.log("#####",this.selectedd)
   }    

  public docDataPostGre = [];
  public getDocsFromPostGre(){
    let ownerPriva = {"ownerId":this.ownerId}
    this.privateService.getDocsFromPostForSeek(ownerPriva).subscribe(respData => {
      console.log("dataaaaa from postgresssssss",respData);
      this.docDataPostGre = respData['userAllDo'];
      this.privateUserId = 'O'+this.ownerId.toString();
      this.loadAll(this.privateUserId);
    },
    error => console.log("errooorr",error)
    );
  }

  public loadAll(userId): Promise<any> {
    console.log("im in loadall methodddddd",userId)
    const tempList = [];
    return this.userDocsServ.getUserAllDocs(userId)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      console.log("userrr All Docssss",result)
      result.forEach(asset => {
        tempList.push(asset);
        this.docDataPostGre.forEach(psDoc => {
          if (asset['docId'] == psDoc['docId']) {
            asset['document'] = psDoc['document'];
            
            if (asset['documentType'] == "Education") {
              this.educationDocs.push(asset);
            }
            else if (asset['documentType'] == "Experience") {
              this.experienceDocs.push(asset);
            }
            else if (asset['documentType'] == "Financial") {
              this.financialDocs.push(asset);
            }
            else if (asset['documentType'] == "Social") {
              this.socialDocs.push(asset);
            }
            else  {
              this.personalDocs.push(asset);
            }
          }
        })
       
      });    
      this.userAllDocs = [this.educationDocs,this.experienceDocs,this.financialDocs,this.socialDocs,this.personalDocs]  
      console.log("this.userAllDocs",this.userAllDocs);
      console.log("this.educationDocs",this.educationDocs);
      console.log("this.experienceDocs",this.experienceDocs);
      console.log("this.financialDocs",this.financialDocs);
      console.log("this.socialDocs",this.socialDocs);
      console.log("this.personalDocs",this.personalDocs);      
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }




  
}





@Component({
  selector: 'seekerDocview',
  templateUrl: './seekerDocview.html',
  styleUrls: ['./seeker-dashboard.component.css']
})

export class SeekerDocView implements OnInit {

  constructor( private PrivateService:PrivateService,
    public dialogRef: MatDialogRef<SeekerDocView>  ) {}

    public seekerDoc;
  ngOnInit() {
    this.seekerDoc =this.PrivateService.viewDoc
  }
  closeimage(){
    this.dialogRef.close();
  }  
}

  

@Component({
selector: 'sendrequest',
templateUrl: 'sendrequestpopup.html'
})
export class Sendrequestpopup{
constructor(
public successpopupRef: MatDialogRef<Sendrequestpopup>,
 public dialog: MatDialog,private router: Router) {}

closepopup(){
this.successpopupRef.close();
}

}



