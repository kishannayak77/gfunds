import { Component, OnInit } from '@angular/core';
import { AdminVerifyService } from '../../Services/PrivateService/AdminVerify/admin-verify.service';
import { LoginService } from '../../Services/PublicService/login.service';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { Globals } from '../../globals';
import { PrivateService } from '../../Services/PrivateService/private.service';
import { MatDialog, MatDialogConfig,MatDialogRef } from '@angular/material';
import { ViewImage } from '../../data-owner/action/action.component';
import { ApproveDeclineModel } from '../../data-accessor/verifier-dashboard/verifier-dashboard.component';
import { UserDocsService } from '../../Services/PrivateService/UserDocs/user-docs.service';

export interface AprvDclnData{
  status:string;
}

@Component({
  selector: 'app-manage-request',
  templateUrl: './manage-request.component.html',
  styleUrls: ['./manage-request.component.css']
})
export class ManageRequestComponent implements OnInit {

  constructor(private adminVerifyService:AdminVerifyService,
              private userDocServ:UserDocsService,
              private globals:Globals,
              private loginservice:LoginService,
              private router: Router,
              private privateService:PrivateService,
              public dialog: MatDialog,) { }

  ngOnInit() {
    this.admiLoadAllDocs();    

    let header = document.getElementById("myDIV");
  	let btns = header.getElementsByClassName("btncls");
  	for (var i = 0; i < btns.length; i++) {
  	  btns[i].addEventListener("click", function() {
  	  var current = document.getElementsByClassName("active");
  	  current[0].className = current[0].className.replace(" active", "");
  	  this.className += " active";
  	  });
  	}
  }

  public allDocuments;
  public admiLoadAllDocs(): Promise<any> {
      const tempList = [];
      return this.userDocServ.getAllDocsForAdmin()
      .toPromise()
      .then((result) => {
        this.errorMessage = null;
        result.forEach(asset => {
          tempList.push(asset);
        });
        this.allDocuments = tempList;
        console.log("Alllll Documentsssss",this.allDocuments);
        this.allDocumentsOfAdmin();
        
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }




  public filteredDocs:any = [];
  public pendingDocs:any=[];
  public approvedDocs:any=[];
  public declinedDocs:any=[];
  allDocumentsOfAdmin() {
    this.pendingDocs = [];
    this.approvedDocs = [];
    this.declinedDocs = [];
    this.adminVerifyService.allDocumentsForAdmin().subscribe(resp => {
      console.log("dataaaaa", resp);
      resp.forEach( obj => {
        this.allDocuments.forEach( cDoc => {
          if(obj['docId'] === cDoc['docId']){
            if(obj['adminVerifyStatus'] == 'PENDING'){
                this.pendingDocs.push(obj);
            }
            else if(obj['adminVerifyStatus'] == 'APPROVED'){
              this.approvedDocs.push(obj);
            }
            else if(obj['adminVerifyStatus'] == 'DECLINED'){
              this.declinedDocs.push(obj)
            }
            else{
              console.log("No Docs");
            }

          }
        })
        
      });
      this.pendingAdminReq();
    });
  }

  public approvedRow:boolean = false;
  public declinedRow:boolean = false;
  pendingAdminReq(){
    this.filteredDocs=this.pendingDocs;
    this.approvedRow = false;
    this.declinedRow = false;
    console.log("hoi...1111",this.filteredDocs );
  }
  approvedAdminReq(){
    this.filteredDocs=this.approvedDocs;
    this.approvedRow = true;
    this.declinedRow = false;
    console.log("hoi...2222",this.filteredDocs );

  }
  declinedAdminReq(){
    this.filteredDocs=this.declinedDocs;
    this.approvedRow = false;
    this.declinedRow = true;
    console.log("hoi...3333",this.filteredDocs );
  }
  
  viewImg(data){
    console.log("popupppppppppppppp",data)
    this.privateService.documentv = data;
    const privateotpref = this.dialog.open( ViewImage, {
      width:"1000px",
      height:"700px"
    });
  }


  public AdminDocInfoTrans:any = {}
  public status:string;
  adminAprvDclnmodel(id, status){
      console.log("dataaaaaaaaaaaaaaaaaaaaa",id,status);
      this.AdminDocInfoTrans["docId"] = id;
      this.AdminDocInfoTrans["status"] = status; 
      this.status = status;
      const openAprvDclnmodelref = this.dialog.open( ApproveDeclineModel, {
        width:"500px",
        // height:"650px",
        data:{status:this.status}
      });
      openAprvDclnmodelref.afterClosed().subscribe(comm =>{
        console.log("commenttttttt",comm);
        if (comm){
          this.AdminDocInfoTrans["comment"] = comm;
          this.adminDocStatusChange(this.AdminDocInfoTrans)
        }        

      })
  }


  public errorMessage;
  public adminDocStatusChange(doc:any): Promise<any> {
    let vdate = new Date();
    let vd = vdate.toString();
    let transaction = {
      $class: 'gowdanar.pehachain.AdminVerification',
      'userDocId': "resource:gowdanar.pehachain.UserDocs#"+doc["docId"],      
      'verifiedDateTime': vd,      
      'adminStatus': doc["status"],
      
    };    

    return this.adminVerifyService.adminTransaction(transaction)
    .toPromise()
    .then(() => {
      this.adminDocStatusChangeInPostGre(doc, vd)
      this.errorMessage = null;
      this.ngOnInit();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
        this.errorMessage = error;
      }
    });



  }


  public adminDocStatusChangeInPostGre(doc, vd){
    doc["veriDate"] = vd;

    this.adminVerifyService.adminDocStatusChangeInPostGre(doc).subscribe(backData =>{
      console.log("Alllll Requestttss",backData);
      this.ngOnInit()
     },
     error => console.log("errooorr",error)
    );
  }

}




