import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent,AdminHeaderComponent } from './dashboard/dashboard.component';
import { ManageDataownerComponent } from './manage-dataowner/manage-dataowner.component';
import { ManageVerifierComponent } from './manage-verifier/manage-verifier.component';
import { ManageSeekerComponent } from './manage-seeker/manage-seeker.component';
import { ManagePaymentsComponent } from './manage-payments/manage-payments.component';
import { ManageRequestComponent } from './manage-request/manage-request.component';
import { ManagePostsComponent } from './manage-posts/manage-posts.component';
import { AuthGuard } from '../auth.guard';
import { ManageOrgnizationsComponent, organizationDetails } from './manage-orgnizations/manage-orgnizations.component';
import { VerifierPayinfoComponent } from './verifier-payinfo/verifier-payinfo.component';
import { SeekerPayinfoComponent } from './seeker-payinfo/seeker-payinfo.component';
import { ManagePartnernodeComponent } from './manage-partnernode/manage-partnernode.component';
import { AddPartnernodeComponent } from './add-partnernode/add-partnernode.component';
const routes: Routes = [
	
	{
	    path: '',  
			component: AdminHeaderComponent,
			// canActivateChild:[AuthGuard],			
	    children:[
			    {
				    path:'',
				    component: DashboardComponent
			    },
				{
					path:'Manage-partnernode',
					component:ManagePartnernodeComponent
				},
				{
					path:'Add-partnernode',
					component:AddPartnernodeComponent
				},
				{
					path:'Manage-dataowner',
					component:ManageDataownerComponent
				},
				{
					path:'Manage-verify',
					component:ManageVerifierComponent
				},
				{
					path:'Manage-seeker',
					component:ManageSeekerComponent
				},
				{
					path:'Manage-pay',
					component:ManagePaymentsComponent
				},
				{
					path:'Manage-request',
					component:ManageRequestComponent
				},
				{
					path:'Manage-post',
					component:ManagePostsComponent
				},
				{
                    path:'manage-orgnizations',
					component:ManageOrgnizationsComponent,
				},
				{
                    path:'verifier-payinfo',
					component:VerifierPayinfoComponent,
				},
				{
                    path:'seeker-payinfo',
					component:SeekerPayinfoComponent,
				},
				{
					path:'orgData',
					component:organizationDetails
				},
			]
		}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
