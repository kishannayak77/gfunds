import { TestBed } from '@angular/core/testing';

import { CustomservicesService } from './customservices.service';

describe('CustomservicesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CustomservicesService = TestBed.get(CustomservicesService);
    expect(service).toBeTruthy();
  });
});
