import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { RegistrationModule } from './registration/registration.module';
import { HomeModule } from './home/home.module';
import { AppCustomPreloader } from './AppCustomPreloader';
import { AuthGuard } from './auth.guard';

// import { DataOwnerModule } from './data-owner/data-owner.module';
// import { DataAccessorModule } from './data-accessor/data-accessor.module';
// import { AdminModule } from './admin/admin.module';

const routes: Routes = [
	{
		path:'Registration',
		canActivate: [AuthGuard],
		component:RegistrationModule,
	},
	{
		path:'Home',
		canActivate: [AuthGuard],
		component:HomeModule
	},
	{
		path:'DataOwner',
		// component:DataOwnerModule
		loadChildren:'./data-owner/data-owner.module#DataOwnerModule',
		data: { preload: true }
	},
	{
		path:'DataAccessor',
		// component:DataAccessorModule
		loadChildren:'./data-accessor/data-accessor.module#DataAccessorModule',
		data: { preload: true }
	},
	{
		path:'Admin',
		// component:AdminModule
		canActivate: [AuthGuard],
		loadChildren:'./admin/admin.module#AdminModule',
		data: { preload: true }
	},
	{
		path:'Partnernode',
		// component:AdminModule
		canActivate: [AuthGuard],
		loadChildren:'./partnernode/partnernode.module#PartnernodeModule',
		// data: { preload: true }
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true ,  preloadingStrategy: AppCustomPreloader})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
