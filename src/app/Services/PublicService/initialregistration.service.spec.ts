import { TestBed } from '@angular/core/testing';

import { InitialregistrationService } from './initialregistration.service';

describe('InitialregistrationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InitialregistrationService = TestBed.get(InitialregistrationService);
    expect(service).toBeTruthy();
  });
});
