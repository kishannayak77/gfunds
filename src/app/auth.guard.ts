import { Injectable } from '@angular/core';
import { CanActivate,Router,ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { LoginService } from './Services/PublicService/login.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate , CanActivateChild
 {
  constructor(private Auth:LoginService,private router:Router,) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    if(this.Auth.getToken()){
    	return true;
    }
    else{
    	this.router.navigate(['/login'],{ queryParams: { returnUrl: state.url }});
    	return false;
    }
  }
  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean{
    if(this.Auth.getToken()){
    	const otp=localStorage.getItem('role')
    	if(otp){
          return true;
      }
      else{
        this.router.navigate(['/home']);
        return false;
      }
    }
    else{
    	this.router.navigate(['/login'],{ queryParams: { returnUrl: state.url }});
    	return false;
    }
  }
}