import { Injectable } from '@angular/core';
import { Observable , of ,Subject, Observer, BehaviorSubject} from 'rxjs';
import { HttpClient,HttpHeaders, HttpErrorResponse} from  '@angular/common/http';
import { environment } from '../../../environments/environment';
import {Globals} from '../../globals';


@Injectable({
  providedIn: 'root'
})
export class PrivateService {
  getDocument(editDoc: { "doc_id": any; }): any {
    throw new Error("Method not implemented.");
  }
  private BLOCKCHAIN_URL:any;
  private DJANGO_URL;
  
  constructor(public http : HttpClient, private globals: Globals) {
  this.BLOCKCHAIN_URL = globals.urlPrivate;
  this.DJANGO_URL = globals.url  
   }
   // private messageSource = new BehaviorSubject<any>('0');
   // currentMessage = this.messageSource.asObservable();
   // changeMessage(message){
   //   console.log("eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",message)
   //   // this.messageSource.next(message)
     
   //   console.log("gdhwghdgwhgd",data)
   // }


   


  geteditData(data) : Observable <any>{
    console.log("im in first serviceeeeeeeeee",data);
    let url: string = `${this.DJANGO_URL}/GetDocument/`;
    return this.http.post(url,data);
  }

  profiledata1(): Observable <any> {
    console.log("im in first serviceeeeeeeeee");
    let url: string = `${this.DJANGO_URL}/profiledata/`;
    return this.http.get(url);
  }

  getallOrgs(): Observable <any> {
    // console.log("%%%%%%%%%%%%%%");
    let url: string = `${this.DJANGO_URL}/getAllorgslist/`;
    console.log("$$$$$$$$$$$$$$$$$");
    return this.http.get(url);
  }

  addDocument(data): Observable <any> {
    console.log("service document", document);
    let url: string = `${this.DJANGO_URL}/addDoc`;
    return this.http.post(url, data);
  }
  bankdetails(data): Observable <any> {
    console.log("bank detailssssss", data);
    let url: string = `${this.DJANGO_URL}/addWalletBankDetail/`;
    return this.http.post(url, data);
  }

  public mobileViewDocData;

  getRequests(): Observable <any> {
    let url: string = `${this.DJANGO_URL}/GetVerifierReq`;
    return this.http.get(url);
  }

  public mobileViewDoc;
  
  public ViewDoucments;

  public ViewImage;

  public docImage;

  public editData;

  public OwneridNmobi:any={};

  public viewDoc;
   public documentv;

  getAllOwnersMobile(): Observable<any>{
    let url: string = `${this.DJANGO_URL}/getDataOwnersMobile`;
    return this.http.get(url);
  }

  getAllOwnerDetails(data): Observable<any>{
    console.log("service..")
    let url: string = `${this.DJANGO_URL}/getOwnersDetails`;
    return this.http.post(url,data);
  }

  sendSeekRequest(data): Observable<any>{
    console.log("service data", data);
    let url: string = `${this.DJANGO_URL}/seekerOwnerRequst`;
    return this.http.post(url, data);
  }

  getSeekerRequests(): Observable<any>{
    let url: string = `${this.DJANGO_URL}/getSeekerRequests`;
    return this.http.get(url);
  }

getnotification(): Observable<any>{
  let url: string = `${this.DJANGO_URL}/notify`;
    return this.http.get(url);
  

}



  changeReqStatus(status): Observable<any>{
    let url: string = `${this.DJANGO_URL}/changeReqStatus`;
    return this.http.post(url,status);
  }

  getSeekerOwners(): Observable<any>{
    let url: string = `${this.DJANGO_URL}/getSeekerOwners`;
    return this.http.get(url);
  }

  getOwnerProfile(data): Observable<any>{
    let url: string = `${this.DJANGO_URL}/getOwnerProfile/`;
    return this.http.post(url, data);
  }
   moveToPayment(data): Observable<any>{
    let url: string = `${this.DJANGO_URL}/ccavRequestHandler`;
    return this.http.post(url, data);
  }
  getSeekerPayDetails():Observable<any>{
    let url:string = `${this.DJANGO_URL}/getSeekerPayDetails`;
    return this.http.get(url);
  }

  getDocsFromPost(): Observable <any>{
    let url:string = `${this.DJANGO_URL}/getDocsOfUser`;
    return this.http.get(url);
  }
  
  newpinfun1(data):Observable<any>{
    let url:string = `${this.DJANGO_URL}/generateNewPin`;
    return this.http.post(url,data);
  }
  verifyPrivatePin(data):Observable<any>{
    let url:string = `${this.DJANGO_URL}/verifyPin`;
    return this.http.post(url,data);
  }

  docStatusChangeInPostGre(data):Observable<any>{
    let url:string = `${this.DJANGO_URL}/changeDocStatus`;
    return this.http.post(url,data);
  } 
  getwalletdetails():Observable<any>{
    let url:string = `${this.DJANGO_URL}/getWalletDetails`;
    return this.http.get(url);
  }

  getDocsFromPostForSeek(data):Observable<any>{
       let url:string = `${this.DJANGO_URL}/getDocsFromPostForSeek`;
    return this.http.post(url,data);
  }
                                                                                                                                          
  post(data):Observable<any>{
    let url:string = `${this.DJANGO_URL}/addPartnerNode`;
    return this.http.post(url,data);
  }
   getPartnernodedetail():Observable<any>{
    let url:string = `${this.DJANGO_URL}/getPartnerNodeinfo`;
    return this.http.get(url);
  }
   savePdata(data):Observable<any>{
      console.log("service ppppppppppp data", data);
    let url:string = `${this.DJANGO_URL}/savePnodedata`;
    return this.http.post(url,data);
  }

  ownerNotifications(){
    let url:string = `${this.DJANGO_URL}/ownerNotifications`;
    return this.http.get(url);
  }

  
}




















































































