import { Injectable } from '@angular/core';
import { DataService } from '../../../data.service';
import { Observable } from 'rxjs';
import { UserDocs } from '../../../gowdanar.pehachain';

@Injectable({
  providedIn: 'root'
})
export class UserDocsService {

  private NAMESPACE = 'UserDocs';

  constructor(private dataService: DataService<UserDocs>) {
  };

  public editDocument(userId,type):Observable<UserDocs>{
    console.log("dataaa in userrrr serviceee",userId,type);
    return this.dataService.update(this.NAMESPACE, userId,type);
  }
  public addDocument(itemToAdd: any): Observable<UserDocs> {
    console.log("dataaa in userrrr serviceee",itemToAdd);
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }
  
  public getUserAllDocs(userId): Observable<UserDocs[]> {
    console.log("dataaa in userrrr serviceee",userId);
    return this.dataService.getUserDocs(userId);
  }
  public getAll(): Observable<UserDocs[]> {
    return this.dataService.getAll(this.NAMESPACE);
  }

  public getAllDocsOfVerifier(mobile: string):Observable<UserDocs[]> {
    console.log("dataaa in userrrr serviceee",mobile);
    return this.dataService.getDocsByVerifierMobile(mobile);
  }
  public getAllDocsForAdmin():Observable<UserDocs[]> {
    console.log("dataaa in userrrr serviceee");
    return this.dataService.getAll(this.NAMESPACE);
  }
}
