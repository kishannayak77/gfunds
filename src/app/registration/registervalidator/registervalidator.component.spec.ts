import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistervalidatorComponent } from './registervalidator.component';

describe('RegistervalidatorComponent', () => {
  let component: RegistervalidatorComponent;
  let fixture: ComponentFixture<RegistervalidatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistervalidatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistervalidatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
