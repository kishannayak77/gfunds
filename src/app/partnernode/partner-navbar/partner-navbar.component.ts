import { Component, OnInit } from '@angular/core';
import {Router,NavigationEnd, RouterModule } from '@angular/router';
import { LoginService } from '../../Services/PublicService/login.service';
import { Globals } from '../../globals';
import { PrivateService } from './../../Services/PrivateService/private.service';

@Component({
  selector: 'app-partner-navbar',
  templateUrl: './partner-navbar.component.html',
  styleUrls: ['./partner-navbar.component.css']
})
export class PartnerNavbarComponent implements OnInit {

  constructor(  private router: Router,
                private globals: Globals,
                private loginService: LoginService,
                private privateserv:PrivateService) { }

  ngOnInit() {
    this.PartnernodeDetail();
  }
  public partnernode :any ={};
  PartnernodeDetail(){
  this.privateserv.getPartnernodedetail().subscribe(backData=>{
    console.log("geuuuuuuuuuuuuuuuuus",backData);
    this.partnernode=backData["info"];
    this.profi=backData["profile"];
    console.log("getpartnerdeatils",this.partnernode);
  })
  }
  public profi;
  closeNav(){
     document.getElementById("mySidenav").style.width = "0px";
   }
   openNav(){
     document.getElementById("mySidenav").style.width = "250px";
   }

   public user:any;
   logoutPrivate () {
    this.user = {};    
    console.log("in component dataaa"); 
    this.loginService.logout11().subscribe(
      backendres =>{
        console.log("response from backend",backendres); 
        localStorage.removeItem('token');
        this.globals.loggedInUserRole='';
        this.router.navigate(['/']);        
      },
      error => console.log("errrroooooorrr",error)
       
    )
 
  } 


}
