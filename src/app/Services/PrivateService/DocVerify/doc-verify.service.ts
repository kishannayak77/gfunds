import { Injectable } from '@angular/core';
import { DataService } from '../../../data.service';
import { Observable } from 'rxjs';
import { DocumentVerification } from '../../../gowdanar.pehachain';

@Injectable({
  providedIn: 'root'
})
export class DocVerifyService {

  private NAMESPACE = 'DocumentVerification';

  constructor(private dataService: DataService<DocumentVerification>) {
  };

  public addTransaction(itemToAdd: any): Observable<DocumentVerification> {
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }
}
