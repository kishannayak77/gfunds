import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActionComponent, OwnerMobileView, ViewDocuments, EditData } from './action/action.component';
import { DashboardComponent,PrivateHeaderComponent, OwnerRequestsComponent} from './dashboard/dashboard.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
	{
		path : '',
		canActivateChild:[AuthGuard],		
		component : PrivateHeaderComponent,
		children:[
			
			{
				path:'',
				component:DashboardComponent
			},
			{
				path:'action',
				component:ActionComponent
			},
			{
				path:'Owner_Mobile_View',
				component:OwnerMobileView
			},
			{
				path:'View_Documents',
				component:ViewDocuments
			},
			{
				path:'Owner_Requests',
				component:OwnerRequestsComponent
			},
			{
				path:'Edit_Data',
				component:EditData
			}
		]
	},
	
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataOwnerRoutingModule { }
