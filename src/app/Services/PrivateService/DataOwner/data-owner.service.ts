import { Injectable } from '@angular/core';
import { DataService } from '../../../data.service';
import { Observable } from 'rxjs';
import { DataOwner } from '../../../gowdanar.pehachain';

@Injectable({
  providedIn: 'root'
})
export class DataOwnerService {

  private NAMESPACE = 'DataOwner';
  constructor(private dataService: DataService<DataOwner>) {
  };

  public addDataOwner(itemToAdd: any): Observable<DataOwner>{
    console.log("userrrrr dataaaaaaaaaa in seeeeeerrvicceee",itemToAdd);
    return this.dataService.add(this.NAMESPACE, itemToAdd);
  }



  
}
