import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent, OrgHeaderComponent} from './dashboard/dashboard.component';
import { VerifierDashboardComponent } from './verifier-dashboard/verifier-dashboard.component';
import { SeekerDashboardComponent, ownerFullInfoComponent } from './seeker-dashboard/seeker-dashboard.component';
import { AccesserPaymentComponent, WalletsComponent, RealizedAmountComponent, CreditAmountComponent } from './accesser-payment/accesser-payment.component';
import { AuthGuard } from '../auth.guard';

const routes: Routes = [
	{
		path:'',
		canActivateChild:[AuthGuard],
		component : OrgHeaderComponent,
		children:[
			{
			   path:'',
			   // canActivateChild: [AuthGuard],
			   component:DashboardComponent,
			   children:[
			   		{
			   			path:'',
			   			component:VerifierDashboardComponent
			   		},
			   		{
			   			path:'Seeker',
			   			component:SeekerDashboardComponent
			   		},
			   		{
			   			path:'ownerinfo',
			   			component:ownerFullInfoComponent
			   		},
			   		
			   ]
			},
			{
				path:'Wallets',
				// canActivateChild : [AuthGuard],
				component:WalletsComponent,
				children:[
					{
						path:'',
						component:RealizedAmountComponent
					},
					{
						path:'CreditAmount',
						component:CreditAmountComponent
					},
					{
			   			path:'AccessorPay',
			   			component:AccesserPaymentComponent
			   		}
				]
			},


			// {
	  //  			path:'AccessorPay',
	  //  			component:AccesserPaymentComponent
	  //  		}			
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataAccessorRoutingModule { }
