import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/PublicService/login.service';
import { Globals } from 'src/app/globals';
import {Router, ActivatedRoute,NavigationEnd} from '@angular/router';
import { InitialregistrationService } from "../../Services/PublicService/initialregistration.service";


@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.css']
})
export class PublicProfileComponent implements OnInit {
personal:any;
expDtls:any;
qualDtls:any;
searchphto:any;
  constructor(public loginservice:LoginService,
    private globals: Globals,
    private router: Router, 
    private initialRegServ: InitialregistrationService){ 
  this.personal=loginservice.searchViewProfile;
  this.expDtls=loginservice.searchExp; 
  this.qualDtls=loginservice.searchQualification;
  this.searchphto=loginservice.searchPrfPic;
  }
  public front_photo;
  public dataaaaaa;

  public presentexp=[];
  public passyears = [];
  public presentqul =[];


  
  ngOnInit() {
    this.initialRegServ.redirectToHome = "No Redirect1";
    if(this.initialRegServ.redirectToHome == "Redirect1"){
      this.router.navigate(['/message']);  
    }
	this.getmyFrds()
    // console.log("SSSSSSSS$$$", this.searchphto);
  	
  	for(let i=0; i<this.expDtls.length; i++){
		  if(this.expDtls[i].Iscurrentcompany == 'yes')
		  {
			  this.presentexp.push(this.expDtls[i]);
		      console.log("hey",this.presentexp)
			  
  		}
  		
  	}
  	console.log("qual", this.qualDtls);
  	for(let i=0; i<this.qualDtls.length; i++){
  		this.passyears.push(parseInt(this.qualDtls[i].year_of_passing));
  		let maxyear = Math.max.apply(null, this.passyears)
  	}

  	for(let i=0; i<this.qualDtls.length; i++){

  		if(this.qualDtls[i].year_of_passing == Math.max.apply(null, this.passyears)){
  			this.presentqul.push(this.qualDtls[i]);
  		}
  	}
  	console.log("passyears", this.passyears);
  	console.log("@@@@@@@@", this.presentqul);
  	
}
public myFriendsIds:any;
  public friendRequestRecieved:any;
  public friendRequestSent:any;
  public followingCompIds:any;
  getmyFrds(){
    this.loginservice.getmyfrds().subscribe(
      backendResponse=>{
        console.log(backendResponse)
        this.myFriendsIds=backendResponse['myFriends'];
        this.friendRequestRecieved=backendResponse['requestRecieved'];
        this.friendRequestSent=backendResponse['requestsSent'];
        this.followingCompIds=backendResponse['followingCompIds']
      },
      error=>{
        console.log(error)
      }
    )
  }
  sendfriendRequest(data){
    console.log("sending REQUEST")
    console.log(data)
    let details={'user_id':data.user_id,'full_name':this.loginservice.searchFull_name}
   this.loginservice.sendfriendRequest1(details).subscribe(
     backResponse => {
       console.log('#######',backResponse);
       // this.data=backResponse; 
       this.getmyFrds(); 
     },
     error => console.log("erroorrrr",error)
   );

 }
 addfriendrequest(data){
  // let obj={"id":data.id};
  let details={'user_id':data.user_id,'full_name':this.loginservice.searchFull_name}
  this.loginservice.addfriendrequest1(details).subscribe(
    backResponse => {
      console.log('#######',backResponse);
      this.getmyFrds();
    },
    error => console.log("erroorrrr",error)
  );
}

deleteFriendRequest1(data){
  console.log("delete request",data)
  let details={'user_id':data.user_id,'full_name':this.loginservice.searchFull_name}
  this.loginservice.deleteFriendRequest(details).subscribe(
    backResponse => {
      console.log('#######',backResponse);
      this.getmyFrds();        
    },
    error => console.log("erroorrrr",error)
  );
}

   

}

