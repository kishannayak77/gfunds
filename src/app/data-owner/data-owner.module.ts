import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule} from '../../material-module';
import { DataOwnerRoutingModule } from './data-owner-routing.module';
import { ActionComponent, OwnerMobileView, ViewDocuments,ViewImage, EditData } from './action/action.component';
import { DashboardComponent, PrivateHeaderComponent, OwnerRequestsComponent } from './dashboard/dashboard.component';
import { UserDocsService } from '../Services/PrivateService/UserDocs/user-docs.service';
import { DataService } from '../data.service';
import { LoginService } from '../Services/PublicService/login.service';
import { NgxPaginationModule} from 'ngx-pagination'; 
import { FiltervaluePipe } from '../filtervalue.pipe';



@NgModule({
  declarations: [
  	ActionComponent,
    DashboardComponent,
    PrivateHeaderComponent,
    ViewDocuments,
    ViewImage,
    OwnerRequestsComponent,
    OwnerMobileView,
    EditData,
    FiltervaluePipe
  ],
  imports: [
    CommonModule,
    DataOwnerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    NgxPaginationModule,
  ],
  entryComponents:[
    PrivateHeaderComponent,
    OwnerMobileView, 
    ViewDocuments,
    ViewImage,
    OwnerRequestsComponent,
    EditData
  ],
  providers: [
    UserDocsService,
    DataService
  ]
})
export class DataOwnerModule { }
